package rs.projekatOSA2019_maven.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.projekatOSA2019_maven.entity.Colloquium;
import rs.projekatOSA2019_maven.entity.Department;
import rs.projekatOSA2019_maven.entity.Document;
import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.FinancialCard;
import rs.projekatOSA2019_maven.entity.ListPayment;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.entity.User;
import rs.projekatOSA2019_maven.enums.Gender;

public class StudentDto {
	
	private int id;
	private String name;
	private String lastName;
	
	private String index;
	private int yearOfEnrollment;
	private int yearStudy;
	
	
	private String countryOfBirth;
	private String placeOfBirth;
	private String DateOfBirth;
	private Gender gender;
	private String wayOfFinancing;
	private String email;
	private String address;
	private int totalECTSPoints;
	private double averageRating;
	private String ordinalNumberOfEntries;
	
	private DepartmentDto department;
	
	private FinancialCardDto financialCard;
	
	private UserDto user;
	
	private List<DocumentDto> document = new ArrayList<DocumentDto>();
												
	private List<ListPaymentDto> listPayment = new ArrayList<ListPaymentDto>();	
	
	private List<ExamDto> exams = new ArrayList<ExamDto>();
	
	private List<ColloquiumDto> colloquium = new ArrayList<ColloquiumDto>();
	
	private List<SubjectDto> subjects = new ArrayList<SubjectDto>();
	
	public StudentDto(Student s) {
		super();
		this.id = s.getId();
		this.name = s.getName();
		this.lastName = s.getLastName();
		this.index = s.getIndex();
		this.yearOfEnrollment = s.getYearOfEnrollment();
		this.yearStudy = s.getYearStudy();
		this.countryOfBirth = s.getCountryOfBirth();
		this.placeOfBirth = s.getPlaceOfBirth();
		DateOfBirth = s.getDateOfBirth();
		this.gender = s.getGender();
		this.wayOfFinancing = s.getWayOfFinancing();
		this.email = s.getEmail();
		this.address = s.getAddress();
		this.totalECTSPoints = s.getTotalECTSPoints();
		this.averageRating = s.getAverageRating();
		this.ordinalNumberOfEntries = s.getOrdinalNumberOfEntries();
		this.department = new DepartmentDto(s.getDepartment());
		System.out.println(s.getFinancialCard());
		System.out.println(s.getDepartment());
		this.financialCard = new FinancialCardDto(s.getFinancialCard());
		this.user = new UserDto(s.getUser());
		if(s.getSubjects() != null) {
			for (Subject subject : s.getSubjects()) {
				this.subjects.add(new SubjectDto(subject));
			}
		}
		if(s.getDepartment().getSubjects() != null) {
			for (Subject subject : s.getDepartment().getSubjects()) {
				this.subjects.add(new SubjectDto(subject));
			}
		}
//		if (s.getDocument() != null) {
//			for (Document document : s.getDocument()) {
//				this.document.add(new DocumentDto(document));
//			}
//		}
//		if (s.getListPayment() != null) {
//			for (ListPayment listPayment : s.getListPayment()) {
//				this.listPayment.add(new ListPaymentDto(listPayment));
//			}
//		}
//
//		if(s.getExam() != null) {
//			for (Exam e : s.getExam()) {
//				this.exams.add(new ExamDto(e));
//			}
//		};
//		if(s.getColloquium() != null) {
//			for (Colloquium co : s.getColloquium()) {
//				this.colloquium.add(new ColloquiumDto(co));
//			}
//		}
	}

	public StudentDto() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public int getYearOfEnrollment() {
		return yearOfEnrollment;
	}

	public void setYearOfEnrollment(int yearOfEnrollment) {
		this.yearOfEnrollment = yearOfEnrollment;
	}

	public int getYearStudy() {
		return yearStudy;
	}

	public void setYearStudy(int yearStudy) {
		this.yearStudy = yearStudy;
	}

	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getDateOfBirth() {
		return DateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getWayOfFinancing() {
		return wayOfFinancing;
	}

	public void setWayOfFinancing(String wayOfFinancing) {
		this.wayOfFinancing = wayOfFinancing;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getTotalECTSPoints() {
		return totalECTSPoints;
	}

	public void setTotalECTSPoints(int totalECTSPoints) {
		this.totalECTSPoints = totalECTSPoints;
	}

	public double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}

	public String getOrdinalNumberOfEntries() {
		return ordinalNumberOfEntries;
	}

	public void setOrdinalNumberOfEntries(String ordinalNumberOfEntries) {
		this.ordinalNumberOfEntries = ordinalNumberOfEntries;
	}

//	public Department getDepartment() {
//		return department;
//	}
//
//	public void setDepartment(Department department) {
//		this.department = department;
//	}

	public FinancialCardDto getFinancialCard() {
		return financialCard;
	}

	public void setFinancialCard(FinancialCardDto financialCard) {
		this.financialCard = financialCard;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public List<DocumentDto> getDocument() {
		return document;
	}

	public void setDocument(List<DocumentDto> document) {
		this.document = document;
	}
//
	public List<ListPaymentDto> getListPayment() {
		return listPayment;
	}
//
	public void setListPayment(List<ListPaymentDto> listPayment) {
		this.listPayment = listPayment;
	}

	public List<SubjectDto> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<SubjectDto> subjects) {
		this.subjects = subjects;
	}
	
	
//
//	public Set<Subject> getSubjects() {
//		return subjects;
//	}
//
//	public void setSubjects(Set<Subject> subjects) {
//		this.subjects = subjects;
//	}
//
//	public Set<Exam> getExam() {
//		return exams;
//	}
//
//	public void setExam(Set<Exam> exams) {
//		this.exams = exams;
//	}
//
//	public Set<Colloquium> getColloquium() {
//		return colloquium;
//	}
//
//	public void setColloquium(Set<Colloquium> colloquium) {
//		this.colloquium = colloquium;
//	}


}
