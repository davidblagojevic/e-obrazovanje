import { UserDto } from "./user-dto";

export class TeacherDto {

    id:number;
    name:string;
    lastName:string;
    email:string;
    user:UserDto
    constructor(id:number,name:string,lastName:string, email:string,user:UserDto){
        this.id=id;
        this.name=name;
        this.lastName=lastName;
        this.email=email;
        this.user=user;
    }

}