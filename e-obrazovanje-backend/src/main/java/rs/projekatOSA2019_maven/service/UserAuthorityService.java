package rs.projekatOSA2019_maven.service;

import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.Authority;
import rs.projekatOSA2019_maven.entity.User;
import rs.projekatOSA2019_maven.repository.AuthorityRepository;
import rs.projekatOSA2019_maven.repository.UserRepository;
import rs.projekatOSA2019_maven.security.SecurityUtils;

@Service
@Transactional
public class UserAuthorityService implements UserAuthorityServiceInterface {

	private final UserRepository userRepository;
	private final AuthorityRepository authorityRepository;
	
    public UserAuthorityService(UserRepository userRepository, AuthorityRepository authorityRepository) {
    	this.userRepository = userRepository;
    	this.authorityRepository = authorityRepository;
	}
	
    @Transactional(readOnly = true)
	@Override
	public Optional<User> getUserWithAuthorities() {
		return SecurityUtils.getCurrentUsername().flatMap(userRepository::findOneWithAuthoritiesByUsername);
	}
    
    public Authority findByName(String name) {
    	Authority authority = authorityRepository.findByName(name);
    	return authority;
    };

}
