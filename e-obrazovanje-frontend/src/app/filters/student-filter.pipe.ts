import { StudentDto } from './../model/student-dto';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'studentFilter'
})
export class StudentFilterPipe implements PipeTransform {

  transform(value: StudentDto[], index: string,name:string): unknown {

    console.log("searchTerm1"+index);
    console.log("searchTerm2"+name);



    if(!value || (!index && !name)){
      return value;
    }
    if(index && !name){
      return value.filter(s=>
        s.index.toLowerCase().indexOf(index.toLowerCase()) !== -1
      );
    }
    else if(!index && name){
      return value.filter(s=>
        s.name.toLowerCase().indexOf(name.toLowerCase()) !== -1
      );
    }
    else if(index && name){
      return value.filter(s=>
        (s.name.toLowerCase().indexOf(name.toLowerCase()) !== -1) && (s.index.toLowerCase().indexOf(index.toLowerCase()) !== -1)
      );
    }
  }

}
