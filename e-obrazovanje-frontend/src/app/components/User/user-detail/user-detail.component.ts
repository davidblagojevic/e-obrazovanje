import { Component, OnInit } from "@angular/core";
import { StudentDto } from "src/app/model/student-dto";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StudentService } from './../../../services/student.service';
import { ActivatedRoute, Router } from "@angular/router";
import { TeacherDto } from "src/app/model/teacher-dto";
import { TeacherService } from "src/app/services/teacher.service";
import { UserService } from "src/app/services/user.service";
import { UserDto } from 'src/app/model/user-dto';

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.css']
  })
  export class UserDetailComponent implements OnInit {

    constructor(private serviceStudent:StudentService,private serviceTeacher:TeacherService,private userS:UserService,private router: Router,private route:ActivatedRoute,private fb: FormBuilder) { }

    student: StudentDto;    
    teacher:TeacherDto;
    user:UserDto;
    updateStudent: FormGroup;
    updateTeacher: FormGroup;
    updateAdmin: FormGroup;
    role=localStorage.getItem("role");
    createStudent: FormGroup;

    get name() {
        return this.updateTeacher.get('name');
      }
    
      get lastName() {
        return this.updateTeacher.get('lastName');
      }
    
      get userName() {
        return this.updateTeacher.get('userName');
      }
    
      get password() {
        return this.updateTeacher.get('password');
      }
    
      get email() {
        return this.updateTeacher.get('email');
      }
    
      get nameS() {
        return this.createStudent.get('nameS');
      }
    
      get lastNameS() {
        return this.createStudent.get('lastNameS');
      }
    
      get userNameS() {
        return this.createStudent.get('userNameS');
      }
    
      get passwordS() {
        return this.createStudent.get('passwordS');
      }

      //admin
      get userNameA(){
          return this.updateAdmin.get('userNameA');
      }
      get passwordA(){
          return this.updateAdmin.get('passwordA');
      }
    
     
    
    
      get dateOfBirth() {
        return this.createStudent.get('dateOfBirth');
      }
    
    
      get countryOfBirth() {
        return this.createStudent.get('countryOfBirth');
      }
    
    

      get emailS() {
        return this.createStudent.get('emailS')
      }

    ngOnInit(): void {

        switch(this.role){
            case 'ROLE_STUDENT':{
              this.serviceStudent.getStudentByUser().subscribe(data => {
                console.log(data);
                this.student=data;

                var _name = this.student.name;
                var _lastName = this.student.lastName;
                var _email = this.student.email;
                var _userName=this.student.user.username;
                var _password=this.student.user.password;
                var _date=this.student.DateOfBirth;
      
                this.createStudent = this.fb.group({
                  nameS: [_name, Validators.required],
                  lastNameS: [_lastName, Validators.required],
                  userNameS: [_userName,Validators.required],
                  passwordS: [_password,Validators.required],
                  emailS: [_email, Validators.required],
                  date: [_date, Validators.required]
                });
            
            });
            break;
            }
            case 'ROLE_PROFESSOR':{
              this.serviceTeacher.getTeacherByUser().subscribe(data => {
                console.log(data);
                this.teacher=data;
                var _name = this.teacher.name;
                var _lastName = this.teacher.lastName;
                var _email = this.teacher.email;
                var _userName=this.teacher.user.username;
                var _password=this.teacher.user.password;
      
                this.updateTeacher = this.fb.group({
                  name: [_name, Validators.required],
                  lastName: [_lastName, Validators.required],
                  userName: [_userName,Validators.required],
                  password: [_password,Validators.required],
                  email: [_email, Validators.required]
                });
              });
            break;
            }
            case 'ROLE_ADMIN':{
                this.userS.getCurrentUser().subscribe(data => {
                    this.user = new UserDto(data.username, data.password);
                    this.user.id = data.id;
                    var _userName = this.user.username;
                    var _password = this.user.password;
                    this.updateAdmin = this.fb.group({
                        userNameA:[_userName, Validators.required],
                        passwordA:[_password, Validators.required]
                    });
                })
            }
            break;
            default:{
                alert('Nije nista od navedenog')
            
            }
          }

  }
  updateTeach(){
    var user=new UserDto(this.userName.value,this.password.value);
    var teacher=new TeacherDto(
      1,this.name.value,this.lastName.value,this.email.value,user
    )
      console.log(teacher);
    this.serviceTeacher.update(teacher,this.route.snapshot.params.id);
    this.router.navigate(['/TeacherComponent']);
  }
  updateStud(){
    var user=new UserDto(this.userNameS.value,this.passwordS.value);
    var student1=new StudentDto(
      1,this.nameS.value,this.lastNameS.value,this.student.index,this.student.countryOfBirth,this.student.placeOfBirth,
      this.student.DateOfBirth,this.student.gender,this.student.wayOfFinancing,this.emailS.value,this.student.address,
      this.student.totalECTSPoints,this.student.averageRating,1,user,this.student.financialCard,this.student.yearStudy
    );

  }

    updateAdm(){
        var admin = new UserDto(this.userNameA.value, this.passwordA.value);
        this.userS.updateUser(admin);
        this.router.navigate(['ExaminationPeriodAll']);

    }
    // if(this.validation(student)===true){
    //   this.service.update(student,this.route.snapshot.params.id);
    //   this.router.navigate(['StudentComponent']);
    // }
     
}