import { SubjectDto } from 'src/app/model/subject-dto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubjectService } from './../../../services/subject.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-subject-detail',
  templateUrl: './subject-detail.component.html',
  styleUrls: ['./subject-detail.component.css']
})
export class SubjectDetailComponent implements OnInit {

  constructor(private service:SubjectService,private router: Router,private route:ActivatedRoute,private fb: FormBuilder) { }

  updateSubject: FormGroup;
  subject: SubjectDto;

  get name() {
    return this.updateSubject.get('name');
  }

  get numberOfECTSPoints() {
    return this.updateSubject.get('numberOfECTSPoints');
  }



  role=localStorage.getItem('role');

  ngOnInit(): void {
    let id=this.route.snapshot.params.id;
    console.log(id);
    this.service.getSubjectsByid(id)
      .subscribe(
        data => {
          this.subject = data;
          console.log(this.subject);

          var _name = this.subject.name;
          var _numberOfECTSPoints = this.subject.numberOfECTSPoints;

          this.updateSubject = this.fb.group({
            name: [_name, Validators.required],
            numberOfECTSPoints: [_numberOfECTSPoints, Validators.required]
          });
        },
        error => {
          console.log(error);
        });
  
  }

  
  updateSub(){

    var subject=new SubjectDto(1,this.name.value,this.numberOfECTSPoints.value,new Date());
    console.log(subject)
    if(this.validation(subject)){
    this.service.update(subject,this.route.snapshot.params.id);
    this.router.navigate(['/SubjectComponent']);
    }
  }

  validation(subject:SubjectDto){
    if( subject.name==='' || subject.name.length < 3){
      alert("ime predmeta je prazno ili je manje od 3 slova");
      return false;
    }
    else if( subject.numberOfECTSPoints <= 0){
      alert("broj esp bodova ne sme biti manji od 0");
      return false;
    }
    else{
      return true;
    }

  }

}
