export class FinancialCardDto {

    id:number;
    cardNumber:number;
    sum:number;
	bankAccount:number;
	modelNumber:number;

    constructor( cardNumber:number,sum:number,bankAccount:number,modelNumber:number){
       
       // this.id=id;
        this.cardNumber=cardNumber;
        this.sum=sum;
        this.bankAccount=bankAccount;

        this.modelNumber=modelNumber;  
    }

}