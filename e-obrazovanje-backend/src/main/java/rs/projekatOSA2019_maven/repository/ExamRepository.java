package rs.projekatOSA2019_maven.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import rs.projekatOSA2019_maven.entity.Exam;

public interface ExamRepository extends JpaRepository<Exam, Integer> {
	
	List<Exam> findByDateOfLaying(Date exam );

	
	List<Exam> findByDateOfApplication(Date exam );
	
	Exam  findById(Long id);
	
	
	
	@Query("select i from Exam i where i.id = :uid ")
	Exam  findByIdExam(Integer uid);
	
	@Query("select i from Exam i where i.student.id = :uid and i.examStatus = 1 ")
	List<Exam>  getExamsFromUserWherePassedExam(Integer uid);

	
	@Query("select i from Exam i where i.student.id = :uid and i.examStatus = 0 ")
	List<Exam>  getNotPassedExamFromUser(Integer uid);

	
//	@Query("select i from Exam i where  i.id = :uid ")
//	Exam  findById1(long uid);
			

	@Query("select i from Exam i where i.student.id = :uid ")

	List<Exam>  getExamsFromUser(Integer uid);
	
	

	
	
	@Query("select e from  Exam e  where e.student.id = :uid and  e.examinationPeriod.id = :uid1  AND e.laiddown = 0 and e.examStatus = 0   GROUP BY  e.id  ")
	List<Exam>  getExamFromStudentAndPeriodPass(Integer uid,long uid1);
	
	@Query("select e from  Exam e  where e.student.id = :uid and  e.examinationPeriod.id = :uid1 AND e.laiddown = 1 and e.examStatus = 0  GROUP BY  e.id  ")
	List<Exam>  getExamFromStudentAndPeriodNotPass(Integer uid,long uid1);

	 @Transactional
	  @Modifying
	@Query("update  Exam e  SET laiddown = 1 WHERE  e.id=:uid")
	void  updateExamLaiddown(long uid);

	 
	 @Transactional
	  @Modifying
	@Query("update  Exam e  SET laiddown = 0 WHERE  e.id=:uid")
	void  updateExamLaiddownOnNull(long uid);
	
	 @Query("select e from  Exam e  where  e.examinationPeriod.id = :p and e.subject.id=:s   AND e.laiddown = 1  AND e.examStatus = 0")
		List<Exam>  getExamFromPeriodPass(Integer s,Long p); 
	 
	



}
