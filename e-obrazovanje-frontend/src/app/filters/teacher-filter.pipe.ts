import { TeacherDto } from './../model/teacher-dto';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'teacherFilter'
})
export class TeacherFilterPipe implements PipeTransform {

  transform(value: TeacherDto[], name:string,email:String): unknown {
    if(!value || ( !name && !email)){
      return value;
    }
   
    else if(!email && name){
      return value.filter(s=>
        s.name.toLowerCase().indexOf(name.toLowerCase()) !== -1
      )
    }
    else if( email && !name){
      return value.filter(s=>
        s.email.toLowerCase().indexOf(email.toLowerCase()) !== -1
      )
    }
    else if(  name && email){
      return value.filter(s=>
        (s.name.toLowerCase().indexOf(name.toLowerCase()) !== -1) 
        && (s.email.toLowerCase().indexOf(email.toLowerCase()) !== -1)
      )
    }
  }

}
