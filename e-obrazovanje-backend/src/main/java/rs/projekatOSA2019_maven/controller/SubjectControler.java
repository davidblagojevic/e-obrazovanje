package rs.projekatOSA2019_maven.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import rs.projekatOSA2019_maven.dto.ExamDto;

import rs.projekatOSA2019_maven.dto.SubjectDto;
import rs.projekatOSA2019_maven.entity.Authority;
import rs.projekatOSA2019_maven.entity.Department;
import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.entity.Teacher;
import rs.projekatOSA2019_maven.service.DepartmentService;
import rs.projekatOSA2019_maven.service.ExamService;
import rs.projekatOSA2019_maven.service.StudentService;
import rs.projekatOSA2019_maven.service.SubjectService;
import rs.projekatOSA2019_maven.service.TeacherService;
import rs.projekatOSA2019_maven.service.UserService;

@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value = "api/subjects")
public class SubjectControler {
	
	@Autowired
	ExamService examService;

	@Autowired
	StudentService studentService;
	@Autowired
	SubjectService subjectService;
	@Autowired
	UserService userService;
	@Autowired
	TeacherService teacherService;

	

	@GetMapping(value = "getSubject/{id}",produces ="application/json")
	public ResponseEntity<List<SubjectDto>> getSubjectFromTeacher(@PathVariable Integer id) {
		System.out.println("exam---1------"+id);

		
		List<Subject> subjects = subjectService.findByTeacher(id);
		List<SubjectDto> subjectsDtoArrayList = new ArrayList<SubjectDto>();
		for (Subject subject : subjects) {
			subjectsDtoArrayList.add(new SubjectDto(subject));
		}
		System.out.println("exam--2-------"+subjects.size());

		return new ResponseEntity<>(subjectsDtoArrayList, HttpStatus.OK);
	}
	
	
//	
//	//@PreAuthorize("hasRole('ROLE_ADMIN')")
//	@GetMapping
//	public ResponseEntity<List<SubjectDTO>> getSubject() {
//		List<Subject> subjects = subjectService.findAll();
//		List<SubjectDTO> examsDTO = new ArrayList<SubjectDTO>();
//		for (Subject subject : subjects) {
//			examsDTO.add(new SubjectDTO(subject));
//		}
//		return new ResponseEntity<>(examsDTO, HttpStatus.OK);
//	}

	@Autowired
	DepartmentService departmentService;
	
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "/{id}",produces ="application/json")
	public ResponseEntity<SubjectDto> getSubjectById(@PathVariable int id) {
		Subject subject = subjectService.findOne(id);
		if (subject == null)
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(new SubjectDto(subject), HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<SubjectDto>> getSubject() {
		List<Subject> subjects = subjectService.findAll();
		List<SubjectDto> examsDTO = new ArrayList<SubjectDto>();
		for (Subject subject : subjects) {
			examsDTO.add(new SubjectDto(subject));
		}
		return new ResponseEntity<>(examsDTO, HttpStatus.OK);
	}

//	
//	@GetMapping(value = "/{id}")
//	public ResponseEntity<SubjectDTO> getSubject(@PathVariable int id){
//		Subject subject=subjectService.findOne(id);
//		if(subject == null)
//			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		else
//			return new ResponseEntity<>(HttpStatus.OK);
//	}
//	
//	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping(value = "/{id}",consumes = "application/json")
	public ResponseEntity<SubjectDto> saveSubject(@RequestBody SubjectDto subjectDTO,@PathVariable int id) {
		Subject subject = new Subject();
		subject.setName(subjectDTO.getName());
		subject.setNumberOfECTSPoints(subjectDTO.getNumberOfECTSPoints());
		subject.setDateOfLaying(subjectDTO.getDateOfLaying());
		Department d=departmentService.findById(1);
		subject.setDepartment(d);
		
		
		Optional<Teacher> t=teacherService.findOne(id);
		t.get().getSubject().add(subject);
		subject.getTeacher().add(t.get());
		subjectService.save(subject);
		teacherService.save(t.get());
		return new ResponseEntity<>(new SubjectDto(subject), HttpStatus.CREATED);
	}
//
////	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<SubjectDto> updateSubject(@RequestBody SubjectDto subjectDTO, @PathVariable int id) {
		Subject subject = subjectService.findOne(id);
		if (subject == null)
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		
	
		subject.setName(subjectDTO.getName());
		subject.setNumberOfECTSPoints(subjectDTO.getNumberOfECTSPoints());
		subject.setDateOfLaying(subjectDTO.getDateOfLaying());
//		s.setListOfTeacher(subjectDTO.getListOfTeacher());	
		subjectService.save(subject);
		return new ResponseEntity<>( new SubjectDto(subject),HttpStatus.OK);
	}
//	
//
////	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> deleteSubject(@PathVariable Integer id) {
		Subject subject = subjectService.findOne(id);
		if (subject == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		
		subjectService.delete(subject);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
//
//	
//	@GetMapping(value = "/getStudentsForSubject/{id}")
//	public ResponseEntity<List<StudentDTO>> getStudentsForSubject(@PathVariable int id) {
//		
//		Subject subject = subjectService.findOne(id);
//		if (subject == null)
//			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		Subject s=subject;
//		List<StudentDTO>students=new ArrayList<StudentDTO>();
//		for (Student student : s.getStudents()) {
//			StudentDTO studentDTO=new StudentDTO(student);
//			students.add(studentDTO);
//		}
//		return new ResponseEntity<>(students,HttpStatus.OK);
//	}
//	
	@GetMapping(value = "/nameOfSubjects/{name}")
	public ResponseEntity<List<SubjectDto>> getSubjectByName(@PathVariable String name) {
		List<Subject> subjects = subjectService.findByNameContaining(name);
		List<SubjectDto> examsDTO = new ArrayList<SubjectDto>();
		for (Subject subject : subjects) {
			examsDTO.add(new SubjectDto(subject));
		}
		return new ResponseEntity<>(examsDTO, HttpStatus.OK);
	}
	

}
