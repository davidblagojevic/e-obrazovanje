package rs.projekatOSA2019_maven.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.projekatOSA2019_maven.dto.StudentDto;
import rs.projekatOSA2019_maven.entity.Authority;
import rs.projekatOSA2019_maven.entity.Department;
import rs.projekatOSA2019_maven.entity.FinancialCard;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.entity.User;
import rs.projekatOSA2019_maven.service.DepartmentService;
import rs.projekatOSA2019_maven.service.StudentService;
import rs.projekatOSA2019_maven.service.UserAuthorityService;
import rs.projekatOSA2019_maven.service.UserService;



@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value = "api/students")
public class StudentController {
//	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private UserAuthorityService userAuthorityService;
	
	
//	
//
	@GetMapping
	public ResponseEntity<List<StudentDto>> getAllStudents() {
		List<Student> students = studentService.findAll();
		List<StudentDto> studentsDTO = new ArrayList<StudentDto>();
		for (Student student : students) {
			studentsDTO.add(new StudentDto(student));
		}
		return new ResponseEntity<>(studentsDTO, HttpStatus.OK);
	}
	
	@GetMapping(value = "/fromUser") 
	public ResponseEntity<StudentDto> getTeacher(Principal p) {
		User u=userService.findByUsername(p.getName());
		if(u.getStudent() == null)
			return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(new StudentDto(u.getStudent()), HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<StudentDto> getStudents( @PathVariable int id) {
		Student s=studentService.findById(id);
		
		if(s == null)
			return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(new StudentDto(s), HttpStatus.OK);
	}
//	
//	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping( value = "/{id}",consumes = "application/json")
	public ResponseEntity<StudentDto> savestudent(@RequestBody StudentDto student1,@PathVariable long id) {
		Student s=new Student();
		Set<Subject> students = new HashSet<Subject>();
		User u=new User();
		u.setUsername(student1.getUser().getUsername());
		u.setPassword(student1.getUser().getPassword());
		
		u.setActivated(true);
		//
		Authority authority = userAuthorityService.findByName("ROLE_STUDENT");
		u.getAuthorities().add(authority);
		u = userService.save(u);
		
		Department d=departmentService.findById(id);
		for (Subject subject : d.getSubjects()) {
			students.add(subject);
		}
		System.out.println("sdfsdfgfgdfgdsdasdas");
		System.out.println(students.size());
		
		s.setSubjects(students);
		s.setDepartment(d);
		FinancialCard f=new FinancialCard();
		f.setBankAccount(student1.getFinancialCard().getBankAccount());
		f.setCardNumber(student1.getFinancialCard().getCardNumber());
		f.setSum(student1.getFinancialCard().getSum());
		f.setModelNumber(student1.getFinancialCard().getModelNumber());
		
		s.setName(student1.getName());
		s.setLastName((student1.getLastName()));
		s.setDateOfBirth(student1.getDateOfBirth());
		s.setIndex(student1.getIndex());
		s.setYearOfEnrollment(1);
		s.setYearStudy(1);
		s.setPlaceOfBirth("Srbija");
		s.setCountryOfBirth(student1.getCountryOfBirth());
		s.setGender(student1.getGender());
		s.setAddress("adresa");
		s.setWayOfFinancing(student1.getWayOfFinancing());
		s.setTotalECTSPoints(0);
		s.setEmail(student1.getEmail());
		s.setAverageRating(0);
		s.setOrdinalNumberOfEntries("");
		s.setUser(u);
		s.setFinancialCard(f);
		s.addUser(u);
		s=studentService.save(s);
		
		
		
		
		return new ResponseEntity<>(new StudentDto(s), HttpStatus.CREATED);
	}
//	
	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<StudentDto> putstudent(@RequestBody StudentDto student1, @PathVariable int id) {
		Student s=studentService.findById(id);
		
		if(s == null)
			return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
		s.setName(student1.getName());
		s.setLastName((student1.getLastName()));
		s.setDateOfBirth(student1.getDateOfBirth());
		s.setIndex(student1.getIndex());
		s.setYearOfEnrollment(student1.getYearOfEnrollment());
		s.setYearStudy(student1.getYearStudy());
		s.setPlaceOfBirth(student1.getPlaceOfBirth());
		s.setCountryOfBirth(student1.getCountryOfBirth());
		s.setDateOfBirth(student1.getDateOfBirth());
		s.setPlaceOfBirth(student1.getPlaceOfBirth());
		s.setAddress(student1.getAddress());
		s.setWayOfFinancing(student1.getWayOfFinancing());
		s.setTotalECTSPoints(student1.getTotalECTSPoints());
		s.setEmail(student1.getEmail());
		s.setAverageRating(student1.getAverageRating());
		s.setOrdinalNumberOfEntries(student1.getOrdinalNumberOfEntries());
		studentService.save(s);
		
		return new ResponseEntity<>(new StudentDto(s), HttpStatus.CREATED);

	}
//	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> deleteStudent(@PathVariable int id) {
		Student s = studentService.findById(id);
		if (s != null) {
			
			studentService.delete(s);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
//	
	@GetMapping(value = "/searchByIndex/{index}")
	public ResponseEntity<StudentDto> getStudentByIndex(@PathVariable String index){
		
		Student s=studentService.findByIndex(index);
		if(s == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		else
			return new ResponseEntity<>(new StudentDto(s),HttpStatus.OK);
		
	}
//	
//	@GetMapping(value = "/searchByName/{name}")
//	public ResponseEntity<List<StudentDTO>> getStudentByName(@PathVariable String name){
//		
//		List<Student> s=studentService.findByNameContaining(name);
//		List<StudentDTO>studentDTOList=new ArrayList<StudentDTO>();
//		if(s == null)
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		else {
//			for (Student student : s) {
//				StudentDTO sDTO=new StudentDTO(student);
//				studentDTOList.add(sDTO);
//			}
//			
//			return new ResponseEntity<>(studentDTOList,HttpStatus.OK);
//		}
//		
//	}
//	
//	@GetMapping(value = "/subjects/{id}")
//	public ResponseEntity<List<SubjectDTO>> getStudentsSubject(@PathVariable String id){
//		
//		List<SubjectDTO>subjectList=new ArrayList<SubjectDTO>();
//		Student student = studentService.findByIndex(id);
//		if (student != null) {
//			for (Subject sub : student.getSubjects()) {
//				SubjectDTO subDTO=new SubjectDTO(sub);
//				subjectList.add(subDTO);
//			}
//			return new ResponseEntity<>(subjectList,HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//	}
//		
//		@PostMapping(value = "/addStudentOnSubject/{index}/{id}")
//		public ResponseEntity<Void> addStudentsOnSubject(@PathVariable String index , @PathVariable int id){
//			Exam exam=examService.findById(id);
//			if(exam == null)
//				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//			else {
//				Student student = studentService.findByIndex(index);
//				
//				if (student != null) {
//					for (Student st : exam.getStudents()) {
//						if(st.getIndex().equals(student.getIndex()))
//							return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//						else {
//							exam.getStudents().add(student);
//							
//							}
//						}
//					return new ResponseEntity<>(HttpStatus.OK);
//					}
//				else
//					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//			}
//		}
//		
//		@GetMapping(value = "/studentFailsExams/{index}")
//		public ResponseEntity<List<ExamDTO>> getStudentsFailsExams(@PathVariable String index){
//		
//			Student s=studentService.findByIndex(index);
//			if(s == null)
//				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//			else {
//				List<ExamDTO>examDTOs=new ArrayList<ExamDTO>();
//				for (Exam e : s.getExams()) {
//					if(e.getPass()==false) {
//						examDTOs.add(new ExamDTO(e));
//						//return new ResponseEntity<>(examDTOs ,HttpStatus.OK);
//					}
//				}
//				return new ResponseEntity<>(examDTOs ,HttpStatus.OK);
//			}
//			
//		}
//		
//		@GetMapping(value = "/studentPassExams/{index}")
//		public ResponseEntity<List<ExamDTO>> getStudentsPassExams(@PathVariable String index){
//		
//			Student s=studentService.findByIndex(index);
//			if(s == null)
//				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//			else {
//				List<ExamDTO>examDTOs=new ArrayList<ExamDTO>();
//				for (Exam e : s.getExams()) {
//					if(e.getPass()==true) 
//						examDTOs.add(new ExamDTO(e));
////					return new ResponseEntity<>(examDTOs ,HttpStatus.OK);
//					
//				}
//				return new ResponseEntity<>(examDTOs ,HttpStatus.OK);
//			}
//			
//		}
//		
//		@GetMapping(value = "/studentsDocuments/{index}")
//		public ResponseEntity<List<DocumentDTO>> getStudentsDocuments(@PathVariable String index){
//			
//			Student s=studentService.findByIndex(index);
//			if(s == null)
//				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//			else {
//				List<DocumentDTO>documentDTOs=new ArrayList<DocumentDTO>();
//				for (Document document : s.getDocuments()) {
//					documentDTOs.add(new DocumentDTO(document));
//					
//				}
//				return new ResponseEntity<>(documentDTOs,HttpStatus.OK);
//			}
//			
//		}
//		
//		@GetMapping(value = "/studentsTransactions/{index}")
//		public ResponseEntity<List<TransactionDTO>> getStudentsTransactions(@PathVariable String index){
//			
//			Student s=studentService.findByIndex(index);
//			if(s == null)
//				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//			else {
//				List<TransactionDTO>documentDTOs=new ArrayList<TransactionDTO>();
//				for (Transaction document : s.getTransactions()) {
//					documentDTOs.add(new TransactionDTO(document));
//					
//				}
//				return new ResponseEntity<>(documentDTOs,HttpStatus.OK);
//			}
//			
//		}
	
		
}
