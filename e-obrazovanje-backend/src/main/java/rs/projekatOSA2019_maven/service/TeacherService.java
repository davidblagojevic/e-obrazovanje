package rs.projekatOSA2019_maven.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.Teacher;
import rs.projekatOSA2019_maven.repository.TeacherRepository;
@Service
public class TeacherService implements TeacherServiceInterface {
	
	@Autowired
	TeacherRepository repo;

	@Override
	public void save(Teacher subject) {
		repo.save(subject);
		
	}

	@Override
	public List<Teacher> findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Teacher> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public void delete(Teacher s) {
		repo.delete(s);
		
	}

	@Override
	public List<Teacher> findByNameContaining(String infix) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Teacher> findOne(int id) {
		// TODO Auto-generated method stub
		return repo.findById(id);
	}
	

}
