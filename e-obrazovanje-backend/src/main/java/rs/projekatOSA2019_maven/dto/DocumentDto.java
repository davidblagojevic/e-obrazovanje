package rs.projekatOSA2019_maven.dto;

import rs.projekatOSA2019_maven.entity.Document;

public class DocumentDto {
	private String name;
	
	private String type;
	
	private String path;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public DocumentDto(String name, String type, String path) {
		super();
		this.name = name;
		this.type = type;
		this.path = path;
	}
	
	public DocumentDto(Document document) {
		this.name = document.getName();
		this.type = document.getType();
		this.path = document.getPath();
	}
	
	
	
	
}
