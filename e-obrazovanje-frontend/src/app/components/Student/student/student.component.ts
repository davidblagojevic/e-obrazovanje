import { StudentDto } from './../../../model/student-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentService } from './../../../services/student.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  students: any;
  studentsFilter: any;
  name: string = '';
  index: string = '';
  constructor(private service:StudentService,private router: Router,private route:ActivatedRoute) { }

  ngOnInit() {
    this.service.getAll()
        .subscribe(
        data => {
          this.students = data;
        }
      )
      console.log(this.students)
  }

  

  public getAllStudents() {
   let students= this.service.getAll();
      students.subscribe(
        data => {
          this.students = data;
        }
      )  
  }
  
  search(value: string): void {
    this.studentsFilter = this.students.filter((val) => val.name.toLowerCase().includes(value));
  }
  deleteTutorial() {
    const id=this.route.snapshot.paramMap.get("id")
    this.service.deleteStudent(id)
      .subscribe(
        response => {
          console.log(response);
          // this.router.navigate(['/tutorials']);
        },
        error => {
          console.log(error);
        });
  }

}
