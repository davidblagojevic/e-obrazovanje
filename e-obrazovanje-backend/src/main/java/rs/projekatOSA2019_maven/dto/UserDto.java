package rs.projekatOSA2019_maven.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



import rs.projekatOSA2019_maven.entity.Authority;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Teacher;
import rs.projekatOSA2019_maven.entity.User;
import rs.projekatOSA2019_maven.enums.TheRoleOfTheUser;

public class UserDto {

	
	
	private int id;
	private String username;
	private String password;
	private boolean activated;
	private List<AuthorityDto> authorities = new ArrayList<AuthorityDto>();
	private int idRole;

	
	
	
	
	
	
	public int getIdRole() {
		return idRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActivated() {
		return activated;
	}
	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	public List<AuthorityDto> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<AuthorityDto> authorities) {
		this.authorities = authorities;
	}

	public UserDto(User u) {
		super();
		this.id = u.getId();
		this.username = u.getUsername();
		this.password = u.getPassword();
		this.activated = u.isActivated();
		if(u.getAuthorities() != null) {
			for (Authority authority : u.getAuthorities()) {
				this.authorities.add(new AuthorityDto(authority));
			}
		}
		if(u.getStudent() != null) {
			this.idRole=u.getStudent().getId();
		}
		if(u.getTeacher() != null) {
			
			System.out.println("555555555555555555555ffdfdf"+u.getTeacher().getId());
			this.idRole=u.getTeacher().getId();
		}

	}
	public UserDto() {
		super();
	}

	
	
	
}
