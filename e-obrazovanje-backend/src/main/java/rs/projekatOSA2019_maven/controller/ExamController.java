package rs.projekatOSA2019_maven.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;

import rs.projekatOSA2019_maven.dto.ColloquiumDto;
import rs.projekatOSA2019_maven.dto.ExamDto;

import rs.projekatOSA2019_maven.dto.ExaminationPeriodDto;
import rs.projekatOSA2019_maven.dto.StudentDto;
import rs.projekatOSA2019_maven.dto.SubjectDto;
import rs.projekatOSA2019_maven.entity.Colloquium;

import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.ExaminationPeriod;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Teacher;
import rs.projekatOSA2019_maven.enums.ExamStatus;
import rs.projekatOSA2019_maven.repository.StudentRepository;
import rs.projekatOSA2019_maven.repository.UserRepository;
import rs.projekatOSA2019_maven.service.ExamService;
import rs.projekatOSA2019_maven.service.ExaminationPeriodService;
import rs.projekatOSA2019_maven.service.StudentService;
import rs.projekatOSA2019_maven.service.SubjectService;
import rs.projekatOSA2019_maven.service.TeacherService;



@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value = "api/exams")
public class ExamController {
	
	@Autowired
	ExamService examService;
	
	@Autowired
	ExaminationPeriodService examinationPeriodService;
	
	@Autowired
	TeacherService teacher; 
	
	@Autowired
	StudentRepository studentRepository;
	
	
	@Autowired
	StudentService studentService;
	
	@Autowired
	UserRepository serRepository;
	
	@Autowired
	SubjectService subjectService;
	
	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<ExamDto>> getExams() {
		
		
		
		
		
		System.out.println("stigo11");
		List<Exam> exams = examService.getExamsFromUser(1);
//		Date currentDate = Calendar.getInstance().getTime();



		
		
		List<ExamDto> examDtosList = new ArrayList<>();
		
			
			for(Exam te:exams) {
				System.out.println("exam---------"+te);

				
				ExamDto  examDtos = new ExamDto();
				examDtos.setId(te.getId());
				examDtos.setDateOfLaying(te.getDateOfLaying());
				examDtos.setDateOfApplication(te.getDateOfApplication());
				examDtosList.add(examDtos);
				
				
			}

			System.out.println(exams);
			
			return new ResponseEntity<>(examDtosList, HttpStatus.OK);	
			
		
		
	}
	
	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "getUsersUnsettledExams/{id}",produces ="application/json")
	public ResponseEntity<List<ExamDto>> getUsersUnsettledExams(@PathVariable int id) {
		
		System.out.println("getUsersUnsettledExams");
	
		
		
		List<Exam> exams = examService.getExamsFromUser(id);
		
		List<ExamDto> examDtosList = new ArrayList<>();

		for(Exam te:exams) {
			System.out.println("exam---------"+te);

			
			ExamDto  examDtos = new ExamDto();
			

			SubjectDto subjectDto= new SubjectDto();

			
			subjectDto.setId(te.getSubject().getId());
			subjectDto.setName(te.getSubject().getName());

			

			examDtos.setSubjectDto(subjectDto);
			examDtos.setId(te.getId());
			examDtos.setDateOfLaying(te.getDateOfLaying());
			examDtos.setDateOfApplication(te.getDateOfApplication());
			examDtos.setAssessment(te.getAssessment());
			
			
			if(te.getColloquium() != null) {
				for (Colloquium c : te.getColloquium()) {
					examDtos.getColloquium().add(new ColloquiumDto(c));
				}
			}
			examDtos.setPrice(te.getPrice());
			examDtos.setPointsTheory(te.getPointsTheory());
			examDtosList.add(examDtos);
			
			
			
		}
		
		
	
		return new ResponseEntity<>(examDtosList, HttpStatus.OK);
		
	}
	
	

	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "getNotPassedExamFromUser/{id}",produces ="application/json")
	public ResponseEntity<List<ExamDto>> getNotPassedExamFromUser(@PathVariable int id) {
		
		System.out.println("getNotPassedExamFromUser");
		List<Exam> exams = examService.getNotPassedExamFromUser(id);
		
		List<ExamDto> examDtosList = new ArrayList<>();

		for(Exam te:exams) {

			ExamDto  examDtos = new ExamDto();
			

			SubjectDto subjectDto= new SubjectDto();

			
			subjectDto.setId(te.getSubject().getId());
			subjectDto.setName(te.getSubject().getName());

			

			examDtos.setSubjectDto(subjectDto);

			examDtos.setId(te.getId());
			examDtos.setDateOfLaying(te.getDateOfLaying());
			examDtos.setDateOfApplication(te.getDateOfApplication());
			examDtos.setAssessment(te.getAssessment());
			
			
			if(te.getColloquium() != null) {
				for (Colloquium c : te.getColloquium()) {
					examDtos.getColloquium().add(new ColloquiumDto(c));
				}
			}
			examDtos.setPrice(te.getPrice());
			examDtos.setPointsTheory(te.getPointsTheory());
			examDtosList.add(examDtos);
			
			
		}
	
		return new ResponseEntity<>(examDtosList, HttpStatus.OK);
		
	}
	
	
	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "PassedExamByUser/{id}",produces ="application/json")
	public ResponseEntity<List<ExamDto>> getUserWherePassedExam(@PathVariable int id) {
		
		System.out.println("PassedExamByUser  id - "+id);
		List<Exam> exams = examService.getExamsFromUserWherePassedExam(id);
		
		List<ExamDto> examDtosList = new ArrayList<>();

		for(Exam te:exams) {

			
			ExamDto  examDtos = new ExamDto();
			SubjectDto subjectDto= new SubjectDto();

			
			subjectDto.setId(te.getSubject().getId());
			subjectDto.setName(te.getSubject().getName());

			

			examDtos.setSubjectDto(subjectDto);
			examDtos.setId(te.getId());
			examDtos.setDateOfLaying(te.getDateOfLaying());
			examDtos.setDateOfApplication(te.getDateOfApplication());
			examDtos.setAssessment(te.getAssessment());
			
			
			if(te.getColloquium() != null) {
				for (Colloquium c : te.getColloquium()) {
					examDtos.getColloquium().add(new ColloquiumDto(c));
				}
			}
			examDtos.setPrice(te.getPrice());
			examDtos.setPointsTheory(te.getPointsTheory());
			examDtosList.add(examDtos);
			
			
			
			
			
		}
		
		return new ResponseEntity<>(examDtosList, HttpStatus.OK);
		
	}

	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	
	@GetMapping(value = "updateExamLaiddown/{id}")
	public  ResponseEntity<Object> updateExam(@PathVariable int id) {

		try {
			System.out.println("novo213");
			examService.updateExamLaiddown(id);
			}
			catch(Exception e) {
			 
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		return new ResponseEntity<>( HttpStatus.OK);
		
	}
	
	
	@GetMapping(value = "updateExamLaiddownOnNull/{id}")
	public  ResponseEntity<Object> updateExamOnNull(@PathVariable int id) {

		
		
		
		
		try {
			System.out.println("novo213");
			
			examService.updateExamLaiddownOnNull(id);
			}
		
		
			catch(Exception e) {
			 
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		

		
		return new ResponseEntity<>( HttpStatus.OK);
		
	}
	
	
	
	
	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "getExamById/{id}",produces ="application/json")
	public ResponseEntity<ExamDto> getExamById(@PathVariable long id) {
		
		


		ExamDto examDto = null;
			try {

				if(id == 0) {

					 return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
				}
				Exam exam = examService.findById(id);
				
				System.out.println("exam.getStudent()---------------"+exam.getStudent().getId());

				 
				examDto = new  ExamDto();

				
				StudentDto studentDto= new StudentDto();
				
				
				System.out.println("exam.getStudent()---------------"+exam.getStudent().getLastName());

//				studentDto.setId(exam.getStudent().getId());
//				studentDto.setLastName(exam.getStudent().getLastName());
//				studentDto.setName(exam.getStudent().getName());
//				studentDto.setIndex(exam.getStudent().getIndex());
				
				
//				examDto.getStudentDto1().setLastName(exam.getStudent().getLastName());
//			
//				examDto.getStudentDto1().setIndex(exam.getStudent().getIndex());
//				examDto.getStudentDto1().setName(exam.getStudent().getName());
				
				
//				examDto.getStudentDto1().setLastName(exam.getStudent().getLastName());

				
				examDto.setLastname(exam.getStudent().getLastName());
				examDto.setName(exam.getStudent().getName());
				examDto.setIndex(exam.getStudent().getIndex());
				

				
				examDto.setId(exam.getId());
				examDto.setAssessment(exam.getAssessment());
				examDto.setDateOfApplication(exam.getDateOfApplication());
				examDto.setPrice(exam.getPrice());
				examDto.setPointsTheory(exam.getPointsTheory());
				examDto.setDateOfLaying(exam.getDateOfLaying());
				examDto.setExercisePoints(exam.getExercisePoints());
				examDto.setLaiddown(exam.isLaiddown());
				
				
				
				
				
				
				
				
			}
			catch(Exception e) {
				System.out.println("HttpStatus.INTERNAL_SERVER_ERROR");

				return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(examDto, HttpStatus.OK);
	}

	
	
	

	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value="saveExam",method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<ExamDto> updateAndSave(@RequestBody ExamDto examDto){
	
		System.out.println("sdsdsd----"+examDto.getAssessment());
		System.out.println("sdsdsd----"+examDto.getDateOfApplication());
		System.out.println("sdsdsd----"+examDto.getDateOfLaying());
		System.out.println("sdsdsd----"+examDto.getAssessment());

		
		Exam exam = examService.findById(examDto.getId());
			
		
		if(examDto.getAssessment()>5) {
			exam.setExamStatus(ExamStatus.COMPLETED);
			
		}
		
			exam.setExercisePoints(examDto.getExercisePoints());
			exam.setPointsTheory(examDto.getPointsTheory());
			exam.setAssessment(examDto.getAssessment());
			
			
			examService.save(exam);
			

		
		
		return new ResponseEntity<>(examDto, HttpStatus.OK);

	}
	
	
	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "getExamfromPeriod/{id}/{id2}",produces ="application/json")
	public ResponseEntity<List<ExamDto>> getExamByExaminationnPeriod(@PathVariable Integer id,@PathVariable Integer id2) {
		System.out.println("subjekt---"+id);
		System.out.println("period----"+id2);

		
		List<ExamDto> examDtosList =new ArrayList<>();

			try {

				if(id == 0) {
					System.out.println("*****HttpStatus.NOT_FOUND*****");

					 return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
				}
				
				
				List<Exam> examList = examService.getExamFromPeriod(id,id2);
				

				for(Exam te:examList) {
					System.out.println("exam---------"+te);

					
					ExamDto  examDtos = new ExamDto();
					

					SubjectDto subjectDto= new SubjectDto();
					System.out.println("te.getSubject().getName()---------"+te.getSubject().getName());

					
					subjectDto.setId(te.getSubject().getId());
					subjectDto.setName(te.getSubject().getName());

					System.out.println("subjectDto.getName()---------"+subjectDto.getName());

					examDtos.setExercisePoints(te.getExercisePoints());
					
					examDtos.setLastname(te.getStudent().getLastName());
					examDtos.setName(te.getStudent().getName());
					examDtos.setIndex(te.getStudent().getIndex());
					
					examDtos.setSubjectDto(subjectDto);

					examDtos.setId(te.getId());
					examDtos.setDateOfLaying(te.getDateOfLaying());
					examDtos.setDateOfApplication(te.getDateOfApplication());
					examDtos.setAssessment(te.getAssessment());
					System.out.println("examDtosgetName()---------"+examDtos.getSubjectDto().getName());

					
					if(te.getColloquium() != null) {
						for (Colloquium c : te.getColloquium()) {
							examDtos.getColloquium().add(new ColloquiumDto(c));
						}
					}
					examDtos.setPrice(te.getPrice());
					examDtos.setPointsTheory(te.getPointsTheory());
					examDtosList.add(examDtos);
				}
				

			}
			catch(Exception e) {
				System.out.println("*****HttpStatus.INTERNAL_SERVER_ERROR*****");

				return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(examDtosList, HttpStatus.OK);
	}
	
	
	
	
	
	
	}





