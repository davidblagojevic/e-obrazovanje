package rs.projekatOSA2019_maven.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.repository.StudentRepository;

@Service
public class StudentService implements StudentServiceInterface {

	@Autowired
	StudentRepository repo;
	
	@Override
	public Student save(Student student) {
		repo.save(student);
		return student;
		
	}

	@Override
	public void delete(Student student) {
		repo.delete(student);
		
	}

	@Override
	public List<Student> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Student findById(int id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public Student findByIndex(String index) {
		// TODO Auto-generated method stub
		return repo.findByIndex(index);
	}

	@Override
	public List<Student> findByNameContaining(String infix) {
//		List<Student>students=repo.findByNameContaining(infix);
//		return students;
		return null;
	}

}
