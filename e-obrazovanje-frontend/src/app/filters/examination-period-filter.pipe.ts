import { Pipe, PipeTransform } from '@angular/core';
import { ExaminationPeriodDto } from '../model/examination-period-dto';

@Pipe({
  name: 'examinationPeriodFilter'
})
export class ExaminationPeriodFilterPipe implements PipeTransform {

  transform(value: ExaminationPeriodDto[], searchTerm1: string,searchTerm3:any,searchTerm2:any): any {
    console.log("searchTerm1"+searchTerm1)
    console.log("searchTerm2"+searchTerm2)
    console.log("searchTerm3"+searchTerm3)

    if(!value || (!searchTerm1 && !searchTerm2 && !searchTerm3)){
      return value;
  }
  if(searchTerm1 && !searchTerm2 && !searchTerm3){


    return value.filter(o => 
        (o.nameOfExamPeriod.toLowerCase().indexOf(searchTerm1.toLowerCase()) !== -1)  ||
        (o.id.toString().toLowerCase().indexOf(searchTerm1.toLowerCase()) !== -1) 

       );
      }
      else if(!searchTerm1 &&searchTerm2&&searchTerm3 ){
        
        return value.filter(o => 
            (!((new Date(searchTerm2) <= new Date(o.endOfExamPeriod) ||( new Date(o.endOfExamPeriod)<=  new Date(searchTerm3))) )));
    }

  }
  
}
