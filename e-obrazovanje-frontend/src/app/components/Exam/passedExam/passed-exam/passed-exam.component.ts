import { Component, OnInit } from '@angular/core';
import { ExamDto } from 'src/app/model/exam-dto';
import { ExamService } from 'src/app/services/exam.service';

@Component({
  selector: 'app-passed-exam',
  templateUrl: './passed-exam.component.html',
  styleUrls: ['./passed-exam.component.css']
})



export class PassedExamComponent implements OnInit {

  field;
  order;
  saerctSubijekt: string = "";
  exam: ExamDto;

  searchTerm2: string;
  searchTerm3: string;

  constructor(private serviceExam: ExamService) { }

  ngOnInit(): void {

    this.getPassedExamFromUser();


  }


  public getPassedExamFromUser() {

    let examPassed = this.serviceExam.getUserPassedExam(localStorage.getItem("idUser"));
    examPassed.subscribe(data => this.exam = data);


  }
  counter = 0;

  sort(param) {
    if (param === 'price') {
      this.field = 'price'
    }
    if (param === 'dateOfLaying') {
      this.field = 'dateOfLaying'
    }
    if (param === 'dateOfApplication') {
      this.field = 'dateOfApplication'
    }
    if (param === 'subject.name') {
      this.field = 'subject.name'
    }
    if (param === 'pointsTheory') {
      this.field = 'pointsTheory'
    }
    if (param === 'exercisePoints') {
      this.field = 'exercisePoints'
    }

    if (this.counter === 0) {
      //desc
      this.order = 'asc';
      this.counter = 1;
    } else if (this.counter === 1) {
      //asc
      this.order = 'desc';
      this.counter = 0;
    }
  }

}
