import { Pipe, PipeTransform } from '@angular/core';
import { ExamDto } from '../model/exam-dto';

@Pipe({
  name: 'examFilter'
})
export class ExamFilterPipe implements PipeTransform {


  transform(value: ExamDto[], sSubijekt: string,searchTerm3:any,searchTerm2:any): any {
    
    console.log("sSubijekt----"+sSubijekt)
    console.log("searchTerm3--"+searchTerm3)
    console.log("searchTerm2--"+searchTerm2)
    if(!value || (!sSubijekt && !searchTerm2 && !searchTerm3)){
      return value;
  }
  if(sSubijekt && !searchTerm2 && !searchTerm3){


    return value.filter(o => 
        // (o.subject.name.toLowerCase().indexOf(sSubijekt.toLowerCase()) !== -1)  ||
        (o.exercisePoints.toString().toLowerCase().indexOf(sSubijekt.toLowerCase()) !== -1) || 
        (o.id.toString().toLowerCase().indexOf(sSubijekt.toLowerCase()) !== -1) ||
        (o.pointsTheory.toString().toLowerCase().indexOf(sSubijekt.toLowerCase()) !== -1) ||
        (o.price.toString().toLowerCase().indexOf(sSubijekt.toLowerCase()) !== -1) 
        // (o.nameStud.toString().toLowerCase().indexOf(sSubijekt.toLowerCase()) !== -1) 

       );
      }
      else if(!sSubijekt &&searchTerm2&&searchTerm3 ){
        
        return value.filter(o => 
            (!((new Date(searchTerm2) <= new Date(o.dateOfLaying) ||( new Date(o.dateOfLaying)<=  new Date(searchTerm3))) )));
    }

  }
  
}





