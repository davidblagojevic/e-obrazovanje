import { SubjectDto } from './../../../model/subject-dto';
import { SubjectService } from './../../../services/subject.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TeacherService } from 'src/app/services/teacher.service';

@Component({
  selector: 'app-create-subject',
  templateUrl: './create-subject.component.html',
  styleUrls: ['./create-subject.component.css']
})
export class CreateSubjectComponent implements OnInit {

  createSubject: FormGroup;
  teachers:any;
  constructor(private service:SubjectService,
    private fb: FormBuilder,
    private router: Router,
    private route:ActivatedRoute,private serviceT:TeacherService) { }

    get name() {
      return this.createSubject.get('name');
    }
  
    get numberOfECTSPoints() {
      return this.createSubject.get('numberOfECTSPoints');
    }
    get teacher() {
      return this.createSubject.get('teacher');
    }

  ngOnInit(): void {
    this.createSubject= this.fb.group({
      name: ['',Validators.required],
      numberOfECTSPoints: [0,Validators.required],
     // dateOfLaying: ['',Validators.required],
      teacher:['',Validators.required]
    });

    this.serviceT.getAll()
        .subscribe(
        data => {
          this.teachers = data;
        }
      )
    
  }

  createSub(){

    var subject=new SubjectDto(1,this.name.value,this.numberOfECTSPoints.value,new Date());
    console.log(subject)
    console.log(this.teacher.value)
    if(this.validation(subject)){
      this.service.create(subject,this.teacher.value);
      this.router.navigate(['/SubjectComponent']);
    }
  }
  validation(subject:SubjectDto){
    if( subject.name==='' || subject.name.length < 3){
      alert("ime predmeta je prazno ili je manje od 3 slova");
      return false;
    }
    else if( subject.numberOfECTSPoints <= 0){
      alert("broj esp bodova ne sme biti manji od 0");
      return false;
    }
    else if( this.teacher.value===''){
      alert("teacher ne sme biti prazan");
      return false;
    }
    else{
      return true;
    }

  }


}
