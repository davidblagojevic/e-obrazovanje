package rs.projekatOSA2019_maven.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rs.projekatOSA2019_maven.entity.Department;

public interface DepartmentRepository extends JpaRepository<Department, Integer>{
	
	Department findById(long id);

}
