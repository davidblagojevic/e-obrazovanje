import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectFromTeacherComponent } from './subject-from-teacher.component';

describe('SubjectFromTeacherComponent', () => {
  let component: SubjectFromTeacherComponent;
  let fixture: ComponentFixture<SubjectFromTeacherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubjectFromTeacherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectFromTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
