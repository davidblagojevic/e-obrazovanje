import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PassedExamComponent } from './passed-exam.component';

describe('PassedExamComponent', () => {
  let component: PassedExamComponent;
  let fixture: ComponentFixture<PassedExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PassedExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PassedExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
