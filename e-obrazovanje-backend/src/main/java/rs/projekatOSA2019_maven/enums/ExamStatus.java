package rs.projekatOSA2019_maven.enums;

public enum ExamStatus {

	REGISTERED,
	COMPLETED
}
