package rs.projekatOSA2019_maven.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.projekatOSA2019_maven.dto.DepartmentDto;
import rs.projekatOSA2019_maven.entity.Department;
import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.service.DepartmentService;

@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value = "api/departments")
public class DepartmentController {
	@Autowired
	DepartmentService departmentService;
	
	
	@GetMapping()
	public ResponseEntity<List<DepartmentDto>> getAll(){
		List<Department> departments = departmentService.findAll();
		List<DepartmentDto> departmentDtos = new ArrayList<DepartmentDto>();
		
		for (Department department : departments) {
			DepartmentDto departmentDto = new DepartmentDto(department);
			departmentDto.addSubjectDtos(department.getSubjects());
			departmentDtos.add(new DepartmentDto(department));
		}
		
		return new ResponseEntity<>(departmentDtos, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<DepartmentDto> getById(@PathVariable long id){
		Department department = departmentService.findById(id);
		if (department == null) {
			return new ResponseEntity<>(null ,HttpStatus.NOT_FOUND);
		}
		DepartmentDto departmentDto = new DepartmentDto(department);
		departmentDto.addSubjectDtos(department.getSubjects());
		return new ResponseEntity<DepartmentDto>(departmentDto,HttpStatus.OK);
	}
	
	
}
