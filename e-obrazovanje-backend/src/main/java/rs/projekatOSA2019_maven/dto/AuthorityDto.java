package rs.projekatOSA2019_maven.dto;

import rs.projekatOSA2019_maven.entity.Authority;

public class AuthorityDto {
	
	 private String name;
	 
	 public AuthorityDto(Authority a) {
		 this.name=a.getName();
	 }

	   public String getName() {
	      return name;
	   }

	   public void setName(String name) {
	      this.name = name;
	   }

	public AuthorityDto() {
		super();
	}
	   

}
