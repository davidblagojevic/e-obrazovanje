package rs.projekatOSA2019_maven.service;

import java.util.List;
import java.util.Optional;

import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Teacher;

public interface TeacherServiceInterface {
	
public void save(Teacher subject);
	
	List<Teacher> findByName(String name);
	
	Optional<Teacher> findOne(int id);
	
	List<Teacher> findAll();
	public void delete(Teacher s);
	
	
	List<Teacher> findByNameContaining(String infix);

}
