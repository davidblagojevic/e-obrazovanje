--Dodati fieldovi:
--  1.Finansijska kartica - ziroRacun,pozivNaBroj,brojModela
--  2.Ucenik - drzavaRodjenja,mestoRodjenja,datumRodjenja,pol,nacinFinansiranja,email,adresa,ukupnoECTSBodova,prosecnaOcena
--  3.Smer - brojECTSBodova
--  4.Predmet - brojECTSBodova
--  5.Nastavnik - /
--  6.Korisnik - /
--  7.Kolokvijum - /
--  8.IspitniRok - /
--  9.Ispit - /
--  10.Dokument - /
--

select * from user;

select * from financial_card;
INSERT INTO `e_obrazovanje`.`financial_card` (`id`, `card_number`, `model_number`, `reference_number`, `sum`, `bank_account`) VALUES ('1', '001', '97', '1111111', '1500', '1111111');
INSERT INTO `e_obrazovanje`.`financial_card` (`id`, `card_number`, `model_number`, `reference_number`, `sum`, `bank_account`) VALUES ('2', '002', '97', '22222', '1200', '2222222');
INSERT INTO `e_obrazovanje`.`financial_card` (`id`, `card_number`, `model_number`, `reference_number`, `sum`, `bank_account`) VALUES ('3', '003', '97', '33333', '500', '3333333');
INSERT INTO `e_obrazovanje`.`financial_card` (`id`, `card_number`, `model_number`, `reference_number`, `sum`, `bank_account`) VALUES ('4', '004', '97', '44444444', '4500', '444444');
INSERT INTO `e_obrazovanje`.`financial_card` (`id`, `card_number`, `model_number`, `reference_number`, `sum`, `bank_account`) VALUES ('5', '005', '97', '55555555', '1500', '555555555');



select * from department;
INSERT INTO `e_obrazovanje`.`department` (`id`, `number_ofectspoints`, `name`, `department_mark`) VALUES ('1', '180', 'Softverske i informacione tehnologije', '1');
INSERT INTO `e_obrazovanje`.`department` (`id`, `number_ofectspoints`, `name`, `department_mark`) VALUES ('2', '240', 'Saobracaj', '2');
INSERT INTO `e_obrazovanje`.`department` (`id`, `number_ofectspoints`, `name`, `department_mark`) VALUES ('3', '240', 'Softversko inzenjerstvo i informacione tehnologije', '3');

select * from student;
INSERT INTO `e_obrazovanje`.`student` (`id`, `address`, `date_of_birth`, `country_of_birth`, `email`, `year_study`, `year_of_enrollment`, `name`, `indexx`, `place_of_birth`, `way_of_financing`, `gender`, `last_name`, `average_rating`, `totalectspoints`, `ordinal_number_of_entries`, `department_id`, `financial_card_id` ) VALUES ('1', 'Prote G. Babovica 33', '1231345', 'Srbija', 'ucenik1@gmail.com', '1', '2019', 'Jovana', 'SF01-2019', 'Sabac', 'Budzet', '0', 'Jovanovic', '0', '0', '3', 1, 1);
INSERT INTO `e_obrazovanje`.`student` (`id`, `address`, `date_of_birth`, `country_of_birth`, `email`, `year_study`, `year_of_enrollment`, `name`, `indexx`, `place_of_birth`, `way_of_financing`, `gender`, `last_name`, `average_rating`, `totalectspoints`, `ordinal_number_of_entries`, `department_id`, `financial_card_id`) VALUES ('2', 'Sumadijske divizije 5', '1231345', 'Srbija', 'ucenik2@gmail.com', '1', '2019', 'Jovan', 'SF02-2019', 'Sabac', 'Budzet', '0', 'Radakovic', '0', '0', '3', 2, 2);
INSERT INTO `e_obrazovanje`.`student` (`id`, `address`, `date_of_birth`, `country_of_birth`, `email`, `year_study`, `year_of_enrollment`, `name`, `indexx`, `place_of_birth`, `way_of_financing`, `gender`, `last_name`, `average_rating`, `totalectspoints`, `ordinal_number_of_entries`, `department_id`, `financial_card_id`) VALUES ('3', 'Masarikova 8', '5132435', 'Srbija', 'ucenik3@gmail.com', '1', '2019', 'Stevan', 'SF03-2019', 'Bac', 'Samofinansiranje', '0', 'Stevanovic', '0', '0', '3', 3, 3);
INSERT INTO `e_obrazovanje`.`student` (`id`, `address`, `date_of_birth`, `country_of_birth`, `email`, `year_study`, `year_of_enrollment`, `name`, `indexx`, `place_of_birth`, `way_of_financing`, `gender`, `last_name`, `average_rating`, `totalectspoints`, `ordinal_number_of_entries`, `department_id`, `financial_card_id`) VALUES ('4', 'Bate Koena 3', '1231345', 'Srbija', 'ucenik4@gmail.com', '1', '2019', 'Mitar', 'SA01-2019', 'Novi Sad', 'Budzet', '0', 'Mitrovic', '0', '0', '3', 2, 4);
INSERT INTO `e_obrazovanje`.`student` (`id`, `address`, `date_of_birth`, `country_of_birth`, `email`, `year_study`, `year_of_enrollment`, `name`, `indexx`, `place_of_birth`, `way_of_financing`, `gender`, `last_name`, `average_rating`, `totalectspoints`, `ordinal_number_of_entries`, `department_id`, `financial_card_id`) VALUES ('5', 'Fruskogorska 5', '423423423', 'Srbija', 'ucenik5@gmail.com', '1', '2019', 'Mita', 'SA02-2019', 'Novi Sad', 'Samofinansiranje', '0', 'Popovic', '0', '0', '3', 3, 5);


select * from teacher;
INSERT INTO `e_obrazovanje`.`teacher` (`id`, `email`, `name`, `last_name`, `the_role_of_the_teacher`,  `department_id`) VALUES ('1', 'vlajko@gmail.com', 'Vlajko', 'Vlajkovic', '0', 1);
INSERT INTO `e_obrazovanje`.`teacher` (`id`, `email`, `name`, `last_name`, `the_role_of_the_teacher`,  `department_id`) VALUES ('2', 'joca22@gmail.com', 'Jovana', 'Stevanovic', '0', 2);
INSERT INTO `e_obrazovanje`.`teacher` (`id`, `email`, `name`, `last_name`, `the_role_of_the_teacher`,  `department_id`) VALUES ('3', 'mirko@gmail.com', 'Mirko', 'Mirkovic', '1', 3);

INSERT INTO `e_obrazovanje`.`user` (`id`, `username`, `password`) VALUES ('1', 'admin', '$2a$10$sKxBhtetMtntR9wpSNduLORFT962tF3mpiVLufSzmy15AZWDIYnoG');
INSERT INTO `e_obrazovanje`.`user` (`id`, `username`, `password`, `student_id`) VALUES ('2', 'ucenik1', '$2a$10$D6585yGDQvTpHFIWIWIBNewK02XtXyTCxlty76TxiI4jPnCC3buWy', '1');
INSERT INTO `e_obrazovanje`.`user` (`id`, `username`, `password`, `student_id`) VALUES ('3', 'ucenik2', '$2a$10$BrLfsll5rZlA5Zo7pLyfpuSPuViEE4bje.OF2EKMeZ69iXGB3p0iu', '2');
INSERT INTO `e_obrazovanje`.`user` (`id`, `username`, `password`, `student_id`) VALUES ('4', 'ucenik3', '$2a$10$z7WK4y1CyjdBlcjhWIIeOu3lex2iRSaEnAbEN1Dxaz/xPL45NEfQG', '3');
INSERT INTO `e_obrazovanje`.`user` (`id`, `username`, `password`, `student_id`) VALUES ('5', 'ucenik4', '$2a$10$UKYa/Qh/y9fkQxHsMdZxkOLDXElM6UftfJZpsd2q63AFQoVSEpTWe', '4');
INSERT INTO `e_obrazovanje`.`user` (`id`, `username`, `password`, `student_id`) VALUES ('6', 'ucenik5', '$2a$10$9uKsy9B7tK.9xoQfNwsDZueALFrq1f6iOHSJ/WUzsjn6FvlHKofI6', '5');
INSERT INTO `e_obrazovanje`.`user` (`id`, `username`, `password`, `teacher_id`) VALUES ('7', 'nastavnik1', '$2a$10$b1Ki5MghGSzoZsvXQtcWIuRqdlLXumuFIinB02who2pE5A.ETUEpW', '1');
INSERT INTO `e_obrazovanje`.`user` (`id`, `username`, `password`, `teacher_id`) VALUES ('8', 'nastavnik2', '$2a$10$PashE8IpUNJDbRrja3YSvOWq3.OGevt/EvMhTpy6iN.wXLcLhPiuK', '2');
INSERT INTO `e_obrazovanje`.`user` (`id`, `username`, `password`, `teacher_id`) VALUES ('9', 'nastavnik3', '$2a$10$X72Kzm1TUkfRcIW2iYP4QeI7UXih7AuwbnN27qSSY/Z4E3Z9WVx76','3');

select * from subject;
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('1', '6', 'Osnove Racunara', '2017-06-12',1);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('2', '4', 'Engleski jezik 1', '2017-06-12', 2);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('3', '8', 'Matematika 1', '2017-06-12', 3);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('4', '8', 'Matematika 2', '2017-06-12', 1);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('5', '5', 'Uvod u razvoj softvera', '2017-06-12', 2);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('6', '4', 'Menadzment', '2017-06-12', 3);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('7', '8', 'Osnove programiranja', '2017-06-12', 1);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('8', '8', 'Objektno-orijentisano progrmairanje', '2017-06-12', 2);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('9', '4', 'Engleski jezik 2', '2017-06-12', 3);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('10', '5', 'Sistemski Softver', '2017-06-12', 1);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('11', '7', 'Matematicka analiza 1', '2017-06-12', 2);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('12', '7', 'Matematicka analiza 2', '2017-06-12', 3);
INSERT INTO `e_obrazovanje`.`subject` (`id`, `number_ofectspoints`, `name`, `date_of_laying`, `department_id`) VALUES ('13', '8', 'Fizika', '2017-06-12', 1);
--
--select * from smer_predmet;
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('1', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('2', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('3', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('4', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('5', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('6', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('7', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('8', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('9', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('10', '1');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('11', '2');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('12', '2');
--INSERT INTO `tseo2k20`.`smer_predmet` (`predmet_id`, `smer_id`) VALUES ('13', '2');
--
select * from attends;
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('1', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('2', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('3', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('4', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('5', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('6', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('7', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('8', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('9', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('10', '1');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('1', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('2', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('3', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('4', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('5', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('6', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('7', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('8', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('9', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('10', '2');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('1', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('2', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('3', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('4', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('5', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('6', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('7', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('8', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('9', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('10', '3');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('11', '4');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('12', '4');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('13', '4');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('11', '5');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('12', '5');
INSERT INTO `e_obrazovanje`.`attends` (`subject_id`, `student_id`) VALUES ('13', '5');

select * from surrender;
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('7', '7');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('8', '7');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('8', '7');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('8', '7');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('8', '7');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('8', '7');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('8', '7');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('8', '7');

select * from surrender;
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('7', '1');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('8', '1');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('1', '1');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('2', '1');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('3', '1');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('4', '1');




INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('5', '3');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('6', '3');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('9', '3');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('10', '3');


INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('11', '2');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('12', '2');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('13', '2');
INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('14', '2');

--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('1', '1');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('2', '1');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('3','1');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('4','1');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('5','1');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('6','1');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('7','1');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('8','1');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('9','2');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('10','2');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('11','2');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('12','3');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('13','3');
--INSERT INTO `e_obrazovanje`.`surrender` (`subject_id`, `teacher_id`) VALUES ('14','3');




select * from examination_period;
INSERT INTO `e_obrazovanje`.`examination_period` (`id`, `end_of_exam_period`, `name_of_exam_period`, `the_beginning_of_the_examination_period`, `examination_period_status`,`examination_period_valid`) VALUES ('1', '2020-11-21', '1', '2021-11-30',0,0);
INSERT INTO `e_obrazovanje`.`examination_period` (`id`, `end_of_exam_period`, `name_of_exam_period`, `the_beginning_of_the_examination_period`, `examination_period_status`,`examination_period_valid`) VALUES ('2', '2020-02-04', '2', '2020-02-20',0,0);
INSERT INTO `e_obrazovanje`.`examination_period` (`id`, `end_of_exam_period`, `name_of_exam_period`, `the_beginning_of_the_examination_period`, `examination_period_status`,`examination_period_valid`) VALUES ('3', '2020-03-22', '3', '2020-03-29',0,0);
INSERT INTO `e_obrazovanje`.`examination_period` (`id`, `end_of_exam_period`, `name_of_exam_period`, `the_beginning_of_the_examination_period`, `examination_period_status`,`examination_period_valid`) VALUES ('4', '2020-04-22', '4', '2020-04-29',0,0);
INSERT INTO `e_obrazovanje`.`examination_period` (`id`, `end_of_exam_period`, `name_of_exam_period`, `the_beginning_of_the_examination_period`, `examination_period_status`,`examination_period_valid`) VALUES ('5', '2020-05-22', '5', '2020-05-29',0,0);

select * from exam;
INSERT INTO `e_obrazovanje`.`exam` (`id`, `points_theory`, `exercise_points`, `price`, `date_of_application`, `assessment`, `laiddown`, `examination_period_id`, `subject_id`, `student_id`,`exam_status`,`date_Of_Laying`) VALUES ('1', '19', '34', '200', '2017-06-15', '5', 0, '1', '7', '1',0, '2017-06-18');
INSERT INTO `e_obrazovanje`.`exam` (`id`, `points_theory`, `exercise_points`, `price`, `date_of_application`, `assessment`, `laiddown`, `examination_period_id`, `subject_id`, `student_id`,`exam_status`,`date_Of_Laying`) VALUES ('2', '59', '11', '200', '2017-07-5', '5', 0, '1', '8', '1',0, '2017-06-16');
INSERT INTO `e_obrazovanje`.`exam` (`id`, `points_theory`, `exercise_points`, `price`, `date_of_application`, `assessment`, `laiddown`, `examination_period_id`, `subject_id`, `student_id`,`exam_status`,`date_Of_Laying`) VALUES ('3', '40', '8', '200', '2017-04-1', '6', 0, '3', '1', '1',0, '2017-06-14');
INSERT INTO `e_obrazovanje`.`exam` (`id`, `points_theory`, `exercise_points`, `price`, `date_of_application`, `assessment`, `laiddown`, `examination_period_id`, `subject_id`, `student_id`,`exam_status`,`date_Of_Laying`) VALUES ('4', '6', '7', '200', '2017-04-2', '6', 0, '3', '1', '1',0, '2017-06-12');


select * from documents;
INSERT INTO `e_obrazovanje`.`documents` (`id`, `name`, `path`, `type`, `student_id`) VALUES ('1', 'dokument1', 'path', 'type', '1');
INSERT INTO `e_obrazovanje`.`documents` (`id`, `name`, `path`, `type`, `student_id`) VALUES ('2', 'dokument2', 'path', 'type', '1');


select * from authority;
INSERT INTO `e_obrazovanje`.`authority` (`name`) VALUES ('ROLE_ADMIN');
INSERT INTO `e_obrazovanje`.`authority` (`name`) VALUES ('ROLE_PROFESSOR');
INSERT INTO `e_obrazovanje`.`authority` (`name`) VALUES ('ROLE_STUDENT');



select * from user_authority;
INSERT INTO `e_obrazovanje`.`user_authority` (`user_id`, `authority_name`) VALUES ('1', 'ROLE_ADMIN');
INSERT INTO `e_obrazovanje`.`user_authority` (`user_id`, `authority_name`) VALUES ('2', 'ROLE_STUDENT');
INSERT INTO `e_obrazovanje`.`user_authority` (`user_id`, `authority_name`) VALUES ('3', 'ROLE_STUDENT');
INSERT INTO `e_obrazovanje`.`user_authority` (`user_id`, `authority_name`) VALUES ('4', 'ROLE_STUDENT');
INSERT INTO `e_obrazovanje`.`user_authority` (`user_id`, `authority_name`) VALUES ('5', 'ROLE_STUDENT');
INSERT INTO `e_obrazovanje`.`user_authority` (`user_id`, `authority_name`) VALUES ('6', 'ROLE_STUDENT');
INSERT INTO `e_obrazovanje`.`user_authority` (`user_id`, `authority_name`) VALUES ('7', 'ROLE_PROFESSOR');
INSERT INTO `e_obrazovanje`.`user_authority` (`user_id`, `authority_name`) VALUES ('8', 'ROLE_PROFESSOR');
INSERT INTO `e_obrazovanje`.`user_authority` (`user_id`, `authority_name`) VALUES ('9', 'ROLE_PROFESSOR');



