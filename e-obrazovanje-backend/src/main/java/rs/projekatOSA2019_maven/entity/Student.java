package rs.projekatOSA2019_maven.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.projekatOSA2019_maven.enums.Gender;


@Entity

public class Student {
	


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;
	private String lastName;
	
	@Column(name="indexx", nullable=false, unique = true, length = 30) 
	private String index;
	private int yearOfEnrollment;
	private int yearStudy;
	
	
	private String countryOfBirth;
	private String placeOfBirth;
	private String DateOfBirth;
	private Gender gender;
	private String wayOfFinancing;
	private String email;
	private String address;
	private int totalECTSPoints;
	private double averageRating;
	private String ordinalNumberOfEntries;
	
	//@JsonIgnore
	@ManyToOne
	@JoinColumn(name="department_id", referencedColumnName="id", nullable=false)
	private Department department;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "financialCard_id", referencedColumnName = "id")
	private FinancialCard financialCard;
	
    @OneToOne(mappedBy = "student")
    @NotFound(action=NotFoundAction.IGNORE)
	private User user;
	
    @OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="student" )
	private Set<Document> document = new HashSet<Document>();
												
    @OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="student" )
	private Set<ListPayment> listPayment = new HashSet<ListPayment>();
	
	@ManyToMany(mappedBy = "students")
	private Set<Subject> subjects = new HashSet<Subject>();
	
	
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="student" )
	private Set<Exam> exams = new HashSet<Exam>();
	
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="student" )
	private Set<Colloquium> colloquium = new HashSet<Colloquium>();

	public Student(int id, String name, String lastName, String index, int yearOfEnrollment, int yearStudy,
			String countryOfBirth, String placeOfBirth, String dateOfBirth, Gender gender, String wayOfFinancing,
			String email, String address, int totalECTSPoints, double averageRating, String ordinalNumberOfEntries,
			Department department, FinancialCard financialCard, User user, Set<Document> document,
			Set<ListPayment> listPayment, Set<Subject> subjects, Set<Exam> exams, Set<Colloquium> colloquium) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.index = index;
		this.yearOfEnrollment = yearOfEnrollment;
		this.yearStudy = yearStudy;
		this.countryOfBirth = countryOfBirth;
		this.placeOfBirth = placeOfBirth;
		DateOfBirth = dateOfBirth;
		this.gender = gender;
		this.wayOfFinancing = wayOfFinancing;
		this.email = email;
		this.address = address;
		this.totalECTSPoints = totalECTSPoints;
		this.averageRating = averageRating;
		this.ordinalNumberOfEntries = ordinalNumberOfEntries;
		this.department = department;
		this.financialCard = financialCard;
		this.user = user;
		this.document = document;
		this.listPayment = listPayment;
		this.subjects = subjects;
		this.exams = exams;
		this.colloquium = colloquium;
	}

	public Student() {
		super();
	}
	
	public void addUser(User user) {
		this.setUser(user);
		user.setStudent(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public int getYearOfEnrollment() {
		return yearOfEnrollment;
	}

	public void setYearOfEnrollment(int yearOfEnrollment) {
		this.yearOfEnrollment = yearOfEnrollment;
	}

	public int getYearStudy() {
		return yearStudy;
	}

	public void setYearStudy(int yearStudy) {
		this.yearStudy = yearStudy;
	}

	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getDateOfBirth() {
		return DateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getWayOfFinancing() {
		return wayOfFinancing;
	}

	public void setWayOfFinancing(String wayOfFinancing) {
		this.wayOfFinancing = wayOfFinancing;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getTotalECTSPoints() {
		return totalECTSPoints;
	}

	public void setTotalECTSPoints(int totalECTSPoints) {
		this.totalECTSPoints = totalECTSPoints;
	}

	public double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}

	public String getOrdinalNumberOfEntries() {
		return ordinalNumberOfEntries;
	}

	public void setOrdinalNumberOfEntries(String ordinalNumberOfEntries) {
		this.ordinalNumberOfEntries = ordinalNumberOfEntries;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public FinancialCard getFinancialCard() {
		return financialCard;
	}

	public void setFinancialCard(FinancialCard financialCard) {
		this.financialCard = financialCard;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Document> getDocument() {
		return document;
	}

	public void setDocument(Set<Document> document) {
		this.document = document;
	}

	public Set<ListPayment> getListPayment() {
		return listPayment;
	}

	public void setListPayment(Set<ListPayment> listPayment) {
		this.listPayment = listPayment;
	}

	public Set<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

	public Set<Exam> getExam() {
		return exams;
	}

	public void setExam(Set<Exam> exams) {
		this.exams = exams;
	}

	public Set<Colloquium> getColloquium() {
		return colloquium;
	}

	public void setColloquium(Set<Colloquium> colloquium) {
		this.colloquium = colloquium;
	}

	






}