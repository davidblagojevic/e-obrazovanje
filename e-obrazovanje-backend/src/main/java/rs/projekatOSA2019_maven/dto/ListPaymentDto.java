package rs.projekatOSA2019_maven.dto;

import rs.projekatOSA2019_maven.entity.ListPayment;

public class ListPaymentDto {
	private long id;
	
	private String purpose;
		
	private double sum;
	
	private boolean added;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public boolean isAdded() {
		return added;
	}

	public void setAdded(boolean added) {
		this.added = added;
	}

	public ListPaymentDto(long id, String purpose, double sum, boolean added) {
		super();
		this.id = id;
		this.purpose = purpose;
		this.sum = sum;
		this.added = added;
	}

	
	public ListPaymentDto(ListPayment listPayment) {
		this.id = listPayment.getId();
		this.purpose = listPayment.getPurpose();
		this.sum = listPayment.getSum();
		this.added = listPayment.isAdded();
	}
	
}
