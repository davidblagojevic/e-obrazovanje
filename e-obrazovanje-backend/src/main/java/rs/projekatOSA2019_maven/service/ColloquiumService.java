package rs.projekatOSA2019_maven.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.Colloquium;
import rs.projekatOSA2019_maven.entity.Document;
import rs.projekatOSA2019_maven.repository.ColloquiumRepository;
@Service
public class ColloquiumService implements ColloquiumServiceInterface {

	@Autowired
	ColloquiumRepository colloquiumRepository;
	
	
	@Override
	public List<Colloquium> getAll() {
		List<Colloquium> colloquiums= colloquiumRepository.findAll();
	return colloquiums;

	
	}

	@Override
	public Colloquium getById(Integer id) {
	Colloquium colloquiums= colloquiumRepository.findById(id).get();
		return colloquiums;
	}

	@Override
	public void delete(Colloquium d) {
		colloquiumRepository.delete(d);
		
	}

	@Override
	public Colloquium save(Colloquium colloquium) {
		Colloquium colloquiums= colloquiumRepository.save(colloquium);
		return colloquiums;
	}

	
	
}
