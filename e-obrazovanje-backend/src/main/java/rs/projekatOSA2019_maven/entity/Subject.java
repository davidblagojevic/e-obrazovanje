package rs.projekatOSA2019_maven.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
public class Subject  {
	
	
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;

	
	private int numberOfECTSPoints;
	private String dateOfLaying
;
	//
	@JsonIgnore
	@ManyToMany
    @JoinTable(name = "attends",
               joinColumns = @JoinColumn(name="subject_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="student_id", referencedColumnName="id"))
	private Set<Student> students = new HashSet<Student>();
	
	@JsonIgnore
	@ManyToMany
    @JoinTable(name = "surrender",
               joinColumns = @JoinColumn(name="subject_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="teacher_id", referencedColumnName="id"))
	private Set<Teacher> teachers = new HashSet<Teacher>();
	
	
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="subject" )
	private Set<Exam> exams = new HashSet<Exam>();
	
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="subject")
	private Set<Colloquium> colloquiums = new HashSet<Colloquium>();
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="department_id", referencedColumnName="id", nullable=false)
	private Department department;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfECTSPoints() {
		return numberOfECTSPoints;
	}

	public void setNumberOfECTSPoints(int numberOfECTSPoints) {
		this.numberOfECTSPoints = numberOfECTSPoints;
	}

	public String getDateOfLaying() {
		return dateOfLaying;
	}

	public void setDateOfLaying(String dateOfLaying) {
		this.dateOfLaying = dateOfLaying;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<Teacher> getTeacher() {
		return teachers;
	}

	public void setTeacher(Set<Teacher> teacher) {
		this.teachers = teacher;
	}

	public Set<Exam> getExams() {
		return exams;
	}

	public void setExams(Set<Exam> exams) {
		this.exams = exams;
	}

	public Set<Colloquium> getColloquiums() {
		return colloquiums;
	}

	public void setColloquiums(Set<Colloquium> colloquiums) {
		this.colloquiums = colloquiums;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}



	public Subject(int id, String name, int numberOfECTSPoints, String dateOfLaying, Set<Student> students,
			Set<Teacher> teacher, Set<Exam> exams, Set<Colloquium> colloquiums, Department department) {
		super();
		this.id = id;
		this.name = name;
		this.numberOfECTSPoints = numberOfECTSPoints;
		this.dateOfLaying = dateOfLaying;
		this.students = students;
		this.teachers = teacher;
		this.exams = exams;
		this.colloquiums = colloquiums;
		this.department = department;
	}

	public Subject() {
		super();
	}

	
	
	
	
	
}


