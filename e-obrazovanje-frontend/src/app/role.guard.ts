import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './services/user.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private _userService: UserService, 
    private _router: Router) {}

        
        // neka ostanu ove uloge, posto ne znam gde sam sta stavio
        // ROLE_PROFESSOR
        // ROLE_ADMIN
        // ROLE_UCENIK



    canActivate(next:ActivatedRouteSnapshot,
      state: RouterStateSnapshot): boolean {
      if(this._userService.loggedIn()){
        let roles = next.data['roles'] as Array<string>;
        if(roles){
          var match = this._userService.isAuthorized(roles);
          if(match) return true;
          else{
            var roleUlogovanog = this._userService.getRole();
            if(roleUlogovanog === "ROLE_ADMIN"){
              return false;
            }else if(roleUlogovanog === "ROLE_UCENIK"){
              return false;
            }else if(roleUlogovanog === "ROLE_PROFESSOR" ){
              return false;
            }
            
            return false;
          }
        }else
          return true;
      }
      this._router.navigate(['login']);
      return false;
    }
  
}