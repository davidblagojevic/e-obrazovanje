package rs.projekatOSA2019_maven.service;

import java.util.List;
import java.util.Optional;

import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;

public interface SubjectServiceInterface {
	
	public void save(Subject subject);
	
	Subject findByName(String name);
	
	Subject findOne(int id);
	
	List<Subject> findAll();
	public void delete(Subject s);
	
	public List<Subject> findByNameContaining(String infix);


	List<Subject> getSubjectStudentAttends(long studentId);

	
	List<Subject> findByTeacher(Integer id);

}
