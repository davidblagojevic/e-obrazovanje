package rs.projekatOSA2019_maven.entity;

import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;


@Entity

public class Colloquium {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	private float points;
	private String dateOfLaying;
	private String name; 
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="subject_id", referencedColumnName="id", nullable=false)
	private Subject subject;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="exam_id", referencedColumnName="id", nullable=false)
	private Exam exam;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="student_id", referencedColumnName="id", nullable=false)
	private Student student;



	public Colloquium() {
		super();
	}

	
	
	
	public Colloquium(long id, float points, String dateOfLaying, String name, Subject subject, Exam exam,
			Student student) {
		super();
		this.id = id;
		this.points = points;
		this.dateOfLaying = dateOfLaying;
		this.name = name;
		this.subject = subject;
		this.exam = exam;
		this.student = student;
	}




	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getPoints() {
		return points;
	}

	public void setPoints(float points) {
		this.points = points;
	}

	public String getDateOfLaying() {
		return dateOfLaying;
	}

	public void setDateOfLaying(String dateOfLaying) {
		this.dateOfLaying = dateOfLaying;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}




	public Student getStudent() {
		return student;
	}




	public void setStudent(Student student) {
		this.student = student;
	}




	@Override
	public String toString() {
		return "Colloquium [id=" + id + ", points=" + points + ", dateOfLaying=" + dateOfLaying + ", name=" + name
				+ ", subject=" + subject + ", exam=" + exam + ", student=" + student + "]";
	}



}
