import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubjectDto } from 'src/app/model/subject-dto';
import { ExamService } from 'src/app/services/exam.service';

@Component({
  selector: 'app-subject-from-teacher',
  templateUrl: './subject-from-teacher.component.html',
  styleUrls: ['./subject-from-teacher.component.css']
})
export class SubjectFromTeacherComponent implements OnInit {
  subjectDtoList:SubjectDto;
  index:string;
  id:string;
  idTeacher:string
  

  constructor( private route:ActivatedRoute,
    private serviceExam:ExamService,) { }


  
  ngOnInit(): void {
    this.idTeacher=localStorage.getItem('idUser');
    this.id = this.route.snapshot.paramMap.get("idPeriod");
    this.getSubjectFromTeacher(this.idTeacher);

  }



  public getSubjectFromTeacher(id){
      let subject = this.serviceExam.getSubjectFromTeacher(id);
      subject.subscribe(data=>this.subjectDtoList = data);
  }
  
    
  }
