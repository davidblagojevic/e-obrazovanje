import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamFromExaminationPeriodComponent } from './exam-from-examination-period.component';

describe('ExamFromExaminationPeriodComponent', () => {
  let component: ExamFromExaminationPeriodComponent;
  let fixture: ComponentFixture<ExamFromExaminationPeriodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamFromExaminationPeriodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamFromExaminationPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
