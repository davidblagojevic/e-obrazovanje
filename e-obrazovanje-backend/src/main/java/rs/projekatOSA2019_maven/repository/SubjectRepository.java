package rs.projekatOSA2019_maven.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;




public interface SubjectRepository extends JpaRepository<Subject, Integer> {
	
	Subject findByname(String name);
	
	Optional<Subject> findById(Integer id);
	
	// void save(Subject subject);
	
	List<Subject> findAll();
	
	List<Subject> findByNameContaining(String infix);
	
	@Query("select subject from Subject subject join subject.students students where students.id = :id")
	List<Subject> getSubjectStudentAttends(long id);



	List<Subject> findByTeachers_id(Integer id);

	
}
