package rs.projekatOSA2019_maven.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.projekatOSA2019_maven.dto.StudentDto;
import rs.projekatOSA2019_maven.dto.UserDto;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.User;
import rs.projekatOSA2019_maven.service.UserService;

@RestController()
@RequestMapping("api/users")
public class UserController {
	@Autowired
	UserService userService;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@GetMapping
	public ResponseEntity<List<UserDto>> getAllUsers() {
		List<User> users = userService.findAll();
		List<UserDto> usersDTO = new ArrayList<UserDto>();
		for (User user : users) {
			usersDTO.add(new UserDto(user));
		}
		return new ResponseEntity<>(usersDTO, HttpStatus.OK);
	}
	
	@GetMapping("/currentUser")
	public ResponseEntity<UserDto> getCurrentUser(Principal principal){
		
		System.out.println();
		
		User user = userService.findByUsername(principal.getName());
		

		System.out.println(principal.getName());
		if (user == null) {
			System.out.println("NEMA TOKEEEEEEEEEEEEEEENNNN");
			return new ResponseEntity<UserDto>(HttpStatus.BAD_REQUEST);
		}
		
		

		return new ResponseEntity<UserDto>(new UserDto(user), HttpStatus.OK);
			
	}
	
	@PutMapping()
	public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto){
		User user = userService.findOne((int) userDto.getId()).get();
		if (user == null) {
			return new ResponseEntity<UserDto>(HttpStatus.BAD_REQUEST);
		}
		user.setUsername(userDto.getUsername());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		
		return new ResponseEntity<UserDto>(userDto ,HttpStatus.OK);
	}
}

