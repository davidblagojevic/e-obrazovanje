package rs.projekatOSA2019_maven.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.projekatOSA2019_maven.dto.FinancialCardDto;
import rs.projekatOSA2019_maven.entity.Document;
import rs.projekatOSA2019_maven.entity.FinancialCard;
import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.service.DocumentService;
import rs.projekatOSA2019_maven.service.FinancialCardServiceInterface;
import rs.projekatOSA2019_maven.service.StudentService;
import rs.projekatOSA2019_maven.service.SubjectService;

@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping("api/financialCard")
public class FinancialCardController {
	@Autowired
	FinancialCardServiceInterface financialCardService;
	@Autowired
	SubjectService subjectService;
	@Autowired
	StudentService studentService;
	@Autowired
	DocumentService documentService;
	
	
	@GetMapping
	public ResponseEntity<List<FinancialCardDto>> getAll(){
		List<FinancialCard> financialCards = financialCardService.findAll();
		List<FinancialCardDto> financialCardDtos = new ArrayList<FinancialCardDto>();
		for (FinancialCard financialCard : financialCards) {
			financialCardDtos.add(new FinancialCardDto(financialCard));
		}
		return new ResponseEntity<List<FinancialCardDto>>(financialCardDtos, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/student/{studentId}")
	public ResponseEntity<?> getByUcenikId(@PathVariable long studentId){
		FinancialCard financialCard = financialCardService.findByStudentId(studentId);
		if(financialCard == null){
			
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		FinancialCardDto dto = new FinancialCardDto(financialCard);
//		List<Document> documents = documentService.findByStudentId(studentId);
//		List<Subject> attends = subjectService.getSubjectStudentAttends(studentId);
//		for(Subject subject : attends)
//			dto.getUcenik().getPohadja().add(new PredmetDTO(p));
//		for(Dokument d : dokumenti)
//			dto.getUcenik().getDokumenti().add(new DokumentDTO(d));
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

}
