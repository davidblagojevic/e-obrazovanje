import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from './services/user.service';
@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private _user: UserService, private _router: Router) {}

  canActivate(): boolean {
    if (this._user.loggedIn()) {
      return true;
    } else {
      this._router.navigate(['PassedExamComponent']);
      return false;
    }
  }
}
