package rs.projekatOSA2019_maven.service;

import java.util.List;
import java.util.Optional;

import rs.projekatOSA2019_maven.entity.Document;



public interface DocumentServiceInterface {
	
	
	Document getById(Integer id);
	
	Document save(Document document);
	
	List<Document> getAll();
	
	void delete(Document d);
	
	List<Document> findByStudentId(long studentId);
	

}
