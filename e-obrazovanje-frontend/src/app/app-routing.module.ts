import { StudentDetailComponent } from './components/Student/student-detail/student-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExamComponent } from './components/Exam/exam/exam.component';
import { NotPasedExamComponent } from './components/exam/historyExam/not-pased-exam/not-pased-exam.component';
import { PassedExamComponent } from './components/exam/passedExam/passed-exam/passed-exam.component';
import { ExaminationPeriodComponent } from './components/ExaminationPeriod/examination-period/examination-period.component';
import { ExaminationPeriodAllComponent } from './components/ExaminationPeriod/ExaminationPeriodAll/examination-period-all/examination-period-all.component';
import { ExaminationPeriodUpdateComponent } from './components/ExaminationPeriod/examination-period-update/examination-period-update.component';
import { ExamFromExaminationPeriodComponent } from './components/Exam/exam-from-examination-period/exam-from-examination-period.component';
import { SubjectFromTeacherComponent } from './components/subject/subject-from-teacher/subject-from-teacher.component';

import { StudentComponent } from './components/Student/student/student.component';
import { CreateStudentComponent } from './components/Student/create-student/create-student.component';
import { TeachersComponent } from './components/Teacher/teachers/teachers.component';
import { TeacherDetailComponent } from './components/Teacher/teacher-detail/teacher-detail.component';
import { CreateTeacherComponent } from './components/Teacher/create-teacher/create-teacher.component';
import { SubjectComponent } from './components/Subject/subject/subject.component';
import { SubjectDetailComponent } from './components/Subject/subject-detail/subject-detail.component';
import { AuthGuard } from './auth.guard';
import { RoleGuard } from './role.guard';
import { CreateSubjectComponent } from './components/Subject/create-subject/create-subject.component';
import{ LoginComponent} from './components/Login/login.component'
import { UserDetailComponent } from './components/User/user-detail/user-detail.component';

        // neka ostanu ove uloge, posto ne znam gde sam sta stavio
        // ROLE_PROFESSOR
        // ROLE_ADMIN
        // ROLE_STUDENT

const routes: Routes = [

  {path:'',component:LoginComponent},
  {
    path: 'ExamComponent/:id',
    component: ExamComponent,
    canActivate: [AuthGuard,RoleGuard],
    data: {
      roles: ['ROLE_PROFESSOR','ROLE_STUDENT','ROLE_ADMIN'],

    },
  },
  // { path: 'PassedExamComponent', component: PassedExamComponent },

  {
    path: 'PassedExamComponent',
    component: PassedExamComponent,
    canActivate: [AuthGuard,RoleGuard],
    data: {
      roles: ['ROLE_STUDENT'],
    },
  },

  // {path:'NotPassedExamFromUser',component:NotPasedExamComponent},

  {
    path: 'NotPassedExamFromUser',
    component: NotPasedExamComponent,
    canActivate: [AuthGuard,RoleGuard],
    data: {
      roles: ['ROLE_STUDENT'],
    },
  },
  // {path:'ExaminationPeriod',component:ExaminationPeriodComponent},

  {
    path: 'ExaminationPeriod',
    component: ExaminationPeriodComponent,
    canActivate:[AuthGuard,RoleGuard],
    data: {
      roles: ['ROLE_STUDENT'],
    },
  },

  // {path:'ExaminationPeriodAll',component:ExaminationPeriodAllComponent},
  {
    path: 'ExaminationPeriodAll',
    component: ExaminationPeriodAllComponent,
    canActivate: [AuthGuard,RoleGuard],
    data: {
      roles: ['ROLE_PROFESSOR','ROLE_ADMIN'],
    },
  },



  // {
  //   path: 'ExaminationPeriodUpdate/:id',
  //   component: ExaminationPeriodUpdateComponent,
  // },
  {
    path: 'ExaminationPeriodUpdate/:id',
    component: ExaminationPeriodUpdateComponent,
    canActivate: [AuthGuard,RoleGuard],
    data: {
      roles: ['ROLE_ADMIN'],
    },
  },



  {
    path: 'ExamFromExaminationPeriod/:idSubject/:idPeriod',
    component: ExamFromExaminationPeriodComponent,
  },





  // {
  //   path: 'SubjectsFromTacher/:idPeriod',
  //   component: SubjectFromTeacherComponent,
  // },
{
  path: 'SubjectsFromTacher/:idPeriod',
  component: SubjectFromTeacherComponent,
  canActivate: [AuthGuard,RoleGuard],
    data: {
      roles: ['ROLE_PROFESSOR'],
    },
  },




  { path: 'StudentComponent', component: StudentComponent,
    canActivate: [AuthGuard,RoleGuard],
    data: {
      roles: ['ROLE_ADMIN'],

    } 

},
  { path: 'StudentComponent/:id', component: StudentDetailComponent,
  canActivate: [AuthGuard,RoleGuard],
    data: {
      roles: ['ROLE_ADMIN'],

    },

},
  { path: 'CreateStudent', component: CreateStudentComponent
  ,canActivate: [AuthGuard,RoleGuard],
  data: {
    roles: ['ROLE_ADMIN'],

  }, 
},

  { path: 'TeacherComponent', component: TeachersComponent
  ,canActivate: [AuthGuard,RoleGuard],
  data: {
    roles: ['ROLE_ADMIN'],

  }, 
},
  { path: 'TeacherComponent/:id', component: TeacherDetailComponent
  ,canActivate: [AuthGuard,RoleGuard],
  data: {
    roles: ['ROLE_ADMIN'],

  },
 },
  { path: 'CreateTeacher', component: CreateTeacherComponent
  ,canActivate: [AuthGuard,RoleGuard],
  data: {
    roles: ['ROLE_ADMIN'],

  },
 },

  { path: 'SubjectComponent', component: SubjectComponent
  ,canActivate: [AuthGuard,RoleGuard],
  data: {
    roles: ['ROLE_PROFESSOR','ROLE_ADMIN','ROLE_STUDENT'],

  }, 
},
  { path: 'SubjectComponent/:id', component: SubjectDetailComponent
  ,canActivate: [AuthGuard,RoleGuard],
  data: {
    roles: ['ROLE_PROFESSOR','ROLE_STUDENT','ROLE_ADMIN'],

  }, 
},
{ path: 'CreateSubject', component: CreateSubjectComponent
  ,canActivate: [AuthGuard,RoleGuard],
  data: {
    roles: ['ROLE_ADMIN'],

  }, 
},
{
  path: 'UserDetail', component:UserDetailComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

export const routingComponents = [
  PassedExamComponent,
  ExamComponent,
  NotPasedExamComponent,
  ExaminationPeriodComponent,
  ExaminationPeriodUpdateComponent,
  ExamFromExaminationPeriodComponent,
  SubjectFromTeacherComponent,
  StudentComponent,
  StudentDetailComponent,
  CreateStudentComponent,
  SubjectComponent,
  SubjectDetailComponent,
  CreateSubjectComponent,
  LoginComponent,
  UserDetailComponent
];
