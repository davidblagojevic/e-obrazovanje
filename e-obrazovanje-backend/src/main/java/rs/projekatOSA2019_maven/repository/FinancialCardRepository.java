package rs.projekatOSA2019_maven.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import rs.projekatOSA2019_maven.entity.FinancialCard;

public interface FinancialCardRepository extends JpaRepository<FinancialCard, Integer> {
	FinancialCard findByCardNumber(String cardNumber);
	
	@Query("select count(fk) from FinancialCard fk")
	int countFinancialCard();
	
	FinancialCard findByStudentId(long id);


}
