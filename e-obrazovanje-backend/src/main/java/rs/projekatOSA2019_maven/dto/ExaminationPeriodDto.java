package rs.projekatOSA2019_maven.dto;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;



import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.ExaminationPeriod;
import rs.projekatOSA2019_maven.enums.NameOfExamPeriod;

public class ExaminationPeriodDto {

	
	
	private long id;
	private NameOfExamPeriod nameOfExamPeriod;
	private Date theBeginningOfTheExaminationPeriod;
	private Date endOfExamPeriod;

	
	
	
	
//	public ExaminationPeriodDto(ExaminationPeriod ExaminationPeriod) {
//		
//		this.id = ExaminationPeriod.getId();
//		this.nameOfExamPeriod =  ExaminationPeriod.getNameOfExamPeriod();
//		this.theBeginningOfTheExaminationPeriod =  ExaminationPeriod.getTheBeginningOfTheExaminationPeriod();		
//		this.endOfExamPeriod =  ExaminationPeriod.getEndOfExamPeriod();
//		
//		if(ExaminationPeriod.getExam() != null) 
//			nastavnik = new NastavnikDTO(smer.getNastavnik());
//		else 
//			nastavnik = new NastavnikDTO();
//
//	}
//	
	
	
	public ExaminationPeriod examinationPeriod(ExaminationPeriodDto examinationPeriod ) {
	
		
		ExaminationPeriod ex= new ExaminationPeriod();
		ex.setEndOfExamPeriod(examinationPeriod.getEndOfExamPeriod());
		ex.setId(examinationPeriod.getId());
		ex.setNameOfExamPeriod(examinationPeriod.getNameOfExamPeriod());
		ex.setTheBeginningOfTheExaminationPeriod(examinationPeriod.getTheBeginningOfTheExaminationPeriod());
		
		
		return ex;
	}
	
	
	
	public long getId() {
		return id;
	}
	public ExaminationPeriodDto(ExaminationPeriod examinationPeriod ) {
		super();
		this.id = examinationPeriod.getId();
		this.nameOfExamPeriod = examinationPeriod.getNameOfExamPeriod();
		this.theBeginningOfTheExaminationPeriod = examinationPeriod.getTheBeginningOfTheExaminationPeriod();
		this.endOfExamPeriod = examinationPeriod.getEndOfExamPeriod();
	}


	public void setId(long id) {
		this.id = id;
	}
	public ExaminationPeriodDto() {
		super();
	}
	public NameOfExamPeriod getNameOfExamPeriod() {
		return nameOfExamPeriod;
	}
	public void setNameOfExamPeriod(NameOfExamPeriod nameOfExamPeriod) {
		this.nameOfExamPeriod = nameOfExamPeriod;
	}
	public Date getTheBeginningOfTheExaminationPeriod() {
		return theBeginningOfTheExaminationPeriod;
	}
	public void setTheBeginningOfTheExaminationPeriod(Date theBeginningOfTheExaminationPeriod) {
		this.theBeginningOfTheExaminationPeriod = theBeginningOfTheExaminationPeriod;
	}
	public Date getEndOfExamPeriod() {
		return endOfExamPeriod;
	}
	public void setEndOfExamPeriod(Date endOfExamPeriod) {
		this.endOfExamPeriod = endOfExamPeriod;
	}

	
	
	
}
