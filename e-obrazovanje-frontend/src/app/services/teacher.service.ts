import { TeacherDto } from './../model/teacher-dto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  getTeacher: string ="http://localhost:8080/api/teachers";

  constructor(private http:HttpClient) { }

  public  getAll(){
    return this.http.get<any>(`${this.getTeacher}`);
  }
  
  public  getTeacherByid(id){
    return this.http.get<TeacherDto>(`${this.getTeacher}/${id}`);
  }

  public  getTeacherByUser(){
    return this.http.get<any>(`${this.getTeacher}/fromUser`);
  }
  
  public create(student,id){
    return this.http.post<TeacherDto>(`${this.getTeacher}/${id}`,student).subscribe(
      (data) => {
       console.log(JSON.stringify(data));
     },
       (error) => {
       
         console.error('There was an error!', error);
     }
 )
  }
  
  public update(student,id){
    return this.http.put<TeacherDto>(`${this.getTeacher}/${id}`,student).subscribe(
      (data) => {
       console.log(JSON.stringify(data));
     },
       (error) => {
       
         console.error('There was an error!', error);
     }
    )
  }
  
  public deleteStudent(id){
    return this.http.delete(this.getTeacher+"/"+id);
  }
}
