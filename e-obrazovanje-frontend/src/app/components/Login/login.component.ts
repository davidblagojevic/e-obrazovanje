import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UserDto } from "src/app/model/user-dto";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['login.component.css']
})


export class LoginComponent implements OnInit{
    
    user:UserDto;
  
    ngOnInit(){
      
    }
    constructor(private fb: FormBuilder, private _userService: UserService, private _activatedRoute: ActivatedRoute,
        private _router:Router){}

   
    
    loginForm = this.fb.group({
        username: [''],
        password: ['']

    });

    submitLogin(){
        console.log(this.loginForm.value);
        this._userService.login(this.loginForm.value)
        .subscribe(
        response => {
        console.log(response)
        // localStorage.setItem('jwt',`Bearer `+"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1Y2VuaWsxIiwiYXV0aCI6IlJPTEVfU1RVREVOVCIsImV4cCI6MTYxMjU0MjI1M30.pgmLSR5MwTvgkaUoaYO945AAQdrssADALAWsed8CAq_D6CGH13pfj3P7RUf7Lm_FLhv74gjCwpTeHRKBjXNOqg");
        localStorage.setItem('jwt',`Bearer `+response.id_token);
        console.log(response.id_token);
        console.log("sdfsdfsdfsdf");
        });
        setTimeout(()=>{
            this._userService.getCurrentUser().subscribe(data => {
                console.log(data);
                
                localStorage.setItem("idUser", data.idRole.toString());

                console.log(data.authorities[0]);
                localStorage.setItem("role", data.authorities[0].name);
                setTimeout(() => {
                    this._userService.getRedirectedFromLogin(data.authorities[0].name);
    
                }, 2500);
            
            });
        },3000);
       

//             localStorage.setItem("idUser", data.idRole.toString());
//             console.log(data.authorities[0]);
//             localStorage.setItem("role", data.authorities[0].name);
//             setTimeout(() => {
//                 this._userService.getRedirectedFromLogin(data.authorities[0].name);

//             }, 3500);
        
//         });
// >>>>>>> 0d4dc2cba53fb983394d82506ed6aa4c31578098
        console.log("nije uslo");
        

      }
}