package rs.projekatOSA2019_maven.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Integer>{
	
//	List<Teacher> findByNameContaining(String infix);
	//Optional<Teacher> findById(int id);

}
