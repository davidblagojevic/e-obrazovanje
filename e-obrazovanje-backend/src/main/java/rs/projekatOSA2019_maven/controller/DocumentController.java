package rs.projekatOSA2019_maven.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import rs.projekatOSA2019_maven.entity.Document;
import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.service.DocumentService;



@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping("api/documents")
public class DocumentController {
//	
//	@Autowired
//	private DocumentService documentServiceImp;
//	
//	
//	//@PreAuthorize("hasRole('ROLE_ADMIN')")
//	@GetMapping
//	public ResponseEntity<List<DocumentDTO>> getAllDocuments() {
//		List<Document> documents = documentServiceImp.getAll();
//		List<DocumentDTO> documentsDTO = new ArrayList<DocumentDTO>();
//		for (Document doc : documents) {
//			documentsDTO.add(new DocumentDTO(doc));
//		}
//		return new ResponseEntity<>(documentsDTO, HttpStatus.OK);
//	}
//	
//	@PostMapping(consumes = "application/json")
//	public ResponseEntity<DocumentDTO> saveDocument(@RequestBody DocumentDTO dDTO) {
//		Document d=new Document();
//		d.setName(dDTO.getName());
//		d.setPath(dDTO.getPath());
//		d.setStudent(dDTO.getStudent());
//		documentServiceImp.save(d);
//		return new ResponseEntity<>(new DocumentDTO(d), HttpStatus.OK);
//	}
//	
//	@PutMapping(value = "/edit/{id}", consumes = "application/json")
//	public ResponseEntity<DocumentDTO> editDocument(@PathVariable int id,@RequestBody DocumentDTO dDTO) {
//		Document d=documentServiceImp.getById(id);
//		
//		if(d==null)
//			return new ResponseEntity<>( HttpStatus.NOT_FOUND);
//		
//		d.setName(dDTO.getName());
//		d.setPath(dDTO.getPath());
//		d.setStudent(dDTO.getStudent());
//		documentServiceImp.save(d);
//		return new ResponseEntity<>(new DocumentDTO(d), HttpStatus.OK);
//	}
//	
//	@GetMapping(value = "/{id}")
//	public ResponseEntity<DocumentDTO> getDocument(@PathVariable Integer id) {
//		Document d=documentServiceImp.getById(id);
//		
//		if(d==null)
//			return new ResponseEntity<>( HttpStatus.NOT_FOUND);
//		else
//			return new ResponseEntity<>(new DocumentDTO(d), HttpStatus.OK);
//	}
//	
//	@DeleteMapping(value = "/{id}")
//	public ResponseEntity<Void> deleteDocument(@PathVariable Integer id) {
//		Document d=documentServiceImp.getById(id);
//		
//		if(d==null)
//			return new ResponseEntity<>( HttpStatus.NOT_FOUND);
//		else {
//			documentServiceImp.delete(d);
//			return new ResponseEntity<>( HttpStatus.OK);
//		}
//			
//	}
//	
//	

}
