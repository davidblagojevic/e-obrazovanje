import { StudentDto } from 'src/app/model/student-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserDto } from 'src/app/model/user-dto';
import { FinancialCardDto } from 'src/app/model/financialCard-dto';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css']
})
export class CreateStudentComponent implements OnInit {

  createStudent: FormGroup;
  constructor(
    private service:StudentService,
    private fb: FormBuilder,
    private router: Router,
    private route:ActivatedRoute,
  ) {}

  get name() {
    return this.createStudent.get('name');
  }

  get lastName() {
    return this.createStudent.get('lastName');
  }

  get userName() {
    return this.createStudent.get('userName');
  }

  get password() {
    return this.createStudent.get('password');
  }

  get bankAccount() {
    return this.createStudent.get('bankAccount');
  }

  get cardNumber() {
    return this.createStudent.get('cardNumber');
  }

  get modelNumber() {
    return this.createStudent.get('modelNumber');
  }

  get sum() {
    return this.createStudent.get('sum');
  }

  get index() {
    return this.createStudent.get('index');
  }

  get dateOfBirth() {
    return this.createStudent.get('dateOfBirth');
  }


  get yearOfEnrollment() {
    return this.createStudent.get('yearOfEnrollment');
  }

  get yearStudy() {
    return this.createStudent.get('yearStudy');
  }

  get placeOfBirth() {
    return this.createStudent.get('placeOfBirth');
  }

  get countryOfBirth() {
    return this.createStudent.get('countryOfBirth');
  }

  get address() {
    return this.createStudent.get('address');
  }

  get wayOfFinancing() {
    return this.createStudent.get('wayOfFinancing');
  }

  get gender() {
    return this.createStudent.get('gender');
  }

  get totalECTSPoints() {
    return this.createStudent.get('totalECTSPoints');
  }

  get averageRating() {
    return this.createStudent.get('averageRating');
  }

  get email() {
    return this.createStudent.get('email');
  }

  get department() {
    return this.createStudent.get('department');
  }

  departments:any;

  ngOnInit(): void {

    this.createStudent= this.fb.group({
      name: ['',Validators.required],
      lastName: ['',Validators.required],
      userName: ['',Validators.required],
      password: ['',Validators.required],
      index: ['',Validators.required],
      bankAccount: [0,Validators.required],
      cardNumber: [0,Validators.required],
      modelNumber: [0,Validators.required],
      sum: [0,Validators.required],
      dateOfBirth: ['',Validators.required],
      yearOfEnrollment: [0,Validators.required],
      yearStudy: [0,Validators.required],
      placeOfBirth: ['',Validators.required],
      countryOfBirth: ['',Validators.required],
      address: ['',Validators.required],
      wayOfFinancing: [0,Validators.required],
      totalECTSPoints: [0,Validators.required],
      averageRating: [0,Validators.required],
      gender: ['',Validators.required],
      email: ['',Validators.required],
      department: [0,Validators.required]
    });
  }

  update(){

    var user=new UserDto(this.userName.value,this.password.value);
    var financialCar=new FinancialCardDto(this.cardNumber.value,this.sum.value,this.bankAccount.value
      ,this.modelNumber.value);

    var student=new StudentDto(
      1,this.name.value,this.lastName.value,this.index.value,this.countryOfBirth.value,this.placeOfBirth.value,
      this.dateOfBirth.value,this.gender.value,this.wayOfFinancing.value,this.email.value,this.address.value,
      this.totalECTSPoints.value,this.averageRating.value,1,user,financialCar,1
    )
    if(this.validation(student)=== true){
      this.service.create(student,this.department.value);
      this.router.navigate(['StudentComponent']);
    }
      
  }
  validation(teacher:StudentDto){
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( teacher.name==='' || teacher.name.length < 3){
      alert("ime studenta je prazno ili je manje od 3 slova");
      return false;
    }
    else if( teacher.lastName==='' || teacher.lastName.length < 3){
      alert("prezime studenta je prazno ili je manje od 3 slova");
      return false;
    }
    else if( teacher.user.username==='' || teacher.user.username.length < 3){
      alert("username studenta je prazno ili je manje od 3 slova");
      return false;
    }
    else if( teacher.user.password==='' || teacher.user.password.length < 5){
      alert("sifra studenta je prazno ili je manje od 5 slova");
      return false;
    }

    else if( teacher.gender===''){
      alert("rod studenta je prazno ili je manje od 3 slova");
      return false;
    }
    else if( teacher.wayOfFinancing==='' ){
      alert("nacin finansiranja studenta je prazno ");
      return false;
    }

    else if( teacher.countryOfBirth===''){
      alert("drzava  studenta je prazno ili je manje od 3 slova");
      return false;
    }
    else if( this.dateOfBirth.value===''){
      alert("datum rodjenja  studenta je prazno");
      return false;
    }

    else if( teacher.financialCard.sum < 0){
      alert("suma studenta je manja od 0");
      return false;
    }
    else if( teacher.financialCard.cardNumber < 10000000){
      alert("broj kartice studenta je kraci od 8 ifara");
      return false;
    }
    else if( teacher.financialCard.modelNumber< 100){
      alert("model kartice studentaj e prazno ili je manje od 3");
      return false;
    }
    else if( this.department.value===''){
      alert("department ne sme biti prazan");
      return false;
    }
    else if (!(re.test(teacher.email)))
    {
      alert("email nije validan");
      return false;
    }
    else{
      return true;
    }

  }

}
