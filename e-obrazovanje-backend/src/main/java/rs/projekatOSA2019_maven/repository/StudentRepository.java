package rs.projekatOSA2019_maven.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {
	
	Student findByIndex(String index);
	
	Student findById(Student index);
	
	
	@Query("select i from Student i where id = :uid ")
	Student  getStudent(long uid);

}
