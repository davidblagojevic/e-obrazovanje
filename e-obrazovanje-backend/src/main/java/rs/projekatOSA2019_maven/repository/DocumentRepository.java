package rs.projekatOSA2019_maven.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.projekatOSA2019_maven.entity.Document;
import rs.projekatOSA2019_maven.entity.FinancialCard;

public interface DocumentRepository extends JpaRepository<Document, Integer> {
	List<Document> findByStudentId(long id);
}
