package rs.projekatOSA2019_maven.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.repository.SubjectRepository;

@Service
public class SubjectService implements SubjectServiceInterface {

	@Autowired
	SubjectRepository repo;
	
	@Override
	public void save(Subject subject) {
		 repo.save(subject);
		
	}

	@Override
	public Subject findByName(String name) {
		// TODO Auto-generated method stub
		return repo.findByname(name);
	}

	@Override
	public Subject findOne(int id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public List<Subject> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public void delete(Subject s) {
		repo.delete(s);
		
	}

	@Override
	public List<Subject> findByNameContaining(String infix) {
		// TODO Auto-generated method stub
		return repo.findByNameContaining(infix);
	}


	@Override
	public List<Subject> getSubjectStudentAttends(long studentId) {
		return repo.getSubjectStudentAttends(studentId);
	}
	
	
	@Override
	public List<Subject> findByTeacher(Integer idTeacher) {
	System.out.println("novosubjekt23432");
		return repo.findByTeachers_id(idTeacher);
		
	}

}
