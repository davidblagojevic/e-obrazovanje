import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpInterceptor} from '@angular/common/http'
 import { ExamService } from './exam.service';


@Injectable({
    providedIn: 'root'
  })
export class TokenInterceptorService  implements HttpInterceptor{

    constructor( private injector: Injector) { }



    intercept(req, next){


        let korService = this.injector.get(ExamService);
        let tokenizedRequest = req.clone({
          setHeaders: {
            Authorization: localStorage.getItem("jwt").toString()
          }
        })
        return next.handle(tokenizedRequest);
      }
}