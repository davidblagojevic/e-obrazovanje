package rs.projekatOSA2019_maven.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.Document;
import rs.projekatOSA2019_maven.repository.DocumentRepository;


@Service
public class DocumentService implements DocumentServiceInterface{

	@Autowired DocumentRepository documentRepository;
	
	
	
	
	@Override
	public Document getById(Integer id) {
		return documentRepository.findById(id).get();
	}
	
	

	@Override
	public Document save(Document document) {
		
		return documentRepository.save(document);
	}

	@Override
	public List<Document> getAll() {
		return  documentRepository.findAll();
	}



	@Override
	public void delete(Document d) {
		documentRepository.delete(d);
		
	}



	@Override
	public List<Document> findByStudentId(long studentId) {
		return documentRepository.findByStudentId(studentId);
	}
	

}
