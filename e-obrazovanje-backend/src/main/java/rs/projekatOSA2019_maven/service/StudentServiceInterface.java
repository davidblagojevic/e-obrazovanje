package rs.projekatOSA2019_maven.service;

import java.util.List;
import java.util.Optional;

import rs.projekatOSA2019_maven.entity.Student;

public interface StudentServiceInterface {
	
	Student save(Student student);
	
	void delete(Student student);
	
	List<Student> findAll();
	
	Student findById(int id);
	
	Student findByIndex(String index);

	List<Student> findByNameContaining(String infix);
}
