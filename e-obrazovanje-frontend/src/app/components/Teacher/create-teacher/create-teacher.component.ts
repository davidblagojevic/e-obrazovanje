import { TeacherDto } from './../../../model/teacher-dto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserDto } from 'src/app/model/user-dto';
import { TeacherService } from 'src/app/services/teacher.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-teacher',
  templateUrl: './create-teacher.component.html',
  styleUrls: ['./create-teacher.component.css']
})
export class CreateTeacherComponent implements OnInit {

  createStudent: FormGroup;
  constructor(
    private service:TeacherService,
    private fb: FormBuilder,
    private router: Router,
    private route:ActivatedRoute,
  ) {}

  get name() {
    return this.createStudent.get('name');
  }

  get lastName() {
    return this.createStudent.get('lastName');
  }

  get username() {
    return this.createStudent.get('username');
  }

  get password() {
    return this.createStudent.get('password');
  }

 
  get email() {
    return this.createStudent.get('email');
  }
  get department() {
    return this.createStudent.get('department');
  }

  ngOnInit(): void {

    this.createStudent= this.fb.group({
      name: ['',Validators.required],
      lastName: ['',Validators.required],
      username: ['',Validators.required],
      password: ['',Validators.required],
      email: ['',Validators.required],
      department:['',Validators.required]
    });
  }

  update(){

    var user=new UserDto(this.username.value,this.password.value);
    

    var teacher=new TeacherDto(
      1,this.name.value,this.lastName.value,this.email.value,user
    )
    console.log(teacher);
    if(this.validation(teacher)===true){
      this.service.create(teacher,this.department.value);
      this.router.navigate(['/TeacherComponent']);
    }

  }

  validation(teacher:TeacherDto){
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( teacher.name==='' || teacher.name.length < 3){
      alert("ime nastavnika je prazno ili je manje od 3 slova");
      return false;
    }
    else if( teacher.lastName==='' || teacher.lastName.length < 3){
      alert("prezime nastavnika je prazno ili je manje od 3 slova");
      return false;
    }
    else if( teacher.user.username==='' || teacher.user.username.length < 3){
      alert("username nastavnika je prazno ili je manje od 3 slova");
      return false;
    }
    else if( teacher.user.password==='' || teacher.user.password.length < 5){
      alert("sifra nastavnika je prazno ili je manje od 5 slova");
      return false;
    }
    else if( this.department.value===''){
      alert("departmant ne sme biti prazan");
      return false;
    }
    else if (!(re.test(teacher.email)))
    {
      alert("email nije validan");
      return false;
    }
    else{
      return true;
    }

  }

}
