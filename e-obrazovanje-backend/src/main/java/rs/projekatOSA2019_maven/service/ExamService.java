package rs.projekatOSA2019_maven.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.repository.ExamRepository;

@Service
public class ExamService implements ExamServiceInterface{

	@Autowired
	ExamRepository repo;

	@Override
	public void save(Exam exam) {
		 repo.save(exam);
		
	}

	@Override
	public void delete(Exam exam) {
		repo.delete(exam);
		
	}

	@Override
	public List<Exam> findAll() {
		List<Exam> exams = repo.findAll();
		return exams;
	}

	@Override
	public List<Exam> findByDateOfLaying(Date index) {
		List<Exam> exams = repo.findByDateOfLaying(index);
		return exams;
	}
	
	
	@Override
	public List<Exam> findByDateOfApplication(Date index) {
		List<Exam> exams = repo.findByDateOfApplication(index);
		return exams;
	}

	
	

	@Override
	public List<Exam> getExamsFromUser(Integer uid) {
		System.out.println("uid"+uid);
		List<Exam> exams = repo.getExamsFromUser(uid);
		return exams;
	}
	
	

	@Override
	public List<Exam> getExamsFromUserWherePassedExam(Integer uid) {
		List<Exam> exams = repo.getExamsFromUserWherePassedExam(uid);
		return exams;
	}

	@Override
	public List<Exam> getNotPassedExamFromUser(Integer uid) {
		
		List<Exam> exams = repo.getNotPassedExamFromUser(uid);
		return exams;
	}

	@Override
	public List<Exam> getExamFromUserAndPeriod(Integer idStud,long idexa){

		List<Exam> exams = repo.getExamFromStudentAndPeriodPass(idStud,idexa);
		
		
		return exams;
	}

	@Override
	public List<Exam> getExamFromUserAndPeriodNotPass(Integer idStud,long idexa){
	
		
		List<Exam> exams = repo.getExamFromStudentAndPeriodNotPass(idStud,idexa);
		
		
		return exams;
	}

	@Override
	public void updateExamLaiddown(long idexa) {
		
	System.out.println("updateExamLaiddown");
		repo.updateExamLaiddown(idexa);
		
	}
	
	
	@Override
	public void updateExamLaiddownOnNull(long idexa) {
		
	System.out.println("updateExamLaiddown");
		repo.updateExamLaiddownOnNull(idexa);
		
	}


	
	
	@Override
	public Exam findById(long id) {
	 Exam a = repo.findById(id);

	 return a;
		
		
	}

	
	@Override
	public List<Exam> getExamFromPeriod(Integer s,long p) {
	
		return 	repo.getExamFromPeriodPass(s,p);
	}

	@Override
	public Exam findById(Integer id) {
		 Exam a = repo.findByIdExam(id);

		 return a;
	}
	
	
	
	
	


}
