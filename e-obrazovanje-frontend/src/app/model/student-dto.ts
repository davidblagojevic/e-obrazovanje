import { FinancialCardDto } from './financialCard-dto';
import { UserDto } from './user-dto';
export class StudentDto {

    id:number;
    name:string;
    lastName:string;
    index:string;
    yearOfEnrollment:number;
	yearStudy:number;
	countryOfBirth:string;
	placeOfBirth:string;
	DateOfBirth:Date;
	gender:string;
	wayOfFinancing:string;
	email:string;
	address:string;
	totalECTSPoints:number;
	averageRating:number;
    ordinalNumberOfEntries:number;
    user:UserDto;
    financialCard:FinancialCardDto;
   
    constructor(id:number,name:string,lastName:string,index:string,countryOfBirth:string,placeOfBirth:string,
        DateOfBirth:Date,gender:string,wayOfFinancing:string,email:string,address:string,totalECTSPoints:number,
        averageRating:number,ordinalNumberOfEntries:number,user:UserDto,financialCard:FinancialCardDto,yearStudy:number){
       
        this.id=id;
        this.name=name;
        this.lastName=lastName;
        this.index=index;
        this.countryOfBirth=countryOfBirth;
        this.placeOfBirth=placeOfBirth;
        this.DateOfBirth=DateOfBirth;
        this.gender=gender;
        this.wayOfFinancing=wayOfFinancing;
        this.email=email;
        this.address=address;
        this.totalECTSPoints=totalECTSPoints;
        this.averageRating=averageRating;
        this.ordinalNumberOfEntries=ordinalNumberOfEntries;
        this.user=user;
        this.financialCard=financialCard; 
        this.yearStudy=yearStudy; 
    }

}
