import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { FormBuilder, FormArray, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { ExaminationPeriodDto } from 'src/app/model/examination-period-dto';
import { ExaminationPeriodService } from 'src/app/services/examination-period.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';


@Component({
  selector: 'app-examination-period-update',
  templateUrl: './examination-period-update.component.html',
  styleUrls: ['./examination-period-update.component.css'],
  providers: [DatePipe]

})
export class ExaminationPeriodUpdateComponent implements OnInit {


  examinationPeriodDto:ExaminationPeriodDto;
  id:string
  addForm: FormGroup;
  saveForm: FormGroup;

  korImeExists = false;
  // nowDate:Data;
  myDate = new Date();



  get startDateOfThePeriod() {
    return this.addForm.get('startDateOfThePeriod');
  }

  get endDateOfThePeriod() {
    return this.addForm.get('endDateOfThePeriod');
  }
  get namePeriod() {
    return this.addForm.get('namePeriod');
  }
 
  get startDateOfThePeriod1() {
    return this.saveForm.get('startDateOfThePeriod1');
  }

  get endDateOfThePeriod1() {
    return this.saveForm.get('endDateOfThePeriod1');
  }
  get namePeriod1() {
    return this.saveForm.get('namePeriod1');
  }


  examinationPeriodArray = ['JANUARY','FEBRUARY','MARTOVSKI','APRIL',
  'MARTOVSKI','JUNE','JULY','SEPTEMBER','OCTOBER'];

  


  constructor(
   
    private examinationPeriodService:ExaminationPeriodService,
    private route:ActivatedRoute,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private _router: Router,
    

    

    

  ) {

 

  }





  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id");


    if(this.id!="add"){
      this.getExaminationPerodFromId(this.id)
    

    this.addForm = this.fb.group({
      startDateOfThePeriod: [,Validators.required],
      endDateOfThePeriod: [,Validators.required],
      namePeriod: [,this.fb.array(this.examinationPeriodArray)],
      });
      
      }



 if(this.id=="add"){

    this.saveForm = this.fb.group({
      startDateOfThePeriod1: [,Validators.required],
      endDateOfThePeriod1: [,Validators.required],
      namePeriod1: [,this.fb.array(this.examinationPeriodArray)],
    });
  }

  }



  


 

  public getExaminationPerodFromId(id){

    let resp=this.examinationPeriodService.getExaminationPeriodFromId(id)

      resp.subscribe(
        
        data=>{
     
        this.examinationPeriodDto = data;
        var vreme_pocetno=this.examinationPeriodDto.theBeginningOfTheExaminationPeriod;
        var vreme_krajnje=this.examinationPeriodDto.endOfExamPeriod;
        var naziv_roka=this.examinationPeriodDto.nameOfExamPeriod;
      


        

        this.addForm.patchValue({
          namePeriod: this.examinationPeriodDto.nameOfExamPeriod,
          startDateOfThePeriod:this.datePipe.transform(new Date(vreme_pocetno),"yyyy-MM-ddTHH:mm:ss.SSS") ,
          endDateOfThePeriod:this.datePipe.transform(new Date(vreme_krajnje),"yyyy-MM-ddTHH:mm:ss.SSS")   ,
          
        })


      }  )



  }













  public saveExaminationPerod(exa){
    let resp=this.examinationPeriodService.saveExaminationPerod(exa)
    resp.subscribe(data =>{ this._router.navigate(['ExaminationPeriodAll']);});
}


public saveExaminationPerod1(exa){
  let resp=this.examinationPeriodService.saveExaminationPerod1(exa)
  resp.subscribe(data =>{ this._router.navigate(['ExaminationPeriodAll']);});
}


  

    submit(){

     

      var start = new Date(this.startDateOfThePeriod.value);
      var end = new Date(this.endDateOfThePeriod.value);
      var namePeriod =this.namePeriod.value; 

  

      if(start>end){

        alert("the start time must be greater than the end time")

      }
      else if (namePeriod==null){
        alert("NamePeriod is mandatory!")
       
      }
      else if (end <new Date() ){
        alert("End date is not correct!")
       
      }

      else if (start <new Date() ){
        alert("Start date is not correct!")
       
      }

      
      else{

      var kor: ExaminationPeriodDto =new ExaminationPeriodDto( Number(this.id),start,end,namePeriod,null);
      this.saveExaminationPerod(kor)
      }
      }
      

    
  
      submitSave(){


        var start = new Date(this.startDateOfThePeriod1.value);
        var end = new Date(this.endDateOfThePeriod1.value);
        var namePeriod =this.namePeriod1.value;



        if(namePeriod===null){

          alert("NamePeriod is mandatory!")
  
        }
        
      else if(start>end){

        alert(" the start time must be greater than the end time")

      }else if (end < new Date()){
        alert("End date is not correct!")
       
      }else if (start <new Date() ){
        alert("Start date is not correct!")
       
      }
      else{

        var kor: ExaminationPeriodDto =new ExaminationPeriodDto( Number(this.id),start,end,namePeriod,null);
        this.saveExaminationPerod1(kor)
      }
  
        }




}
export class dialho {}