import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationPeriodUpdateComponent } from './examination-period-update.component';

describe('ExaminationPeriodUpdateComponent', () => {
  let component: ExaminationPeriodUpdateComponent;
  let fixture: ComponentFixture<ExaminationPeriodUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExaminationPeriodUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationPeriodUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
