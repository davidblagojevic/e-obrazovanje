import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'e-obrazovanje-frontend';

  role: string;

  constructor() {
    this.role = localStorage.getItem('role');
  }
}
