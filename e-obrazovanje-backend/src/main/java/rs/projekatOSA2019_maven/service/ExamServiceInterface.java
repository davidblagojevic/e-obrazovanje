package rs.projekatOSA2019_maven.service;

import java.util.Date;
import java.util.List;

import rs.projekatOSA2019_maven.entity.Exam;


public interface ExamServiceInterface {
	
	void save(Exam exam);

	void delete(Exam exam);
	
	List<Exam> findAll();
	
	 List<Exam> findByDateOfLaying(Date index);
	 
	 List<Exam> findByDateOfApplication(Date index);
	 

	 
	 
	 
	 List<Exam> getExamsFromUser(Integer uid);
	 List<Exam> getExamsFromUserWherePassedExam(Integer uid);
	 List<Exam> getNotPassedExamFromUser(Integer uid);

	 
	 List<Exam> getExamFromUserAndPeriod(Integer idStud,long idexa);
	 List<Exam> getExamFromUserAndPeriodNotPass(Integer idStud,long idexa);
	 
	 List<Exam> getExamFromPeriod(Integer idexa,long idexa2);
	 
	 void updateExamLaiddown(long idexa);

	 void updateExamLaiddownOnNull(long idexa);

	Exam findById(long id);
	Exam findById(Integer id);
	
	 
	 
}
