import { SubjectDto } from './../model/subject-dto';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'subjectFilter'
})
export class SubjectFilterPipe implements PipeTransform {

  transform(value: SubjectDto[], name:string): unknown {
    
    if(!value ||  !name){
      return value;
    }
    
    else if(name){
      return value.filter(s=>
        s.name.toLowerCase().indexOf(name.toLowerCase()) !== -1
      )
    }
  }

}
