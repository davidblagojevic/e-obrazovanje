package rs.projekatOSA2019_maven.service;

import java.util.List;

import rs.projekatOSA2019_maven.entity.FinancialCard;

public interface FinancialCardServiceInterface {
	public List<FinancialCard> findAll();
	public FinancialCard save(FinancialCard financialCard);
	public FinancialCard findByCardNumber(String cardNumber);
	public  FinancialCard findByStudentId(long id);
	public String createCardNumber();
	public int countFinancialCard();
	
}
