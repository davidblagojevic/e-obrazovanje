package rs.projekatOSA2019_maven.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.projekatOSA2019_maven.entity.Colloquium;
import rs.projekatOSA2019_maven.entity.Department;
import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.entity.Teacher;

public class SubjectDto {
	
	private int id;
	private String name;

	private int numberOfECTSPoints;
	private String dateOfLaying;

//	@JsonIgnore
//	private List<StudentDto> students = new ArrayList<StudentDto>();
//	
	@JsonIgnore
	private List<ExamDto> exams = new ArrayList<ExamDto>();
	
	private List<ColloquiumDto> colloquiums = new ArrayList<ColloquiumDto>();

	
	
	private DepartmentDto department;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfECTSPoints() {
		return numberOfECTSPoints;
	}

	public void setNumberOfECTSPoints(int numberOfECTSPoints) {
		this.numberOfECTSPoints = numberOfECTSPoints;
	}

	public String getDateOfLaying() {
		return dateOfLaying;
	}

	public void setDateOfLaying(String dateOfLaying) {
		this.dateOfLaying = dateOfLaying;
	}

//	public List<StudentDto> getStudents() {
//		return students;
//	}
//
//	public void setStudents(List<StudentDto> students) {
//		this.students = students;
//	}


	public List<ExamDto> getExams() {
		return exams;
	}

	public void setExams(List<ExamDto> exams) {
		this.exams = exams;
	}

	public List<ColloquiumDto> getColloquiums() {
		return colloquiums;
	}

	public void setColloquiums(List<ColloquiumDto> colloquiums) {
		this.colloquiums = colloquiums;
	}

	public DepartmentDto getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentDto department) {
		this.department = department;
	}



	public SubjectDto(Subject s) {
		super();
		this.id = s.getId();
		this.name = s.getName();
		this.numberOfECTSPoints = s.getNumberOfECTSPoints();
		this.dateOfLaying = s.getDateOfLaying();
//		if(s.getStudents() != null) {
//			for (Student st : s.getStudents()) {
//				this.students.add(new StudentDto(st));
//			}
//		}

		if(s.getExams() != null) {
			for (Exam e : s.getExams()) {
				this.exams.add(new ExamDto(e));
			}
		}
		if(s.getColloquiums() != null) {
			for (Colloquium co : s.getColloquiums()) {
				this.colloquiums.add(new ColloquiumDto(co));
			}
		}

	}

	public SubjectDto() {
		super();
	}

	

}



