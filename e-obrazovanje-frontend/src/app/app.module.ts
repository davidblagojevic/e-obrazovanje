import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/Forms';

import { TokenInterceptorService } from './services/TokenInterceptorService';

import { AppRoutingModule,routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { NotPasedExamComponent } from './components/exam/historyExam/not-pased-exam/not-pased-exam.component';
import { ExaminationPeriodComponent } from './components/ExaminationPeriod/examination-period/examination-period.component';
import { ExaminationPeriodAllComponent } from './components/ExaminationPeriod/ExaminationPeriodAll/examination-period-all/examination-period-all.component';
import {  ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { ExamFilterPipe } from './filters/exam-filter.pipe';
import { ExamSortPipe } from './filters/exam-sort.pipe';
import { ExaminationPeriodSortPipe } from './filters/examination-period-sort.pipe';
import { ExaminationPeriodFilterPipe } from './filters/examination-period-filter.pipe';
import { StudentDetailComponent } from './components/Student/student-detail/student-detail.component';
import { CreateStudentComponent } from './components/Student/create-student/create-student.component';
import { TeachersComponent } from './components/Teacher/teachers/teachers.component';
import { TeacherDetailComponent } from './components/Teacher/teacher-detail/teacher-detail.component';
import { CreateTeacherComponent } from './components/Teacher/create-teacher/create-teacher.component';
import { StudentFilterPipe } from './filters/student-filter.pipe';
import { SubjectComponent } from './components/Subject/subject/subject.component';
import { SubjectDetailComponent } from './components/Subject/subject-detail/subject-detail.component';
import { LoginComponent } from './components/Login/login.component';

import { AuthGuard } from './auth.guard';
import { CreateSubjectComponent } from './components/Subject/create-subject/create-subject.component';
import { SubjectFilterPipe } from './filters/subject-filter.pipe';
import { TeacherFilterPipe } from './filters/teacher-filter.pipe';
import { UserDetailComponent } from './components/User/user-detail/user-detail.component';




@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    NotPasedExamComponent,
    ExaminationPeriodComponent,
    ExaminationPeriodAllComponent,
    ExamFilterPipe,
    ExamSortPipe,
    ExaminationPeriodSortPipe,
    ExaminationPeriodFilterPipe,
    StudentDetailComponent,
    CreateStudentComponent,
    TeachersComponent,
    TeacherDetailComponent,
    CreateTeacherComponent,
    StudentFilterPipe,
    SubjectComponent,
    SubjectDetailComponent,
    CreateSubjectComponent,
    SubjectFilterPipe,
    TeacherFilterPipe,
    LoginComponent,
    UserDetailComponent


    // ExamFromExaminationPeriodComponent
  ],


  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatDialogModule,
    FormsModule
  ],
  providers: [DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true 
    },AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
