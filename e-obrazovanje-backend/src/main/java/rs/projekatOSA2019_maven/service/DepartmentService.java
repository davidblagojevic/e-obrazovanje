package rs.projekatOSA2019_maven.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.Department;
import rs.projekatOSA2019_maven.repository.DepartmentRepository;

@Service
public class DepartmentService implements DepartmentServiceInterface {

	@Autowired
	DepartmentRepository repo;
	
	@Override
	public Department findById(long id) {
		// TODO Auto-generated method stub
		return repo.findById(id);
	}

	@Override
	public List<Department> findAll() {
		return repo.findAll();
	}
	

}
