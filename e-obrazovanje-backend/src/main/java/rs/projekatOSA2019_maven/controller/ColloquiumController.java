package rs.projekatOSA2019_maven.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.projekatOSA2019_maven.dto.ColloquiumDto;
import rs.projekatOSA2019_maven.entity.Colloquium;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.service.ColloquiumServiceInterface;
import rs.projekatOSA2019_maven.service.StudentService;
import rs.projekatOSA2019_maven.service.SubjectService;

@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value = "api/colloquiums")
public class ColloquiumController {
	@Autowired
	ColloquiumServiceInterface coloqiumService;
	
	@Autowired
	SubjectService subjectService;
	
	@Autowired
	StudentService studentService;
	
	@PostMapping(consumes = "application/json")
	public ResponseEntity<Void> add(@RequestBody ColloquiumDto colloquiumDto){
		Colloquium colloquium = new Colloquium();
		Student student = studentService.findById(colloquiumDto.getStudent().getId());
		Subject subject = subjectService.findOne(colloquiumDto.getSubject().getId());
		if (student == null || subject == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		colloquium.setName(colloquiumDto.getName());
		colloquium.setPoints(colloquiumDto.getPoints());
		colloquium.setDateOfLaying(colloquiumDto.getDateOfLaying());
		colloquium.setStudent(student);
		colloquium.setSubject(subject);
		
		coloqiumService.save(colloquium);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
