package rs.projekatOSA2019_maven.entity;


import java.util.HashSet;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Department {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String name;
	
	
	
	private int numberOfECTSPoints;
	private String DepartmentMark;
	//
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="department" )
	private Set<Teacher> teacher = new HashSet<Teacher>();
	
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="department" )
	private Set<Student> students = new HashSet<Student>();
	
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="department" )
	private Set<Subject> subjects = new HashSet<Subject>();

	public Department() {
		super();
	}

	public Department(long id, String name, int numberOfECTSPoints, String departmentMark, Set<Teacher>  teacher,
			Set<Student> students, Set<Subject> subjects) {
		super();
		this.id = id;
		this.name = name;
		this.numberOfECTSPoints = numberOfECTSPoints;
		DepartmentMark = departmentMark;
		this.teacher = teacher;
		this.students = students;
		this.subjects = subjects;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfECTSPoints() {
		return numberOfECTSPoints;
	}

	public void setNumberOfECTSPoints(int numberOfECTSPoints) {
		this.numberOfECTSPoints = numberOfECTSPoints;
	}

	public String getDepartmentMark() {
		return DepartmentMark;
	}

	public void setDepartmentMark(String departmentMark) {
		DepartmentMark = departmentMark;
	}

	public Set<Teacher> getTeacher() {
		return teacher;
	}

	public void setTeacher(Set<Teacher> teacher) {
		this.teacher = teacher;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}
	

}
