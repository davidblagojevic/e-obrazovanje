package rs.projekatOSA2019_maven.dto;

import rs.projekatOSA2019_maven.entity.FinancialCard;

public class FinancialCardDto {
	private long id;
	private String cardNumber;
	private double sum;
	private String bankAccount;
	private String referenceNumber;
	private String ModelNumber;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public double getSum() {
		return sum;
	}
	public void setSum(double sum) {
		this.sum = sum;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getModelNumber() {
		return ModelNumber;
	}
	public void setModelNumber(String modelNumber) {
		ModelNumber = modelNumber;
	}
	public FinancialCardDto(long id, String cardNumber, double sum, String bankAccount, String referenceNumber,
			String modelNumber) {
		super();
		this.id = id;
		this.cardNumber = cardNumber;
		this.sum = sum;
		this.bankAccount = bankAccount;
		this.referenceNumber = referenceNumber;
		ModelNumber = modelNumber;
	}
	public FinancialCardDto(FinancialCard financialCard) {
		this.id = financialCard.getId();
		this.cardNumber = financialCard.getCardNumber();
		this.sum = financialCard.getSum();
		this.bankAccount = financialCard.getBankAccount();
		this.referenceNumber = financialCard.getReferenceNumber();
		ModelNumber = financialCard.getModelNumber();
	}
	
}
