package rs.projekatOSA2019_maven.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.projekatOSA2019_maven.enums.ExamStatus;



@Entity

public class Exam {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String dateOfLaying;
	private String dateOfApplication;
	private int assessment;
	private int pointsTheory;
	private int ExercisePoints;
	private boolean laiddown = false;
	private int Price;
	private ExamStatus examStatus;
	
	
    
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="subject_id", referencedColumnName="id", nullable=false)
	private Subject subject;
    
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="student_id", referencedColumnName="id", nullable=false)
	private Student student;
    
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="examinationPeriod_id", referencedColumnName="id", nullable=false)
	private ExaminationPeriod examinationPeriod;
	
	
	@JsonIgnore
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="exam" )
	private Set<Colloquium> colloquium = new HashSet<Colloquium>();

	public Exam() {
		super();
	}

	public Exam(long id, String dateOfLaying, String dateOfApplication, int assessment, int pointsTheory,
			int exercisePoints, boolean laiddown, int price, ExamStatus examStatus, Subject subject, Student student,
			ExaminationPeriod eaminationPeriod, Set<Colloquium> colloquium) {
		super();
		this.id = id;
		this.dateOfLaying = dateOfLaying;
		this.dateOfApplication = dateOfApplication;
		this.assessment = assessment;
		this.pointsTheory = pointsTheory;
		ExercisePoints = exercisePoints;
		this.laiddown = laiddown;
		Price = price;
		this.examStatus = examStatus;
		this.subject = subject;
		this.student = student;
		this.examinationPeriod = eaminationPeriod;
		this.colloquium = colloquium;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDateOfLaying() {
		return dateOfLaying;
	}

	public void setDateOfLaying(String dateOfLaying) {
		this.dateOfLaying = dateOfLaying;
	}

	public String getDateOfApplication() {
		return dateOfApplication;
	}

	public void setDateOfApplication(String dateOfApplication) {
		this.dateOfApplication = dateOfApplication;
	}

	public int getAssessment() {
		return assessment;
	}

	public void setAssessment(int assessment) {
		this.assessment = assessment;
	}

	public int getPointsTheory() {
		return pointsTheory;
	}

	public void setPointsTheory(int pointsTheory) {
		this.pointsTheory = pointsTheory;
	}

	public int getExercisePoints() {
		return ExercisePoints;
	}

	public void setExercisePoints(int exercisePoints) {
		ExercisePoints = exercisePoints;
	}

	public boolean isLaiddown() {
		return laiddown;
	}

	public void setLaiddown(boolean laiddown) {
		this.laiddown = laiddown;
	}

	public int getPrice() {
		return Price;
	}

	public void setPrice(int price) {
		Price = price;
	}

	public ExamStatus getExamStatus() {
		return examStatus;
	}

	public void setExamStatus(ExamStatus examStatus) {
		this.examStatus = examStatus;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public ExaminationPeriod getEaminationPeriod() {
		return examinationPeriod;
	}

	public void setEaminationPeriod(ExaminationPeriod eaminationPeriod) {
		this.examinationPeriod = eaminationPeriod;
	}

	public Set<Colloquium> getColloquium() {
		return colloquium;
	}

	public void setColloquium(Set<Colloquium> colloquium) {
		this.colloquium = colloquium;
	}

	
	


}
