import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationPeriodAllComponent } from './examination-period-all.component';

describe('ExaminationPeriodAllComponent', () => {
  let component: ExaminationPeriodAllComponent;
  let fixture: ComponentFixture<ExaminationPeriodAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExaminationPeriodAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationPeriodAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
