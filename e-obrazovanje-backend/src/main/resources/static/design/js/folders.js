$(document).ready(function () {
    fetchFolders();


    $(document).on("click", 'tr', function (event) {
        highlightRow(this);
    });

    //za brisanje
    $(document).on("click", '#delete', function (event) {
        var name = getNameOfSelectedEntity();
        if (name != null) {
            $('#deletePromptText').text("Jeste li sigurni da želite da izbiršete kontakt: " + name);
            $('#deletePromptModal').modal('show');
        }

    });

    $(document).on("click", '.deletePromptClose', function (event) {
        $('#deletePromptModal').modal('hide');
    });

    $(document).on("click", '#doDeleteFolder', function (event) {
        deleteEntity('folders', fetchFolders);
        $('#deletePromptModal').modal('hide');
    });

    //za dodavanje
    $(document).on("click", '#add', function (event) {
        $('#addModalScrollable').modal('show');
    });
});


function fetchFolders() {
    $.ajax({
        type: "GET",
        url: "folders",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: createAuthorizationTokenHeader()
    }).then(
        function (data) {
            $("#dataTableBody").empty();
            for (i = 0; i < data.length; i++) {
                newRow = "<tr><td class=\"nameCell\">" + data[i].name + "</td>"
                    + "<td class=\"conditionCell\">" + data[i].rule.condition + "</td>"
                    + "<td class=\"operationCell\">" + data[i].rule.operation + "</td>"
                    + "<td class=\"idCell\">" + data[i].id + "</td>" + "</tr>"
                $("#dataTableBody").append(newRow);
            }
        });

}