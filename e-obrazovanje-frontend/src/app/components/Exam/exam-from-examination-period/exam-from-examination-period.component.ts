import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExamDto } from 'src/app/model/exam-dto';
import { ExamService } from 'src/app/services/exam.service';

@Component({
  selector: 'app-exam-from-examination-period',
  templateUrl: './exam-from-examination-period.component.html',
  styleUrls: ['./exam-from-examination-period.component.css']
})
export class ExamFromExaminationPeriodComponent implements OnInit {

  constructor(
    private route:ActivatedRoute,
    private serviceExam:ExamService,


  ) { }


  subjects:[];
  idSubject:string
  idPeriod:string
  examDto:ExamDto;

  listExamDto:[];

  field;
  order;
  saerctSubijekt: string = '';
  searchTerm2: string = '';
  searchTerm3: string = '';


  ngOnInit(): void {

    this.idSubject = this.route.snapshot.paramMap.get("idSubject");
    this.idPeriod = this.route.snapshot.paramMap.get("idPeriod");



    this.examByPeriod(this.idSubject,this.idPeriod);
  }





  private  examByPeriod(idSubject,idPeriod){

      let examPassed = this.serviceExam.getExamByPeriod(idSubject,idPeriod);
      examPassed.subscribe(data=>this.listExamDto = data);
  }



counter = 0;

sort(param) {
  if (param === 'price') {
    this.field = 'price';
  }
  if (param === 'dateOfLaying') {
    this.field = 'dateOfLaying';
  }
  if (param === 'dateOfApplication') {
    this.field = 'dateOfApplication';
  }
  if (param === 'subject.name') {
    this.field = 'subject.name';
  }
  if (param === 'pointsTheory') {
    this.field = 'pointsTheory';
  }
  if (param === 'exercisePoints') {
    this.field = 'exercisePoints';
  }

  if (this.counter === 0) {
    //desc
    this.order = 'asc';
    this.counter = 1;
  } else if (this.counter === 1) {
    //asc
    this.order = 'desc';
    this.counter = 0;
  }
}

}
