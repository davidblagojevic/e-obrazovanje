import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ExamDto } from '../model/exam-dto';
import { ExaminationPeriodDto } from '../model/examination-period-dto';
import { ExamService } from '../services/exam.service';

@Injectable({
  providedIn: 'root'
})
export class ExaminationPeriodService {


  private _getNotRegisteredExam: string ="http://localhost:8080/api/examinationperiod/get_exam_from_period_pass";
  private _getRegisteredExam: string ="http://localhost:8080/api/examinationperiod/getexamfromperiodnotpass";
  private _updatExamLaiddown: string ="http://localhost:8080/api/exams/updateExamLaiddown";
  private _updatExamLaiddownOnNull: string ="http://localhost:8080/api/exams/updateExamLaiddownOnNull";
  private _getEximinationPeriodAll: string ="http://localhost:8080/api/examinationperiod/getExamsInPeriodAll";
 
  private _getEximinationPeriodId: string ="http://localhost:8080/api/examinationperiod/examinationPeriodById";
  
  
  private _saveExaminationPeriod: string ="http://localhost:8080/api/examinationperiod/saveExaminationPeriod";

  private _saveExaminationPeriod1: string ="http://localhost:8080/api/examinationperiod/updateEximinationPeriod";

  private _deleteExaminationPeriod1: string ="http://localhost:8080/api/examinationperiod/examinationPeriodDeleteById";

  


  constructor(private http:HttpClient , private exam: ExamService) { }

  public  getNotRegisteredExam(nid){
   return this.http.get<ExamDto>(`${this._getNotRegisteredExam}/${+nid}`);
 } 

 public  getRegisteredExam(nid){
 return this.http.get<any>(`${this._getRegisteredExam}/${+nid}`);
} 

public  setExamStatus1(nid){
 return this.http.get<ExamDto>(`${this._updatExamLaiddown}/${+nid}`);
} 

public  setExamStatus1OnNull(nid){
 return this.http.get<ExamDto>(`${this._updatExamLaiddownOnNull}/${+nid}`);
} 

public  getExaminationPeriodAll(){
 return this.http.get<ExaminationPeriodDto>(`${this._getEximinationPeriodAll}`);

} 

public  getExaminationPeriodFromId(nid){
 return this.http.get<ExaminationPeriodDto>(`${this._getEximinationPeriodId}/${+nid}`);


} 

public  saveExaminationPerod(data:ExaminationPeriodDto){
  


 return this.http.post(`${this._saveExaminationPeriod1}`,data);
} 


public  saveExaminationPerod1(data:ExaminationPeriodDto){

   return this.http.put(`${this._saveExaminationPeriod}`,data);
  } 



public  deleteExaminationPeriodById(nid){
 return this.http.delete<any>(`${this._deleteExaminationPeriod1}/${+nid}`);


} 


}
