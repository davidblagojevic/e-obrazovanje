import { SubjectDto } from './../model/subject-dto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  getSubject: string ="http://localhost:8080/api/subjects";

  constructor(private http:HttpClient) { }

  public  getAll(){
    return this.http.get<any>("http://localhost:8080/api/subjects");
  }
  
  
  public  getSubjectsByid(id){
    return this.http.get<SubjectDto>(`${this.getSubject}/${id}`);
  }
  
  public create(subject,id){
    console.log(JSON.stringify(subject));
    return this.http.post<SubjectDto>(`${this.getSubject}/${id}`,subject).subscribe(
       (data) => {
        console.log(JSON.stringify(data));
      },
        (error) => {
        
          console.error('There was an error!', error);
      }
  )
  }
  
  public update(subject,id){
    return this.http.put<SubjectDto>(this.getSubject+"/"+id,subject).subscribe(
      (data) => {
       console.log(JSON.stringify(data));
     },
       (error) => {
       
         console.error('There was an error!', error);
     }
 );
  }
  
  public deleteStudent(id){
    return this.http.delete(this.getSubject+"/"+id);
  }
}
