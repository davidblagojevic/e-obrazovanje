
import {  StudentDto} from "./student-dto";
import { SubjectDto } from "./subject-dto";
export class ExamDto {


    id:number;
    dateOfLaying:string;
    dateOfApplication:string;
    price:number;
    exercisePoints:number;
    pointsTheory:number;
    assessment:string;
    laiddown:boolean;
    student:StudentDto;
    subject:SubjectDto;

    lastname:string;
    name:string;
    index:string;
   



    constructor(laiddown:boolean,id:number,
        dateOfLaying:string,dateOfApplication:string,
        Price:number,ExercisePoints:number,
        pointsTheory:number,assessment:string,
        _student:StudentDto,
        _subject:SubjectDto,
        _lastname:string,
        _name:string,
        _index:string
        
        ){
       
        this.id=id;
        this.dateOfLaying=dateOfLaying;
        this.dateOfApplication=dateOfApplication;
        this.price  =Price;
        this.assessment=assessment;
        this.exercisePoints=ExercisePoints;
        this.pointsTheory=pointsTheory;
        this.laiddown=laiddown;
        this.student=_student;
        this.subject=_subject;
        this.lastname=_lastname;
        this.name=_name;
        this.index=_index;
      
    }

}
