package rs.projekatOSA2019_maven.controller;

import java.util.ArrayList;

import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import rs.projekatOSA2019_maven.security.jwt.JWTFilter;

import org.springframework.http.MediaType;
@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping("/transaction")
public class TransactionController {

//	@Autowired
//	private TransactionService transactionServiceImpl;
//	
//	
//	
//	
//	
//	@RequestMapping(value="/user", method = RequestMethod.GET)
//    public String listUser(){
//		return "uspeo";  
//		}
//	
//	
//	
//	
//	@RequestMapping(value = "/all", method = RequestMethod.GET)
//	public String getAllq(){
//		
//		return  "uspeo";
//		
//	}
//	
//	
//	@GetMapping("/getAllTransaction")
//	public ResponseEntity<List<TransactionDTO>> getAll(){
//		List<Transaction> transactions = transactionServiceImpl.findAll();
//		List<TransactionDTO> transactionsDTO = new ArrayList<TransactionDTO>();
//		for (Transaction transaction : transactions) {
//			transactionsDTO.add(new TransactionDTO(transaction));
////			System.out.println("verzija"+transaction.getVersion());
//		}
//		return new ResponseEntity<>(transactionsDTO, HttpStatus.OK);
//		
//		
//	}
//	@RequestMapping(value = "/save",
//			method = RequestMethod.PUT,
//			consumes = MediaType.APPLICATION_JSON_VALUE,
//			produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<TransactionDTO> addOne(@RequestBody TransactionDTO transactionDTO ) {
//		System.out.println("stigo1--"+transactionDTO.getBankAccount());
//		Transaction t= new Transaction();
//		t.setBankAccount(transactionDTO.getBankAccount());
//		t.setDateAndTime(transactionDTO.getDateAndTime());
//		t.setId(transactionDTO.getId());
//		t.setPrice(transactionDTO.getPrice());
//		t.setPurpose(transactionDTO.getPurpose());
//		t.setRecipient(transactionDTO.getRecipient());
////		t.setVersion(transactionDTO.getVersion());
//		System.out.println("stigo2");
//		System.out.println("st---"+t.getPrice());
//		System.out.println("stigo2"+transactionDTO.getVersion());
//
//		transactionServiceImpl.save(t);
//		
//		System.out.println("stigo3");
//
//		return new ResponseEntity<TransactionDTO>(transactionDTO, HttpStatus.OK);
//
//	}
//
//	
//	@RequestMapping(value = "/updatedtransaction/",
//			method = RequestMethod.PUT,
//			consumes = MediaType.APPLICATION_JSON_VALUE,
//			produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Transaction> updateTransaction(@RequestBody Transaction transaction) throws Exception {
//		
//		Transaction updatedTransaction = null;
//		try{
//			updatedTransaction = transactionServiceImpl.save(transaction);
//
//		} catch(NoSuchElementException e) {
//			return new ResponseEntity<Transaction>(HttpStatus.NOT_FOUND);
//		} catch(ObjectOptimisticLockingFailureException oplfe) {
//			return new ResponseEntity<Transaction>(HttpStatus.I_AM_A_TEAPOT); // :)
//		}
//		
//		return new ResponseEntity<Transaction>(updatedTransaction, HttpStatus.OK);
//	}
//	
//	
//	
//	
//	@RequestMapping(value = "/update",
//			method = RequestMethod.PUT,
//			consumes = MediaType.APPLICATION_JSON_VALUE,
//			produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<TransactionDTO> update(@RequestBody TransactionDTO transactionDTO ) {
//		
//		
//		Transaction transaction = transactionServiceImpl.findOne(transactionDTO.getId());
//		if (transaction == null)
//			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		
//		transaction.setBankAccount(transactionDTO.getBankAccount());
//		transaction.setDateAndTime(transactionDTO.getDateAndTime());
//		transaction.setId(transactionDTO.getId());
//		transaction.setPrice(transactionDTO.getPrice());
//		transaction.setPurpose(transactionDTO.getPurpose());
//		transaction.setRecipient(transactionDTO.getRecipient());
////		t.setVersion(transactionDTO.getVersion());
//		
//		
//		
//		System.out.println("stigo45");
//
//		
//		transactionServiceImpl.save(transaction);
//		
//		
//
//		return new ResponseEntity<>(new TransactionDTO(transaction), HttpStatus.OK);
//
//	}
//	
//	
//	
//	
//	
//	@PostMapping
//	public ResponseEntity<String> adidOne( ){
//		
//		System.out.println("stigo");
//		
//		return new ResponseEntity<String>("uspeo",HttpStatus.OK);
//
//	}
//	

	
}
