import { Component, OnInit } from '@angular/core';
import { ExamDto } from 'src/app/model/exam-dto';
import { ExamService } from 'src/app/services/exam.service';

@Component({
  selector: 'app-not-pased-exam',
  templateUrl: './not-pased-exam.component.html',
  styleUrls: ['./not-pased-exam.component.css'],
})
export class NotPasedExamComponent implements OnInit {
  exam: ExamDto;
  field;
  order;
  saerctSubijekt: string = '';
  searchTerm2: string = '';
  searchTerm3: string = '';
  constructor(private serviceExam: ExamService) {

    console.log("11111111NotPasedExamComponent11111111111")
  }






  ngOnInit(): void {
    this.getNotPassedExam();
  }

  public getNotPassedExam() {
    let examPassed = this.serviceExam.getNotPassedExam(
      localStorage.getItem('idUser')
    );
    examPassed.subscribe((data) => (this.exam = data));
  }

  counter = 0;

  sort(param) {
    if (param === 'price') {
      this.field = 'price';
    }
    if (param === 'dateOfLaying') {
      this.field = 'dateOfLaying';
    }
    if (param === 'dateOfApplication') {
      this.field = 'dateOfApplication';
    }
    if (param === 'subject.name') {
      this.field = 'subject.name';
    }
    if (param === 'pointsTheory') {
      this.field = 'pointsTheory';
    }
    if (param === 'exercisePoints') {
      this.field = 'exercisePoints';
    }

    if (this.counter === 0) {
      //desc
      this.order = 'asc';
      this.counter = 1;
    } else if (this.counter === 1) {
      //asc
      this.order = 'desc';
      this.counter = 0;
    }
  }
}
