package rs.projekatOSA2019_maven.entity;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import rs.projekatOSA2019_maven.enums.NameOfExamPeriod;

//import tseo.app.enums.NazivRoka;
@Entity
public class ExaminationPeriod  {
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private long id;
	
	private NameOfExamPeriod nameOfExamPeriod;
	
	private Date theBeginningOfTheExaminationPeriod;
	
	private Date endOfExamPeriod;
	
	
	private boolean examinationPeriod_status = false;

	private boolean examinationPeriodValid = false;

	
	

	public boolean isExaminationPeriodValid() {
		return examinationPeriodValid;
	}

	public void setExaminationPeriodValid(boolean examinationPeriodValid) {
		this.examinationPeriodValid = examinationPeriodValid;
	}

	public ExaminationPeriod(long id, NameOfExamPeriod nameOfExamPeriod, Date theBeginningOfTheExaminationPeriod,
			Date endOfExamPeriod, boolean examinationPeriod_status, Set<Exam> exam,boolean examinationPeriodValid) {
		super();
		this.id = id;
		this.nameOfExamPeriod = nameOfExamPeriod;
		this.theBeginningOfTheExaminationPeriod = theBeginningOfTheExaminationPeriod;
		this.endOfExamPeriod = endOfExamPeriod;
		this.examinationPeriod_status = examinationPeriod_status;
		this.exam = exam;
		this.examinationPeriodValid = examinationPeriodValid;
	}

	public boolean isExaminationPeriod_status() {
		return examinationPeriod_status;
	}

	public void setExaminationPeriod_status(boolean examinationPeriod_status) {
		this.examinationPeriod_status = examinationPeriod_status;
	}

	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="examinationPeriod" )
	private Set<Exam> exam = new HashSet<Exam>();

	public ExaminationPeriod() {
		super();
	}

	public ExaminationPeriod(long id, NameOfExamPeriod nameOfExamPeriod, Date theBeginningOfTheExaminationPeriod,
			Date endOfExamPeriod, Set<Exam> exam) {
		super();
		this.id = id;
		this.nameOfExamPeriod = nameOfExamPeriod;
		this.theBeginningOfTheExaminationPeriod = theBeginningOfTheExaminationPeriod;
		this.endOfExamPeriod = endOfExamPeriod;
		this.exam = exam;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NameOfExamPeriod getNameOfExamPeriod() {
		return nameOfExamPeriod;
	}

	public void setNameOfExamPeriod(NameOfExamPeriod nameOfExamPeriod) {
		this.nameOfExamPeriod = nameOfExamPeriod;
	}

	public Date getTheBeginningOfTheExaminationPeriod() {
		return theBeginningOfTheExaminationPeriod;
	}

	public void setTheBeginningOfTheExaminationPeriod(Date theBeginningOfTheExaminationPeriod) {
		this.theBeginningOfTheExaminationPeriod = theBeginningOfTheExaminationPeriod;
	}

	public Date getEndOfExamPeriod() {
		return endOfExamPeriod;
	}

	public void setEndOfExamPeriod(Date endOfExamPeriod) {
		this.endOfExamPeriod = endOfExamPeriod;
	}

	public Set<Exam> getExam() {
		return exam;
	}

	public void setExam(Set<Exam> exam) {
		this.exam = exam;
	}



	
	
	
}

