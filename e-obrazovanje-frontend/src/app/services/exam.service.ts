import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ExamDto } from '../model/exam-dto';
import { SubjectDto } from '../model/subject-dto';

@Injectable({
  providedIn: 'root',
})
export class ExamService {

  constructor(private http: HttpClient, private fb: FormBuilder) {}

  authRequset: any = {
    username: 'admin',
    password: 'admin',
  };

  private _getUserFailedExams: string =
    'http://localhost:8080/api/exams/getUsersUnsettledExams';
  private _getUserPassedExam: string =
    'http://localhost:8080/api/exams/PassedExamByUser';
  private _getNotPassedExam: string =
    'http://localhost:8080/api/exams/getNotPassedExamFromUser';
  private _getExamById: string = 'http://localhost:8080/api/exams/getExamById';
  private _saveExam: string = 'http://localhost:8080/api/exams/saveExam';
  private _examFromPeriod: string =
    'http://localhost:8080/api/exams/getExamfromPeriod';

  private _getSubjectFromTeacher: string =
    'http://localhost:8080/api/subjects/getSubject';

  getToken() {
    return localStorage.getItem('jwt');
  }

  public generateToken(userData) {
    return this.http.post<any>(
      'http://localhost:8080/api/authenticate',
      userData
    );
  }

  public welcome() {
    return this.http.get('http://localhost:8080/api/exams');
  }

  public getUserFailedExam(nid) {
    return this.http.get<any>(`${this._getUserFailedExams}/${+nid}`);
  }

  public getUserPassedExam(nid) {
    return this.http.get<any>(`${this._getUserPassedExam}/${+nid}`);
  }

  public getNotPassedExam(nid) {
    return this.http.get<any>(`${this._getNotPassedExam}/${+nid}`);
  }

  public getExamById(nid) {
    return this.http.get<any>(`${this._getExamById}/${+nid}`);
  }

  public saveExam(data: ExamDto) {
    return this.http.post(`${this._saveExam}`, data);
  }

  public getExamByPeriod(idSubject, idPeriod) {
    return this.http.get<any>(
      `${this._examFromPeriod}/${+idSubject}/${+idPeriod}`
    );
  }

  public getSubjectFromTeacher(nid) {
    return this.http.get<any>(`${this._getSubjectFromTeacher}/${+nid}`);
  }
}
