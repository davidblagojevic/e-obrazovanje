package rs.projekatOSA2019_maven.service;

import java.util.List;

import rs.projekatOSA2019_maven.entity.Colloquium;
import rs.projekatOSA2019_maven.entity.Document;

public interface ColloquiumServiceInterface {

	List<Colloquium> getAll();
	
	Colloquium getById(Integer id);
	
	void delete(Colloquium d);
	
	Colloquium save(Colloquium colloquium);
}
