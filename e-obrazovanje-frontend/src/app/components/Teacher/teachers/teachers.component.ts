import { TeacherService } from './../../../services/teacher.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {

  teachers:any;

  name: string = '';
  lastName: string = '';
  
  email: string = '';
  constructor(private service:TeacherService) { }

  ngOnInit(): void {
    this.service.getAll()
        .subscribe(
        data => {
          this.teachers = data;
        }
      )
      console.log(this.teachers)
  }

}
