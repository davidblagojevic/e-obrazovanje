package rs.projekatOSA2019_maven.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.FinancialCard;
import rs.projekatOSA2019_maven.repository.FinancialCardRepository;

@Service
public class FinancialCardService implements FinancialCardServiceInterface {

	@Autowired
	FinancialCardRepository financialCardRepository;
	
	@Override
	public List<FinancialCard> findAll() {
		List<FinancialCard> financialCards = financialCardRepository.findAll();
		return financialCards;
	}

	@Override
	public FinancialCard save(FinancialCard financialCard) {
		return financialCardRepository.save(financialCard);
	}

	@Override
	public FinancialCard findByCardNumber(String cardNumber) {
		return financialCardRepository.findByCardNumber(cardNumber);
	}

	@Override
	public FinancialCard findByStudentId(long studentId) {
		return findByStudentId(studentId);
	}
	

	@Override
	public String createCardNumber() {
		int countCards = financialCardRepository.countFinancialCard();
		FinancialCard financialCard = null;
		String newCardNumber = "";
		final String cardNumberConst = "00";
		if(countCards == 0) {
			newCardNumber = cardNumberConst + "1";
		}else {
			do {
				countCards++;
				String serialNumber = Integer.toString(countCards);
				newCardNumber = cardNumberConst + serialNumber;
				financialCard = financialCardRepository.findByCardNumber(newCardNumber);
				//ako je nova fkr nece pronaci karticu i fk ostaje null
				//ako je postojeca fkr pronalazi karticu i fk nije null i ponovo se pokrece increment
			}while(financialCard != null);
			
			
		}	
		return newCardNumber;

	}

	@Override
	public int countFinancialCard() {
		return financialCardRepository.countFinancialCard();
	}

}
