package rs.projekatOSA2019_maven.repository;

import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import rs.projekatOSA2019_maven.entity.ExaminationPeriod;

public interface ExaminationPeriodRepository   extends JpaRepository<ExaminationPeriod, String>{

	
	ExaminationPeriod findByid(Long id);
	
	ExaminationPeriod findByTheBeginningOfTheExaminationPeriod(Date heBeginningOfTheExaminationPeriod );
	
	
	List<ExaminationPeriod>findByNameOfExamPeriod(String  nameOfExamPeriod);
	
	
	@Query("select i FROM ExaminationPeriod i where i.examinationPeriod_status=0")
	List<ExaminationPeriod>findAllDeleteFalse();

	

	
	
	@Query("select i FROM ExaminationPeriod i where i.theBeginningOfTheExaminationPeriod > :date and   :date > i.endOfExamPeriod")
	ExaminationPeriod  GetEximinationPeriodBetwenData(Date date);
	
	
	


	
	
	
	@Query("DELETE FROM ExaminationPeriod WHERE id=3")
	ExaminationPeriod  deleteExaminationPeriod(Long id);



}




