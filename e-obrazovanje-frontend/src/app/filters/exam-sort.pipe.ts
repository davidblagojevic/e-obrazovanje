import { Pipe, PipeTransform } from '@angular/core';
import { ExamDto } from '../model/exam-dto';
import { orderBy } from 'lodash';

@Pipe({
  name: 'examSort'
})
export class ExamSortPipe implements PipeTransform {

  transform(value: ExamDto[], order = '', column: string = ''): ExamDto[] {

    if (!value || order === '' || !order) {

      return value;    
  } // no array
    if (value.length <= 1) { 

      return value; } 
    
    if (!column || column === '') { 

      if(order==='asc'){

        return value.sort()}
      else{

        return value.sort().reverse();}
    } // sort 1d array
    return orderBy(value, [column], [order]);
  }
}