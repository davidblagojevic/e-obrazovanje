package rs.projekatOSA2019_maven.entity;

import java.io.Serializable;
import java.util.HashSet;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.projekatOSA2019_maven.enums.TheRoleOfTheTeacher;


@Entity
@Table(name = "teacher")
public class Teacher  {
	

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;
	private String lastName;
	private String email;
	private TheRoleOfTheTeacher theRoleOfTheTeacher;
	
	@OneToOne(mappedBy = "teacher")
	@NotFound(action=NotFoundAction.IGNORE)
	private User user;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="department_id", referencedColumnName="id", nullable=false)
	private Department department;
	
	
	@ManyToMany(mappedBy = "teachers")
	private Set<Subject> subject = new HashSet<Subject>();

	public Teacher() {
		super();
	}

	public Teacher(int id, String name, String lastName, String email, TheRoleOfTheTeacher theRoleOfTheTeacher,
			User user, Department department, Set<Subject> subject) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.email = email;
		this.theRoleOfTheTeacher = theRoleOfTheTeacher;
		this.user = user;
		this.department = department;
		this.subject = subject;
	}
	
	public void addUser(User user) {
		this.setUser(user);
		user.setTeacher(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public TheRoleOfTheTeacher getTheRoleOfTheTeacher() {
		return theRoleOfTheTeacher;
	}

	public void setTheRoleOfTheTeacher(TheRoleOfTheTeacher theRoleOfTheTeacher) {
		this.theRoleOfTheTeacher = theRoleOfTheTeacher;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Set<Subject> getSubject() {
		return subject;
	}

	public void setSubject(Set<Subject> subject) {
		this.subject = subject;
	}

	

	
	


}
