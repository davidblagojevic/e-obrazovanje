package rs.projekatOSA2019_maven.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="financialCard") 
public class FinancialCard {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String cardNumber;

	private double sum;
	
	private String bankAccount;
	
	private String referenceNumber;

	private String ModelNumber;
	
	@JsonIgnore
	@OneToOne(mappedBy = "financialCard")
	private Student student;

	public FinancialCard() {
		super();
	}

	public FinancialCard(long id, String cardNumber, double sum, String bankAccount, String referenceNumber,
			String modelNumber, Student student) {
		super();
		this.id = id;
		this.cardNumber = cardNumber;
		this.sum = sum;
		this.bankAccount = bankAccount;
		this.referenceNumber = referenceNumber;
		ModelNumber = modelNumber;
		this.student = student;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getModelNumber() {
		return ModelNumber;
	}

	public void setModelNumber(String modelNumber) {
		ModelNumber = modelNumber;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	

}
