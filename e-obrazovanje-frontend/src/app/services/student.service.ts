import { StudentDto } from './../model/student-dto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class StudentService {

 

getStudents: string ="http://localhost:8080/api/students";

constructor(private http:HttpClient) { }

public  getAll(){
  return this.http.get<any>("http://localhost:8080/api/students");
}

public  getAllDepartment(){
  return this.http.get<any>("http://localhost:8080/api/departments");
}

public  getStudentByUser(){
  return this.http.get<any>(`${this.getStudents}/fromUser`);
}

public  getStudentsByid(id){
  return this.http.get<StudentDto>(`${this.getStudents}/${id}`);
}

public create(student,id){
  console.log(JSON.stringify(student));
  return this.http.post<StudentDto>("http://localhost:8080/api/students/"+id,student).subscribe(
     (data) => {
      console.log(JSON.stringify(data));
    },
      (error) => {
      
        console.error('There was an error!', error);
    }
)
}

public update(student,id){
  return this.http.put<StudentDto>("http://localhost:8080/api/students/"+id,student).subscribe(
    (data) => {
     console.log(JSON.stringify(data));
   },
     (error) => {
     
       console.error('There was an error!', error);
   }
);
}

public deleteStudent(id){
  return this.http.delete(this.getStudents+"/"+id);
}
}
