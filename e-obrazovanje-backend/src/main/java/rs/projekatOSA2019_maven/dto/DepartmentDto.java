package rs.projekatOSA2019_maven.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rs.projekatOSA2019_maven.entity.Department;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;

public class DepartmentDto {
	
	private long id;
	
	private String name;
	
	private int numberOfECTSPoints;
	
	private String DepartmentMark;

	private TeacherDto teacher;
	
	private List<StudentDto> students = new ArrayList<StudentDto>();
	
	private List<SubjectDto> subjects = new ArrayList<SubjectDto>();

	public DepartmentDto() {
		super();
	}

	public DepartmentDto(Department d) {
		super();
		this.id = d.getId();
		this.name = d.getName();
		this.numberOfECTSPoints = d.getNumberOfECTSPoints();
		this.DepartmentMark = d.getDepartmentMark();
//		this.teacher = new TeacherDto(d.getTeacher());
//		if(d.getStudents() != null) {
//			for (Student student : d.getStudents()) {
//				this.students.add(new StudentDto(student));
//			}
//		}
		if(d.getSubjects() != null) {
			for (Subject subject : d.getSubjects()) {
				this.subjects.add(new SubjectDto(subject));
			}
		}
		
	}
	
	public void addSubjectDtos(Set<Subject> subjects) {
		for (Subject subject : subjects) {
			this.subjects.add(new SubjectDto(subject));
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfECTSPoints() {
		return numberOfECTSPoints;
	}

	public void setNumberOfECTSPoints(int numberOfECTSPoints) {
		this.numberOfECTSPoints = numberOfECTSPoints;
	}

	public String getDepartmentMark() {
		return DepartmentMark;
	}

	public void setDepartmentMark(String departmentMark) {
		DepartmentMark = departmentMark;
	}

	public TeacherDto getTeacher() {
		return teacher;
	}

	public void setTeacher(TeacherDto teacher) {
		this.teacher = teacher;
	}

	public List<StudentDto> getStudents() {
		return students;
	}

	public void setStudents(List<StudentDto> students) {
		this.students = students;
	}

	public List<SubjectDto> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<SubjectDto> subjects) {
		this.subjects = subjects;
	}

}
