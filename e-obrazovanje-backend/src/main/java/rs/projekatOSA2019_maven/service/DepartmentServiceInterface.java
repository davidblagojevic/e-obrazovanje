package rs.projekatOSA2019_maven.service;

import java.util.List;

import rs.projekatOSA2019_maven.entity.Department;

public interface DepartmentServiceInterface {
	
	Department findById(long id);
	
	List<Department> findAll();

}
