export class SubjectDto {

        id:number;
        name:string;
        numberOfECTSPoints:number;
        dateOfLaying:Date;

        constructor(id:number,name:string,numberOfECTSPoints:number,dateOfLaying:Date){
           
            this.id=id;
            this.name=name;
            this.numberOfECTSPoints=numberOfECTSPoints;
            this.dateOfLaying=dateOfLaying;
        }
}
    

