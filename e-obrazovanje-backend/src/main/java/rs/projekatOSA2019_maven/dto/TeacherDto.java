package rs.projekatOSA2019_maven.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.projekatOSA2019_maven.entity.Department;
import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.entity.Teacher;
import rs.projekatOSA2019_maven.entity.User;
import rs.projekatOSA2019_maven.enums.TheRoleOfTheTeacher;

public class TeacherDto {
	
	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public ArrayList<SubjectDto> getSubjectDto() {
		return subjectDto;
	}

	public void setSubjectDto(ArrayList<SubjectDto> subjectDto) {
		this.subjectDto = subjectDto;
	}

	private long id;
	private String name;
	private String lastName;
	private String email;
	private TheRoleOfTheTeacher theRoleOfTheTeacher;
	
	private UserDto user;


	private ArrayList<SubjectDto> subjectDto = new ArrayList<SubjectDto>();

	public TeacherDto() {
		super();
	}

	public TeacherDto(Teacher t) {
		super();
		this.id = t.getId();
		this.name = t.getName();
		this.lastName = t.getLastName();
		this.email = t.getEmail();
		this.theRoleOfTheTeacher = t.getTheRoleOfTheTeacher();
		this.user= new UserDto(t.getUser());
		if(t.getSubject() != null) {
			for (Subject subject : t.getSubject()) {
				this.subjectDto.add(new SubjectDto(subject));
			}
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public TheRoleOfTheTeacher getTheRoleOfTheTeacher() {
		return theRoleOfTheTeacher;
	}

	public void setTheRoleOfTheTeacher(TheRoleOfTheTeacher theRoleOfTheTeacher) {
		this.theRoleOfTheTeacher = theRoleOfTheTeacher;
	}

//	public User getKorisnik() {
//		return korisnik;
//	}
//
//	public void setKorisnik(User korisnik) {
//		this.korisnik = korisnik;
//	}
//
//	public Department getDepartment() {
//		return department;
//	}
//
//	public void setDepartment(Department department) {
//		this.department = department;
//	}
//
//	public Set<Subject> getSubject() {
//		return subject;
//	}
//
//	public void setSubject(Set<Subject> subject) {
//		this.subject = subject;
//	}


}

