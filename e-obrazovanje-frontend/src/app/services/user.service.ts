import { HttpClient } from '@angular/common/http';
import { error } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserDto } from '../model/user-dto';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private _httpClient: HttpClient, private _router: Router) {}

  _urlLoginAuthentication = 'http://localhost:8080/api/authenticate';
  _urlCurrentUser = 'http://localhost:8080/api/users/currentUser';
  _urlEditUser = 'http://localhost:8080/api/users/';

  login(userData) {
    console.log(userData);
    return this._httpClient.post<any>(
      'http://localhost:8080/api/authenticate',
      userData
    );
  }

  getCurrentUser() {
    return this._httpClient.get<any>(this._urlCurrentUser);
  }

  getRedirectedFromLogin(role) {
    console.log(localStorage.getItem('role'));
    console.log(localStorage.getItem('idUser'));

    console.log(localStorage.getItem('jwt'));

    if (role == 'GRESKA') {
      this._router.navigate(['login']);
    } else {
      if (role == 'ROLE_ADMIN') {
        this._router.navigate(['ExaminationPeriodAll']);
      } else if (role == 'ROLE_STUDENT') {
        console.log('+++++++getRedirectedFromLogin+++++++++');
        this._router.navigate(['ExaminationPeriod']);
      } else if (role == 'ROLE_PROFESSOR') {
        this._router.navigate(['ExaminationPeriodAll']);
      }
    }
  }

  loggedIn() {
    if (!this.getRole() || !this.getToken()) {
      return false;
    }

    return true;
  }

  getRole() {
    return localStorage.getItem('role');
  }

  getToken() {
    return localStorage.getItem('jwt');
  }

  isAuthorized(roles): boolean {
    var isMatch = false;

    var role = localStorage.getItem('role');
    roles.forEach((element) => {
      if (element === role) {
        isMatch = true;
        return false;
      }
    });

    return isMatch;
  }

  logout() {
    //LoginComponent.korIme = null;
    localStorage.removeItem('jwt');
    localStorage.removeItem('idUser');
    localStorage.removeItem('role');
    this._router.navigate(['login']);
  }

  updateUser(user) {
    this._httpClient.put<UserDto>(this._urlCurrentUser, user).subscribe(
      (data) => {
        console.log(JSON.stringify(data));
      },
      (error) => {
        console.error('There was an error!', error);
      }
    );
  }
}
