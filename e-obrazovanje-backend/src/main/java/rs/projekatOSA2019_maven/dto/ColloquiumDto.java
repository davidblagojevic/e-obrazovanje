package rs.projekatOSA2019_maven.dto;


import rs.projekatOSA2019_maven.entity.Colloquium;
import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;

public class ColloquiumDto {

	
	
	private long id;
	private float points;
	private String dateOfLaying;
	private String name; 
	private SubjectDto subject;
	private ExamDto exam;
	private StudentDto student;
	
	public ColloquiumDto(Colloquium c) {
		super();
		this.id = c.getId();
		this.points = c.getPoints();
		this.dateOfLaying = c.getDateOfLaying();
		this.name = c.getName();
		this.subject = new SubjectDto(c.getSubject());
		this.exam = new ExamDto(c.getExam());
		this.student = new StudentDto(c.getStudent());
	}

	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public float getPoints() {
		return points;
	}
	public void setPoints(float points) {
		this.points = points;
	}
	public String getDateOfLaying() {
		return dateOfLaying;
	}
	public void setDateOfLaying(String dateOfLaying) {
		this.dateOfLaying = dateOfLaying;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public SubjectDto getSubject() {
		return subject;
	}
	public void setSubject(SubjectDto subject) {
		this.subject = subject;
	}
	public ExamDto getExam() {
		return exam;
	}
	public void setExam(ExamDto exam) {
		this.exam = exam;
	}
	public StudentDto getStudent() {
		return student;
	}
	public void setStudent(StudentDto student) {
		this.student = student;
	}
	
	public ColloquiumDto() {
		super();
	}

	
	
	
}
