package rs.projekatOSA2019_maven.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.projekatOSA2019_maven.dto.SubjectDto;
import rs.projekatOSA2019_maven.dto.TeacherDto;
import rs.projekatOSA2019_maven.entity.Authority;
import rs.projekatOSA2019_maven.entity.Department;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.entity.Subject;
import rs.projekatOSA2019_maven.entity.Teacher;
import rs.projekatOSA2019_maven.entity.User;
import rs.projekatOSA2019_maven.enums.TheRoleOfTheUser;
import rs.projekatOSA2019_maven.service.DepartmentService;
import rs.projekatOSA2019_maven.service.TeacherService;
import rs.projekatOSA2019_maven.service.UserAuthorityService;
import rs.projekatOSA2019_maven.service.UserService;

@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value = "api/teachers")
public class TeacherController {
	
	@Autowired
	TeacherService teacherService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private UserAuthorityService userAuthorityService;
	
	@GetMapping 
	public ResponseEntity<List<TeacherDto>> getAllTeachers() {
		List<Teacher> teachers = teacherService.findAll();
		List<TeacherDto> teachersDTO = new ArrayList<TeacherDto>();
		for (Teacher teacher : teachers) {
			teachersDTO.add(new TeacherDto(teacher));
		}
		return new ResponseEntity<>(teachersDTO, HttpStatus.OK);
	}
	
	@GetMapping(value = "/fromUser") 
	public ResponseEntity<TeacherDto> getTeacher(Principal p) {
		User u=userService.findByUsername(p.getName());
		if(u.getTeacher() == null)
			return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(new TeacherDto(u.getTeacher()), HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}") 
	public ResponseEntity<TeacherDto> getTeacher(@PathVariable int id) {
		Optional<Teacher> s=teacherService.findOne(id);
		
		if(s.get() == null)
			return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(new TeacherDto(s.get()), HttpStatus.OK);
	}
	
//	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<TeacherDto> saveTeacher(@RequestBody TeacherDto teacherDto,@PathVariable long id) {
		Teacher s=new Teacher();
		
		User u=new User();
		u.setUsername(teacherDto.getUser().getUsername());
		u.setPassword(teacherDto.getUser().getPassword());
		u.setActivated(true);
		Authority authority = userAuthorityService.findByName("ROLE_PROFESSOR");
		u.getAuthorities().add(authority);
		u = userService.save(u);
//		userService.save(u);
		
		Department d=departmentService.findById(id);
		s.setDepartment(d);
		
		s.setName(teacherDto.getName());
		s.setLastName((teacherDto.getLastName()));
		s.setEmail(teacherDto.getEmail());
		s.setTheRoleOfTheTeacher(teacherDto.getTheRoleOfTheTeacher());
		s.setSubject(new HashSet<Subject>());
		s.setUser(u);
		s.addUser(u);
		teacherService.save(s);

		return new ResponseEntity<>(new TeacherDto(s), HttpStatus.CREATED);
	}
//	
	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<TeacherDto> editTeacher(@RequestBody TeacherDto student1, @PathVariable int id) {
		Optional<Teacher> s=teacherService.findOne(id);
		
		if(s == null)
			return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
		
		s.get().getUser().setUsername(student1.getUser().getUsername());
		s.get().setName(student1.getName());
		s.get().setLastName((student1.getLastName()));
		s.get().setEmail(student1.getEmail());
		teacherService.save(s.get());
		return new ResponseEntity<>(new TeacherDto(s.get()), HttpStatus.CREATED);
	}
//	
//	@DeleteMapping(value = "/{id}")
//	public ResponseEntity<Void> deleteTeacher(@PathVariable int id) {
//		Teacher student = teacherService.findOne(id);
//		if (student != null) {
//			Teacher s=student;
//			teacherService.delete(s);
//			return new ResponseEntity<>(HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//	}
//	@GetMapping(value = "/{id}")
//	public ResponseEntity<TeacherDTO> findTeacher(@PathVariable int id) {
//		Optional<Teacher> t = teacherService.findOne(id);
//		System.out.println("ucitelj");
//		if(t.get()==null)
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		else
//			return new ResponseEntity<>(new TeacherDTO(t.get()), HttpStatus.OK);
//	}
//	
	@GetMapping(value = "/subjects/{id}")
	public ResponseEntity<List<SubjectDto>> getTeachersSubject(@PathVariable int id) {
		Optional<Teacher> t = teacherService.findOne(id);
		if(t==null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		else {
			List<SubjectDto>subjectDTOs=new ArrayList<SubjectDto>();
			for (Subject s : t.get().getSubject()) {
				subjectDTOs.add(new SubjectDto(s));
				
			}
			return new ResponseEntity<>(subjectDTOs, HttpStatus.OK);
		}
			
	}
//	
//	@GetMapping(value = "/teachers/{name}")
//	public ResponseEntity<List<TeacherDTO>> findTeacher(@PathVariable String name) {
//		List<Teacher> students = teacherService.findAll();
//		List<TeacherDTO> studentsDTO = new ArrayList<TeacherDTO>();
//		for (Teacher student : students) {
//			studentsDTO.add(new TeacherDTO(student));
//		}
//		return new ResponseEntity<>(studentsDTO, HttpStatus.OK);
//	}

}
