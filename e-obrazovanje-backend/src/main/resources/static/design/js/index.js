$(document).ready(function () {

    fetchEmails();

    $(document).on("click", 'tr', function (event) {
        highlightRow(this);
    });

    //za slanje
    $(document).on("click", '#add', function (event) {
        $('#addModalScrollable').modal('show');
    });

});

function fetchEmails() {
    var url = window.location; // http://google.com?id=test
    var urlObject = new URL(url);
    var account = urlObject.searchParams.get('account');
    console.log(account);
    if (account === null){
        window.location.replace('email-accounts.html')
        return; 
    }
    $.ajax({
        type: "GET",
        url: "messages/account/" + account,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: createAuthorizationTokenHeader()
    }).then(
        function (data) {
            $("#dataTableBody").empty();
            for (i = 0; i < data.length; i++) {
                newRow = "<tr><td class=\"fromCell\">" + data[i].from + "</td>"
                    + "<td class=\"toCell\">" + data[i].to + "</td>"
                    + "<td class=\"subjectCell\">" + data[i].subject + "</td>"
                    + "<td class=\"contentCell\">" + data[i].content.substring(0, 8) + "..." + "</td>"
                    + "<td class=\"dateTimeCell\">" + data[i].dateTime + "</td>"
                    + "<td class=\"idCell\">" + data[i].id + "</td>" + "</tr>"
                $("#dataTableBody").append(newRow);
            }
        });

}