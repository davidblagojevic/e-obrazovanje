package rs.projekatOSA2019_maven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableScheduling
@EnableTransactionManagement
@EnableJpaRepositories
public class ProjekatOsa2019MavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjekatOsa2019MavenApplication.class, args);
	}

}
