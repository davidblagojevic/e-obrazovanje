import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StudentDto } from 'src/app/model/student-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentService } from './../../../services/student.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent implements OnInit {

  constructor(private service:StudentService,private router: Router,private route:ActivatedRoute,private fb: FormBuilder) { }

  student: StudentDto;
  updateStudent: FormGroup;

  get yearStudy() {
    return this.updateStudent.get('yearStudy');
  }

  get wayOfFinancing() {
    return this.updateStudent.get('wayOfFinancing');
  }
  get totalECTSPoints() {
    return this.updateStudent.get('totalECTSPoints');
  }

  get averageRating() {
    return this.updateStudent.get('averageRating');
  }

  ngOnInit(): void {
    let id=this.route.snapshot.params.id;
    this.service.getStudentsByid(id)
      .subscribe(
        data => {
          this.student = data;
          console.log(data);
          this.updateStudent= this.fb.group({
            yearStudy: [this.student.yearStudy,Validators.required],
            wayOfFinancing: [this.student.wayOfFinancing,Validators.required],
            totalECTSPoints: [this.student.totalECTSPoints,Validators.required],
            averageRating: [this.student.averageRating,Validators.required],
          });
        },
        error => {
          console.log(error);
        });
  
  }
  updateStud(){

    var student=new StudentDto(
      1,this.student.name,this.student.lastName,this.student.index,this.student.countryOfBirth,this.student.placeOfBirth,
      this.student.DateOfBirth,this.student.gender,this.wayOfFinancing.value,this.student.email,this.student.address,
      this.totalECTSPoints.value,this.averageRating.value,1,this.student.user,this.student.financialCard,this.yearStudy.value
    )
    if(this.validation(student)===true){
      this.service.update(student,this.route.snapshot.params.id);
      this.router.navigate(['StudentComponent']);
    }
     
  }
  validation(teacher:StudentDto){
    if( teacher.wayOfFinancing==='' ){
      alert("nacin finansiranja studenta je prazno ");
      return false;
    }
    else if( teacher.totalECTSPoints < 0){
      alert("etc broj studenta je manja od 0");
      return false;
    }
    else if( teacher.yearStudy < 1){
      alert("godina studija studenta je manja od 1");
      return false;
    }
    else if( teacher.averageRating < 0){
      alert("prosecna ocena studenta je manja od 0");
      return false;
    }
    else{
      return true;
    }

  }


}
