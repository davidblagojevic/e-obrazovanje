import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ExamDto } from 'src/app/model/exam-dto';
import { StudentDto } from 'src/app/model/student-dto';
import { ExamService } from 'src/app/services/exam.service';
// import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css'],
})
export class ExamComponent implements OnInit {
  updateExam: FormGroup;
  exam: ExamDto;
  examDto: ExamDto;
  id: string;
  stud: StudentDto;
  studentValue: string;
  index: string;
  field;
  order;
  saerctSubijekt: string = '';

  _assessment = ['5', '6', '7', '8', '9', '10'];

  get points_theory() {
    return this.updateExam.get('points_theory');
  }

  get exercise_points() {
    return this.updateExam.get('exercise_points');
  }

  get price() {
    return this.updateExam.get('price');
  }

  get assessment() {
    return this.updateExam.get('assessment');
  }

  get dateOfApplication() {
    return this.updateExam.get('dateOfApplication');
  }

  get dateOfLaying() {
    return this.updateExam.get('dateOfLaying');
  }

  constructor(
    private serviceExam: ExamService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {

    this.updateExam = this.fb.group({
      points_theory: ['', Validators.required],
      exercise_points: ['', Validators.required],
      price: ['', Validators.required],
      assessment: ['', this.fb.array(this._assessment)],
      dateOfApplication: ['', Validators.required],
      dateOfLaying: ['', Validators.required],
    });

    this.id = this.route.snapshot.paramMap.get('id');

    if (this.id == 'main') {
      let user = this.serviceExam.getUserFailedExam(
        localStorage.getItem('idUser')
      );
      user.subscribe((data) => (this.exam = data));
    }

    if (this.id != 'main') {
      this.getExamByIdAndSetValue(this.id);
    }
  }

 

  public getExamByIdAndSetValue(id) {
    let resp = this.serviceExam.getExamById(id);
    resp.subscribe((data) => {
      this.examDto = data;
      var _Price = this.examDto.price;
      var _ExercisePoints = this.examDto.exercisePoints;
      var _pointsTheory = this.examDto.pointsTheory;
      var _dateOfAapplication = this.examDto.dateOfApplication;
      var _dateOfLaying = this.examDto.dateOfLaying;
      var _assessment1 = this.examDto.assessment;


      this.studentValue = this.examDto.name + ' ' + this.examDto.lastname;
      this.index = this.examDto.index;

      this.updateExam.patchValue({
        price: _Price,
        exercise_points: _ExercisePoints,
        points_theory: _pointsTheory,
        dateOfApplication: _dateOfAapplication,
        dateOfLaying: _dateOfLaying,
        assessment: _assessment1.toString(),
      });
    });
  }

  public saveExaminationPerod(exa) {
    this.serviceExam.saveExam(exa).subscribe((data) => {
      this.router.navigate(['ExaminationPeriodAll']);
    });
  }

  updateExamSubmit() {
    var exercise_points = this.exercise_points.value;
    var points_theory = this.points_theory.value;
    var assessment = this.assessment.value;

    if(exercise_points == null || points_theory == null || assessment == null ){
      alert("fields must be filled")
    }
else{


    

    var exam = new ExamDto(
      this.examDto.laiddown,
      this.examDto.id,
      this.examDto.dateOfLaying,
      this.examDto.dateOfApplication,
      this.examDto.price,
      exercise_points,
      points_theory,
      assessment,
      this.stud,
      null,
      this.examDto.lastname,
      this.examDto.name,
      this.examDto.index
    );

    this.saveExaminationPerod(exam);

  }
  }

  counter = 0;
  sort(param) {
    if (param === 'price') {
      this.field = 'price';
    }
    if (param === 'dateOfLaying') {
      this.field = 'dateOfLaying';
    }
    if (param === 'dateOfApplication') {
      this.field = 'dateOfApplication';
    }
    if (param === 'subject.name') {
      this.field = 'subject.name';
    }
    if (param === 'pointsTheory') {
      this.field = 'pointsTheory';
    }
    if (param === 'exercisePoints') {
      this.field = 'exercisePoints';
    }

    if (this.counter === 0) {
      //desc
      this.order = 'asc';
      this.counter = 1;
    } else if (this.counter === 1) {
      //asc
      this.order = 'desc';
      this.counter = 0;
    }
  }
}
