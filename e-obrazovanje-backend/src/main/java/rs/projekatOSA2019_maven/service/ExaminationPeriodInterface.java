package rs.projekatOSA2019_maven.service;

import java.util.Date;
import java.util.List;

import rs.projekatOSA2019_maven.entity.ExaminationPeriod;

public interface ExaminationPeriodInterface {
	
	
	
	List<ExaminationPeriod> findAllDeleteFalse();
	
	ExaminationPeriod findByid(Long id);
	void remove(ExaminationPeriod id);

	List<ExaminationPeriod> fingByNameOfExamPeriod(String point);
	 
	ExaminationPeriod findBytheBeginningOfTheExaminationPeriod(Date dateOfLaying);
	
	ExaminationPeriod findByEndOfExamPeriod(Date endOfExamPeriod);
		
	ExaminationPeriod findBtBetwineData(Date data);
	void save(ExaminationPeriod examinationPeriod);

	void delete(ExaminationPeriod examinationPeriod);
		
//		List<Exam > findExamByUserAndPeriod();
	
}
