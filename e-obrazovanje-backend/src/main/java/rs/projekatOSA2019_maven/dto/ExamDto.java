package rs.projekatOSA2019_maven.dto;

import java.util.ArrayList;
import java.util.List;



import rs.projekatOSA2019_maven.entity.Colloquium;
import rs.projekatOSA2019_maven.entity.Exam;

import rs.projekatOSA2019_maven.enums.ExamStatus;

public class ExamDto {

	
	 
	
	private long id;
	private String dateOfLaying;
	private String dateOfApplication;
	private int assessment;
	private int pointsTheory;
	private int ExercisePoints;
	private boolean laiddown = false;
	private int Price;
	private ExamStatus examStatus;
	private ExaminationPeriodDto examinationPeriod;
	private List<ColloquiumDto> colloquium = new ArrayList<ColloquiumDto>();
	private SubjectDto subjectDto;
	private String lastname ;
	private String name ;
	private String index ;
	
	

	
	
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public SubjectDto getSubjectDto() {
		return subjectDto;
	}
	public void setSubjectDto(SubjectDto subjectDto) {
		this.subjectDto = subjectDto;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDateOfLaying() {
		return dateOfLaying;
	}
	public void setDateOfLaying(String dateOfLaying) {
		this.dateOfLaying = dateOfLaying;
	}
	public String getDateOfApplication() {
		return dateOfApplication;
	}
	public void setDateOfApplication(String dateOfApplication) {
		this.dateOfApplication = dateOfApplication;
	}
	public int getAssessment() {
		return assessment;
	}
	public void setAssessment(int assessment) {
		this.assessment = assessment;
	}
	public int getPointsTheory() {
		return pointsTheory;
	}
	public void setPointsTheory(int pointsTheory) {
		this.pointsTheory = pointsTheory;
	}
	public int getExercisePoints() {
		return ExercisePoints;
	}
	public void setExercisePoints(int exercisePoints) {
		ExercisePoints = exercisePoints;
	}
	public boolean isLaiddown() {
		return laiddown;
	}
	public void setLaiddown(boolean laiddown) {
		this.laiddown = laiddown;
	}
	public int getPrice() {
		return Price;
	}
	public void setPrice(int price) {
		Price = price;
	}
	public ExamStatus getExamStatus() {
		return examStatus;
	}
	public void setExamStatus(ExamStatus examStatus) {
		this.examStatus = examStatus;
	}

	public ExaminationPeriodDto getExaminationPeriod() {
		return examinationPeriod;
	}
	public void setExaminationPeriod(ExaminationPeriodDto examinationPeriod) {
		this.examinationPeriod = examinationPeriod;
	}
	public List<ColloquiumDto> getColloquium() {
		return colloquium;
	}
	public void setColloquium(List<ColloquiumDto> colloquium) {
		this.colloquium = colloquium;
	}

	
	public ExamDto(Exam e) {
		super();
		this.id = e.getId();
		this.dateOfLaying = e.getDateOfLaying();
		this.dateOfApplication = e.getDateOfApplication();
		this.assessment = e.getAssessment();
		this.pointsTheory = e.getPointsTheory();
		ExercisePoints = e.getExercisePoints();
		this.laiddown = e.isLaiddown();
		Price = e.getPrice();
		
	
		
		
		this.examStatus = e.getExamStatus();
		this.examinationPeriod = new ExaminationPeriodDto(e.getEaminationPeriod());
		if(e.getColloquium() != null) {
			for (Colloquium c : e.getColloquium()) {
				this.colloquium.add(new ColloquiumDto(c));
			}
		}
	}
	public ExamDto() {
		super();
	}
	public ExamDto(long id, String dateOfLaying, String dateOfApplication, int assessment, int pointsTheory,
			int exercisePoints, boolean laiddown, int price, ExamStatus examStatus,
			ExaminationPeriodDto examinationPeriod, List<ColloquiumDto> colloquium, SubjectDto subjectDto) {
		super();
		this.id = id;
		this.dateOfLaying = dateOfLaying;
		this.dateOfApplication = dateOfApplication;
		this.assessment = assessment;
		this.pointsTheory = pointsTheory;
		ExercisePoints = exercisePoints;
		this.laiddown = laiddown;
		Price = price;
		this.examStatus = examStatus;
		this.examinationPeriod = examinationPeriod;
		this.colloquium = colloquium;
		this.subjectDto = subjectDto;
	}
	
	
	public ExamDto(long id, String dateOfLaying, String dateOfApplication, int assessment, int pointsTheory,
			int exercisePoints, boolean laiddown, int price, ExamStatus examStatus,
			ExaminationPeriodDto examinationPeriod, List<ColloquiumDto> colloquium, SubjectDto subjectDto,
			String lastname, String name, String index) {
		super();
		this.id = id;
		this.dateOfLaying = dateOfLaying;
		this.dateOfApplication = dateOfApplication;
		this.assessment = assessment;
		this.pointsTheory = pointsTheory;
		ExercisePoints = exercisePoints;
		this.laiddown = laiddown;
		Price = price;
		this.examStatus = examStatus;
		this.examinationPeriod = examinationPeriod;
		this.colloquium = colloquium;
		this.subjectDto = subjectDto;
		this.lastname = lastname;
		this.name = name;
		this.index = index;
	}
	
	

	
	
	
	
	
	
	
	
//	public ExamDto(Exam exam) {
//		super();
//		this.id =  exam.getId();
//		this.dateOfLaying =  exam.getDateOfApplication();
//		this.dateOfApplication =  exam.getDateOfApplication();
//		this.assessment =  exam.getAssessment();
//		this.pointsTheory =  exam.getPointsTheory();
//		this.ExercisePoints =  exam.getExercisePoints();
//		this.laiddown =  exam.isLaiddown();
//		this.Price =  exam.getPrice();
//		this.subject =  exam.getSubject();
//		this.student =  exam.getStudent();
//		this.examinationPeriod =  exam.getEaminationPeriod();
//		this.colloquium =  exam.getColloquium();
//		this.examStatus =  exam.getExamStatus();
//	}
//	
	
	
}
