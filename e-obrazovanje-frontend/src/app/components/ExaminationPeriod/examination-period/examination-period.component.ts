import { Component, OnInit } from '@angular/core';
import { ExamDto } from 'src/app/model/exam-dto';
import { ExamService } from 'src/app/services/exam.service';
import { ExaminationPeriodService } from 'src/app/services/examination-period.service';

@Component({
  selector: 'app-examination-period',
  templateUrl: './examination-period.component.html',
  styleUrls: ['./examination-period.component.css']
})
export class ExaminationPeriodComponent implements OnInit {

  examsNotRegistered:ExamDto;
  examsRegistered:ExamDto;
 
  isShown: boolean = true ; 

  constructor(private serviceExaminationPeriod:ExaminationPeriodService,
    private exam:ExamService
    
    ) { }
  ngOnInit(): void {


    this.getExamFromUserAndPeriodRegistered()
    this.getExamFromUserAndPeriodNotRegistered()
    
  
  }
 
 
 
      public getExamFromUserAndPeriodNotRegistered(){



        
          let resp=this.serviceExaminationPeriod.getNotRegisteredExam( localStorage.getItem('idUser'));
          resp.subscribe(data=>this.examsNotRegistered = data
            
            ,
            (error) => {                              //Error callback
              console.log('error caught in component')
             this.toggleShow()

            }
        
        );

      }


 

      public getExamFromUserAndPeriodRegistered(){
        
        let resp=this.serviceExaminationPeriod.getRegisteredExam(localStorage.getItem('idUser'))
        
        resp.subscribe(data=>this.examsRegistered = data
          

          ,
            (error) => {                              //Error callback
              console.log('error caught in component')
              this.toggleShow()

            }
          
          
          );

      }

      toggleShow() {

        this.isShown = ! this.isShown;
       
        
        }
      
      
        


      public addToCart(product){


        window.alert('do you want to exam check-in?');
        console.log("product"+product)
        let resp=this.serviceExaminationPeriod.setExamStatus1(product);

        resp.subscribe(data=>this.examsNotRegistered = data);
        
        
        this.getExamFromUserAndPeriodRegistered();
        this.getExamFromUserAndPeriodNotRegistered();
        location.reload();

    }


        


    public addToCartOnNull(product){


      window.alert('do you want to exam cancellation?');
          console.log("product"+product)
      let resp=this.serviceExaminationPeriod.setExamStatus1OnNull(product);

      resp.subscribe(data=>this.examsNotRegistered = data);
      
      
      this.getExamFromUserAndPeriodRegistered();
      this.getExamFromUserAndPeriodNotRegistered();
      location.reload();

  }









}




