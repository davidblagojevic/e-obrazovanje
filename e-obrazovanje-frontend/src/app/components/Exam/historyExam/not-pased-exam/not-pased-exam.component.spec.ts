import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotPasedExamComponent } from './not-pased-exam.component';

describe('NotPasedExamComponent', () => {
  let component: NotPasedExamComponent;
  let fixture: ComponentFixture<NotPasedExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotPasedExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotPasedExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
