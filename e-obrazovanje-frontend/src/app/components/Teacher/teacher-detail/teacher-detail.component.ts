import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TeacherDto } from './../../../model/teacher-dto';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TeacherService } from 'src/app/services/teacher.service';
import { UserDto } from 'src/app/model/user-dto';

@Component({
  selector: 'app-teacher-detail',
  templateUrl: './teacher-detail.component.html',
  styleUrls: ['./teacher-detail.component.css']
})
export class TeacherDetailComponent implements OnInit {

  teacher:TeacherDto;
  updateTeacher: FormGroup;
  constructor(private service:TeacherService,private router: Router,private route:ActivatedRoute,private fb: FormBuilder) { }


  get name() {
    return this.updateTeacher.get('name');
  }

  get lastName() {
    return this.updateTeacher.get('lastName');
  }

  get userName() {
    return this.updateTeacher.get('userName');
  }

  get password() {
    return this.updateTeacher.get('password');
  }

  get email() {
    return this.updateTeacher.get('email');
  }

  ngOnInit(): void {
    let id=this.route.snapshot.params.id;
    this.service.getTeacherByid(id)
      .subscribe(
        data => {
          this.teacher = data;
          console.log(this.teacher);
          console.log(data);
          var _name = this.teacher.name;
          var _lastName = this.teacher.lastName;
          var _email = this.teacher.email;
          var _userName=this.teacher.user.username;
          var _password=this.teacher.user.password;

          this.updateTeacher = this.fb.group({
            name: [_name, Validators.required],
            lastName: [_lastName, Validators.required],
            userName: [_userName,Validators.required],
            password: [_password,Validators.required],
            email: [_email, Validators.required]
          });
        },
        error => {
          console.log(error);
        });
  }

  updateTeach(){
    var user=new UserDto(this.userName.value,this.password.value);
    var teacher=new TeacherDto(
      1,this.name.value,this.lastName.value,this.email.value,user
    )
      console.log(teacher);
    this.service.update(teacher,this.route.snapshot.params.id);
    this.router.navigate(['/TeacherComponent']);
  }


}
