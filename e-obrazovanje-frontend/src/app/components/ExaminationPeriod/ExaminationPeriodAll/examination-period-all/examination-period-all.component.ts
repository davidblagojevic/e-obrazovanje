import { Component, OnInit } from '@angular/core';
import { ExaminationPeriodDto } from 'src/app/model/examination-period-dto';
import { ExamService } from 'src/app/services/exam.service';
import { ExaminationPeriodService } from 'src/app/services/examination-period.service';



@Component({
  selector: 'app-examination-period-all',
  templateUrl: './examination-period-all.component.html',
  styleUrls: ['./examination-period-all.component.css']
})
export class ExaminationPeriodAllComponent implements OnInit {

  field;
  order;
  examinationPeriodDto:ExaminationPeriodDto;
  role:string;

  searchTerm1:string;
  searchTerm2:string;
  searchTerm3:string;

  constructor(private serviceExaminationPeriod:ExaminationPeriodService,
    ) { }







  ngOnInit(): void {

    
    this.role=localStorage.getItem("role");
    this.getExamFromUserAndPeriodRegistered()
  }



  public getExamFromUserAndPeriodRegistered(){
    console.log("getExamFromUserAndPeriodRegistered")
    let resp=this.serviceExaminationPeriod.getExaminationPeriodAll()
    resp.subscribe(data=>this.examinationPeriodDto = data);
    
  }



  public deleteExaminationPeriod(nid){
    
    if(confirm("Are you sure to delete ")) {
      let resp=this.serviceExaminationPeriod.deleteExaminationPeriodById(nid)
      resp.subscribe(data=>this.examinationPeriodDto = data);
      location.reload();

    }
   
    
  }


  counter = 0;
  sort(param){
    console.log("sss-- "+param)
    if(param === 'endOfExamPeriod'){
      this.field = 'endOfExamPeriod'
    }
    if(param === 'nameOfExamPeriod'){
      this.field = 'nameOfExamPeriod'
    }
    if(param === 'theBeginningOfTheExaminationPeriod'){
      this.field = 'theBeginningOfTheExaminationPeriod'
    }

    if(this.counter === 0){
      //desc
      this.order = 'asc';
      this.counter = 1;
    }else if(this.counter === 1){
      //asc
      this.order = 'desc';
      this.counter = 0;
    }
  }

}
