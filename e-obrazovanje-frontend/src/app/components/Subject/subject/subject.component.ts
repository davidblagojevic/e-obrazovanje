import { StudentService } from './../../../services/student.service';
import { TeacherService } from './../../../services/teacher.service';
import { UserService } from './../../../services/user.service';
import { SubjectService } from './../../../services/subject.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  subjects: any;
  name: string;
  date: Date;
  role=localStorage.getItem("role");
  constructor(private service:SubjectService,private router: Router,private route:ActivatedRoute, private _userService: UserService
    ,private teach: TeacherService,private sub:StudentService) { }

  ngOnInit(): void {
    switch(this.role){
      case 'ROLE_STUDENT':{
        this.sub.getStudentByUser().subscribe(data => {
          console.log(data);
          this.subjects=data.subjects;
          
      });
      break;
      }
      case 'ROLE_PROFESSOR':{
        this.teach.getTeacherByUser().subscribe(data => {
          console.log(data);
          this.subjects=data.subjectDto;
          // location.reload();

      });
      break;
      }
      default:{
        this.service.getAll()
        .subscribe(
        data => {
          this.subjects = data;

        }
      )
      }
    }

  }

}
