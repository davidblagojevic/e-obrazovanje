package rs.projekatOSA2019_maven.controller;

import java.io.Console;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.security.auth.Subject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import rs.projekatOSA2019_maven.dto.ColloquiumDto;
import rs.projekatOSA2019_maven.dto.ExamDto;
import rs.projekatOSA2019_maven.dto.ExaminationPeriodDto;
import rs.projekatOSA2019_maven.dto.SubjectDto;
import rs.projekatOSA2019_maven.entity.Colloquium;
import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.ExaminationPeriod;
import rs.projekatOSA2019_maven.entity.Student;
import rs.projekatOSA2019_maven.enums.ExamStatus;
import rs.projekatOSA2019_maven.repository.ExamRepository;
import rs.projekatOSA2019_maven.repository.UserRepository;
import rs.projekatOSA2019_maven.service.ExamService;
import rs.projekatOSA2019_maven.service.ExaminationPeriodService;
import rs.projekatOSA2019_maven.service.StudentService;
import rs.projekatOSA2019_maven.service.SubjectService;
import rs.projekatOSA2019_maven.service.TeacherService;

@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value = "api/examinationperiod")
public class ExaminationPeriodController {




	@Autowired
	ExamService examService;
	
	@Autowired
	ExamRepository examRepository;
	
	
	@Autowired
	ExaminationPeriodService examinationPeriodService;
	
	
	@Autowired
	TeacherService teacher; 
	
	
	@Autowired
	StudentService studentService;
	
	@Autowired
	UserRepository serRepository;
	
	@Autowired
	SubjectService subjectService;
	
	
	
	
	
	
	
	
	
	public void  setExamFromUserInExaminationPeriod(@PathVariable Integer id) {
		
		

	
		
		System.out.println("-----------------------------setExamFromUserInExaminationPeriod-----------------------");
		
		  LocalDateTime myDateObj = LocalDateTime.now();
		    System.out.println("Before formatting: " + myDateObj);
		    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd ");

		    String formattedDate = myDateObj.format(myFormatObj);
		
		
		
		Date currentDate = Calendar.getInstance().getTime();

		ExaminationPeriod examinationPeriod= examinationPeriodService.findBtBetwineData(currentDate);
		
		System.out.println("--------examinationPeriodsss--------"+examinationPeriod.isExaminationPeriod_status());

		if(examinationPeriod.isExaminationPeriodValid( )== false) {
		Student student =studentService.findById(id);
		
		Set<rs.projekatOSA2019_maven.entity.Subject> subjectList =studentService.findById(id).getSubjects();

		List<Exam> examspassedList = examService.getExamsFromUserWherePassedExam(id);
		
		System.out.println("examItemLength---------"+examspassedList.size());

		
		for(rs.projekatOSA2019_maven.entity.Subject te: subjectList) {
			System.out.println("examItemLength---1------");

			
			if(examspassedList.size()>0) {
				
			
			
				
			for(Exam examItem: examspassedList) {
				
				
				
				System.out.println("examItemLength---2------");

				if(examItem.getSubject().getId() !=te.getId() &&  examItem.getEaminationPeriod().getId()== examinationPeriod.getId() ) {

					System.out.println("examItemLength---222e2------"+examinationPeriod.getId());

					Exam exam= new Exam();
					
					exam.setDateOfApplication(formattedDate);
					exam.setDateOfLaying(formattedDate);
					exam.setEaminationPeriod(examinationPeriod);
					exam.setExamStatus(ExamStatus.REGISTERED);
					exam.setSubject(te);
					exam.setStudent(student);
					exam.setPointsTheory(0);
					exam.setLaiddown(false);

					examService.save(exam);
					
						
		}
		}
			
			}else if(examspassedList.size()==0){
				
				System.out.println("examItemLength---222e2------"+examinationPeriod.getId());


					Exam exam= new Exam();
					
					exam.setDateOfApplication(formattedDate);
					exam.setDateOfLaying(formattedDate);
					exam.setEaminationPeriod(examinationPeriod);
					exam.setExamStatus(ExamStatus.REGISTERED);
					exam.setSubject(te);
					exam.setStudent(student);
					exam.setPointsTheory(0);
					exam.setLaiddown(false);

					examService.save(exam);
				
				
			}
			
			
			examinationPeriod.setExaminationPeriodValid(true);
		}
		}
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "get_exam_from_period_pass/{id}",produces ="application/json")
	public ResponseEntity<List<ExamDto>>  getExamsInPeriod1(@PathVariable Integer id) {
		
		
		this.setExamFromUserInExaminationPeriod(id);
		
		System.out.println("idperiod   "+id);
		

		Date currentDate = Calendar.getInstance().getTime();

		
		ExaminationPeriod examinationPeriod= examinationPeriodService.findBtBetwineData(currentDate);
		

	
		List<Exam> exams = (List<Exam>) examService.getExamFromUserAndPeriod(id,examinationPeriod.getId());
		
		
		List<ExamDto> examDtosList = new ArrayList<>();
		for(Exam te: exams) {
			
				System.out.println("exam---------"+te.getSubject().getName());

				SubjectDto subjectDto = new SubjectDto();
				
				subjectDto.setName(te.getSubject().getName());
				subjectDto.setId(te.getSubject().getId());
				
				ExamDto  examDtos = new ExamDto();
				examDtos.setId(te.getId());
				examDtos.setDateOfLaying(te.getDateOfLaying());
				examDtos.setDateOfApplication(te.getDateOfApplication());
				examDtos.setAssessment(te.getAssessment());
				if(te.getColloquium() != null) {
					for (Colloquium c : te.getColloquium()) {
						examDtos.getColloquium().add(new ColloquiumDto(c));
					}
				}
				examDtos.setSubjectDto(subjectDto);
				examDtos.setPrice(te.getPrice());
				examDtos.setPointsTheory(te.getPointsTheory());
				examDtosList.add(examDtos);
				

		}
		
		System.out.println("idperiod  examDtosList   "+examDtosList);

		
		
		
		return new ResponseEntity<>(examDtosList, HttpStatus.OK);	
		
		
		
		
		
	}
	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "getexamfromperiodnotpass/{id}",produces ="application/json")
	public  ResponseEntity<List<ExamDto>> getExamsInPeriod(@PathVariable Integer id) {
		
		
		
		Date currentDate = Calendar.getInstance().getTime();

		
		ExaminationPeriod examinationPeriod= examinationPeriodService.findBtBetwineData(currentDate);
		
		

		
//		System.out.println("getExamsInPeriod  "+id);
		
		
		List<Exam> exams1 = (List<Exam>) examService.getExamFromUserAndPeriodNotPass(id,(int) (long) examinationPeriod.getId());
		
		
		
		List<ExamDto> examDtosList = new ArrayList<>();
		for(Exam te: exams1) {
			
				System.out.println("exam---------"+te);
				SubjectDto subjectDto = new SubjectDto();
				
				subjectDto.setName(te.getSubject().getName());
				subjectDto.setId(te.getSubject().getId());
				
				ExamDto  examDtos = new ExamDto();
				examDtos.setId(te.getId());
				examDtos.setDateOfLaying(te.getDateOfLaying());
				examDtos.setDateOfApplication(te.getDateOfApplication());
				examDtos.setAssessment(te.getAssessment());
				
				if(te.getColloquium() != null) {
					for (Colloquium c : te.getColloquium()) {
						examDtos.getColloquium().add(new ColloquiumDto(c));
					}
				}
				
				examDtos.setSubjectDto(subjectDto);

				examDtos.setPrice(te.getPrice());
				examDtos.setPointsTheory(te.getPointsTheory());
				examDtosList.add(examDtos);
				

		}
		
		
		return new ResponseEntity<>(examDtosList, HttpStatus.OK);	
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "deleteExaminationPeriod/{id}")
	public  ResponseEntity<?> delete(@PathVariable Long id) {
		
		System.out.println("delete");
		
		ExaminationPeriod ex= examinationPeriodService.findByid(id);
		
		
		System.out.println("delete--"+ex.getId());


	try {
				
		
		System.out.println("delete221 --");

	
		examinationPeriodService.remove(ex);
	
				}
				catch(Exception e) {
					System.out.println("HttpStatus.NOT_FOUND --");

					return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
				}
	System.out.println("HttpStatus.OK --");

		return new ResponseEntity<>(null, HttpStatus.OK);
		
		
		
		
	}
		
		
		
		
		
		

	
	
	
	
	
	
	
	
	
	
	

//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "getExamsInPeriodAll",produces ="application/json")
	public  ResponseEntity<List<ExaminationPeriodDto>> getExamsInPeriodAll() {
	System.out.println("ResponseEntity<List<ExaminationPeriodDto>> getExamsInPeriodAll() ");
	System.out.println("no1vo");

		List<ExaminationPeriod> examinationPeriod =(List<ExaminationPeriod>) examinationPeriodService.findAllDeleteFalse();
		System.out.println("novo");

		List<ExaminationPeriodDto> examinationPeriodDtoList = new ArrayList<ExaminationPeriodDto>();
	
		
		
		try {
			
			
			for(ExaminationPeriod te: examinationPeriod) {
				
					
				ExaminationPeriodDto ExaminationPeriodDto =new ExaminationPeriodDto(te);

				
					examinationPeriodDtoList.add(ExaminationPeriodDto);
					
					
			}
			}
			catch(Exception e) {
				System.out.println("novo1");

				return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);


			}
		
		
		
		
		return new ResponseEntity<>(examinationPeriodDtoList, HttpStatus.OK);	
		
	}
	
	
	
	
	

	
	
	
	
	@RequestMapping(value="updateEximinationPeriod",method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<ExaminationPeriodDto> save(@RequestBody ExaminationPeriodDto examp){
	
		
		System.out.println("++++++++++++++++++++++++++++++++++++++");

		System.out.println("save1"+examp.getId());
		System.out.println("save2"+examp.getEndOfExamPeriod());

		System.out.println("save3"+examp.getTheBeginningOfTheExaminationPeriod());

		System.out.println("save4"+examp.getNameOfExamPeriod());

		System.out.println("++++++++++++++++++++++++++++++++++++++");
		ExaminationPeriod examinUp=examinationPeriodService.findByid(examp.getId());
		
				
		if(examinUp == null) {
			System.out.println("return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);");
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }

		
		examinUp.setEndOfExamPeriod(examp.getEndOfExamPeriod());
		examinUp.setId(examp.getId());
		examinUp.setNameOfExamPeriod(examp.getNameOfExamPeriod());
		examinUp.setTheBeginningOfTheExaminationPeriod(examp.getTheBeginningOfTheExaminationPeriod());
	
		examinationPeriodService.save(examinUp);
		
		return new ResponseEntity<>(examp, HttpStatus.OK);
	}

	
	
	
	
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "examinationPeriodById/{id}",produces ="application/json")
	public ResponseEntity<ExaminationPeriodDto>  getExamsInPeriodBtId(@PathVariable long id) {
		
		System.out.println("STIGO 1");
		
		ExaminationPeriod examinationPeriod= examinationPeriodService.findByid(id);
		if(examinationPeriod == null) {
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
		
		
		ExaminationPeriodDto examinationPeriodDto=new ExaminationPeriodDto(examinationPeriod);
		
	
		
		System.out.println("STIGO 2");

		return new ResponseEntity<>(examinationPeriodDto, HttpStatus.OK);	
		
		
		
		
		
	}
	
	
	
	
	 @RequestMapping(value="/saveExaminationPeriod" ,method = RequestMethod.PUT, consumes =MediaType.APPLICATION_JSON_VALUE)
	 @ResponseBody
	 
	 public ResponseEntity<ExaminationPeriodDto>  saveExaminationPeriod(@RequestBody ExaminationPeriodDto examp) {
		
		System.out.println("saveExaminationPeriod 1");
		
		
		if(examp == null) {
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }

		
		ExaminationPeriodDto examinationPeriodDto=new ExaminationPeriodDto();
		
		
		
		examinationPeriodService.save(examinationPeriodDto.examinationPeriod(examp));
		
		System.out.println("STIGO 2");

		return new ResponseEntity<>(examinationPeriodDto, HttpStatus.OK);	
		
		
		
		
		
	}
	 
	 
	 
	 
//		@PreAuthorize("hasRole('ROLE_ADMIN')")
		@DeleteMapping(value = "examinationPeriodDeleteById/{id}",produces ="application/json")
		public ResponseEntity<ExaminationPeriodDto>  deleteExamination(@PathVariable long id) {
			
		
			
			
			
			
			
			try {
				System.out.println("STIGO 1"+id);
				
				ExaminationPeriod examinationPeriod= examinationPeriodService.findByid(id);
			
				
				if(examinationPeriod == null) {
		       
					System.out.println("STIGO 2");

					
					return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		        
				}
				
				System.out.println("STIGO 3");

				 examinationPeriodService.delete(examinationPeriod);
				
//				ExaminationPeriodDto examinationPeriodDto=new ExaminationPeriodDto(examinationPeriod);
				
			
				

				}
				catch(Exception e) {
					System.out.println("STIGO 2");

					return new ResponseEntity<>(  null ,HttpStatus.INTERNAL_SERVER_ERROR);
				}
			
			return new ResponseEntity<>(null, HttpStatus.OK);	
		}
		
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
}
