import { SubjectDto } from "./subject-dto";


export class ExaminationPeriodDto {

    id:number;
    endOfExamPeriod:Date;
    theBeginningOfTheExaminationPeriod:Date;
    nameOfExamPeriod:string;
    subject:SubjectDto;

    constructor(id:number,endOfExamPeriod:Date,theBeginningOfTheExaminationPeriod:Date,nameOfExamPeriod:string,_subject:SubjectDto){
        
        this.subject=_subject;
        this.id = id;
        this.endOfExamPeriod = endOfExamPeriod;
        this.theBeginningOfTheExaminationPeriod = theBeginningOfTheExaminationPeriod;
        this.nameOfExamPeriod = nameOfExamPeriod;
      
    }

    

}

