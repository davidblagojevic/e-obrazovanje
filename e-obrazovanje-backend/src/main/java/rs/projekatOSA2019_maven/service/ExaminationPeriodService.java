package rs.projekatOSA2019_maven.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.projekatOSA2019_maven.entity.Exam;
import rs.projekatOSA2019_maven.entity.ExaminationPeriod;
import rs.projekatOSA2019_maven.repository.ExaminationPeriodRepository;


@Service
public class ExaminationPeriodService implements ExaminationPeriodInterface {

	@Autowired
	ExaminationPeriodRepository examinationPeriodRepository;
	
	
	@Override
	public List<ExaminationPeriod> findAllDeleteFalse() {
		
		return examinationPeriodRepository.findAllDeleteFalse() ;
	}
	
	
	

	
	@Override
	public ExaminationPeriod findByid(Long id) {

			System.out.println("ExaminationPeriod");
		
		ExaminationPeriod examinationPeriod=examinationPeriodRepository.findByid(id);
			
		return examinationPeriod;
	}

	@Override
	public List<ExaminationPeriod> fingByNameOfExamPeriod(String p) {
		List<ExaminationPeriod> examinationPeriod=examinationPeriodRepository.findByNameOfExamPeriod(p);
		
		return examinationPeriod;	
	}


	
	

	@Override
	public ExaminationPeriod findBytheBeginningOfTheExaminationPeriod(Date dateOfLaying) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public ExaminationPeriod findByEndOfExamPeriod(Date endOfExamPeriod) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public ExaminationPeriod findBtBetwineData(Date date) {
		ExaminationPeriod examinationPeriod=examinationPeriodRepository.GetEximinationPeriodBetwenData( date);
		System.out.println("stigo period-- "+examinationPeriod.getEndOfExamPeriod());
		return examinationPeriod;
	}



	@Override
	public void remove(ExaminationPeriod examinationPeriod) {
		
		System.out.println("remove");
		examinationPeriodRepository.save(examinationPeriod);

	}


	
	
	@Override
	public void save(ExaminationPeriod examinationPeriod) {
		
		examinationPeriodRepository.save(examinationPeriod);

	}
	
	
	
	@Override
	public void delete(ExaminationPeriod examinationPeriod) {
		
		examinationPeriod.setExaminationPeriod_status(true);
		
		
		examinationPeriodRepository.save(examinationPeriod);

	}

	
	


	
}
