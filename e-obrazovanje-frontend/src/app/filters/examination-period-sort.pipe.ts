import { Pipe, PipeTransform } from '@angular/core';
import { ExaminationPeriodDto } from '../model/examination-period-dto';
import { orderBy } from 'lodash';

@Pipe({
  name: 'examinationPeriodSort'
})
export class ExaminationPeriodSortPipe implements PipeTransform {

  transform(value: ExaminationPeriodDto[], order = '', column: string = ''): ExaminationPeriodDto[] {

console.log("eeee")

    if (!value || order === '' || !order) {

      return value;    
  } // no array
    if (value.length <= 1) { 

      return value; } 
    
    if (!column || column === '') { 

      if(order==='asc'){

        return value.sort()}
      else{

        return value.sort().reverse();}
    } // sort 1d array
    return orderBy(value, [column], [order]);
  }
}